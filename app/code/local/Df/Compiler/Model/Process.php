<?php

class Df_Compiler_Model_Process extends Mage_Compiler_Model_Process {

	/**
	 * @override
	 * @return array
	 */
	public function getCompileClassList() {

		/** @var Df_Core_Model_Lib $libCore */
		$libCore = Mage::getSingleton ('df_core/lib');
		$libCore->init ();

		/** @var Df_Core_Model_Lib $libZf */
		$libZf = Mage::getSingleton ('df_zf/lib');
		$libZf->init ();

		$arrFiles = array();

		foreach ($this->getScopes() as $code) {

			/** @var array|null $classes */
			$classes = df_a ($this->getConfigMap(), $code);

			if (is_array($classes)) {
				$arrFiles[$code] = array_keys($classes);
			} else {
				$arrFiles[$code] = array();
			}

			$statClasses = array();


			/** @var string|null $statFileForTheCurrentScope */
			$statFileForTheCurrentScope =
				df_a (
					$this->getStatFiles()
					,
					$code
				)
			;

			if (!is_null ($statFileForTheCurrentScope)) {

				$statClassesAll = explode("\n", file_get_contents($statFileForTheCurrentScope));

				/** @var int $statClassesCount */
				$statClassesCount = count ($statClassesAll);

				/** @var int $statClassesLimit */
				$statClassesLimit = round (1.0 * $statClassesCount);

				$popularStatClasses = array();
				foreach ($statClassesAll as $index => $classInfo) {
					$classInfo = explode(':', $classInfo);
					$popularStatClasses[$classInfo[1]][] = $classInfo[0];
				}
				krsort($popularStatClasses);


				$statClassesUsageCount = 0;

				foreach ($popularStatClasses as $popularStatClassesCurrrent) {
					/** @var array $popularStatClassesCurrrent */

					if ($statClassesUsageCount > $statClassesLimit) {
						break;
					}

					$statClasses =
						array_merge (
							$statClasses
							,
							$popularStatClassesCurrrent
						)
					;

					$statClassesUsageCount += count ($popularStatClassesCurrrent);
				}
			}
			$arrFiles[$code] = array_merge($arrFiles[$code], $statClasses);
			$arrFiles[$code] = array_unique($arrFiles[$code]);
			sort($arrFiles[$code]);
		}

		foreach ($arrFiles as $scope=>$classes) {
			if ($scope != 'default') {
				foreach ($classes as $index => $class) {
					if (in_array($class, $arrFiles['default'])) {
						unset($arrFiles[$scope][$index]);
					}
				}
			}
		}
		return $arrFiles;
	}



	/**                  
	 * @override
	 * @param  $classes
	 * @param  $scope
	 * @return string
	 */
    protected function _getClassesSourceCode($classes, $scope)
    {
		if (@class_exists('Mage_Shell_Compiler', false)) {
			Mage::helper ('df_core/lib');
		}

		return
				!(
					// Видимо, улучшенную компиляцию нельзя отрубать даже по истичению лицензии,
					// иначе при неправильной компиляции сайт может перестать работать
					//
					df_cfg()->admin()->system()->tools()->compilation()->getFix()
				)
			?
				parent::_getClassesSourceCode ($classes, $scope)
			:
				$this->_getClassesSourceCodeDf ($classes, $scope)

		;
    }

	



	/**              
	 * @param  $classes
	 * @param  $scope
	 * @return string
	 */
    private function _getClassesSourceCodeDf ($classes, $scope)
    {
		$sortedClasses = array();
        foreach ($classes as $className) {

			if (!@class_exists ($className)) {
				continue;
			}

            $implements = array_reverse(class_implements($className));
            foreach ($implements as $class) {
                if (!in_array($class, $sortedClasses) && !in_array($class, $this->_processedClasses) && strstr($class, '_')) {
                    $sortedClasses[] = $class;
                    if ('default' === $scope) {
                        $this->_processedClasses[] = $class;
                    }
                }
            }
            $extends    = array_reverse(class_parents($className));
            foreach ($extends as $class) {
                if (!in_array($class, $sortedClasses) && !in_array($class, $this->_processedClasses) && strstr($class, '_')) {
                    $sortedClasses[] = $class;
                    if ('default' === $scope) {
                        $this->_processedClasses[] = $class;
                    }
                }
            }
            if (!in_array($className, $sortedClasses) && !in_array($className, $this->_processedClasses)) {
                $sortedClasses[] = $className;
                    if ('default' === $scope) {
                        $this->_processedClasses[] = $className;
                    }
            }
        }

        $classesSource = "<?php\n";
        foreach ($sortedClasses as $className) {
            $file = $this->_includeDir.DS.$className.'.php';
            if (!file_exists($file)) {
                continue;
            }
            $content = file_get_contents($file);


			/**
			 * Не компилируем закодированные посредством ionCube файлы
			 */
			if (
					(false !== strpos ($content, 'ionCube Loader'))
				&&
					(false === strpos ($content, "ioncube\_loader\_"))
			) {
				continue;
			}


			/*************************************
			 * Начало заплатки
			 *************************************/

			/** @var string $contentBeforeRemovingBom  */
			$contentBeforeRemovingBom = $content;

			df_assert_string ($contentBeforeRemovingBom);


			/** @var string $content  */
			$content = df_text()->bomRemove ($content);

			df_assert_string ($content);


			if ($content !== $contentBeforeRemovingBom) {
				Mage
					::log (
						sprintf (
							'Российская сборка Magento предотвратила сбой компиляции,
							удалив недопустимый символ BOM из файла %s.'
							,
							$file
						)
					)
				;
			}


            $content = ltrim($content, '<?php');

            $content = rtrim($content, "\n\r\t?>");


	        
            $classesSource.=
		        sprintf (
			        "\n\nif (!class_exists ('%s', false) && !(interface_exists ('%s', false))) {\n%s\n}"
		            ,
			        $className
			        ,
			        $className
		            ,
			        $content
		        )
            ;


			/*************************************
			 * Конец заплатки
			 *************************************/

        }
        return $classesSource;
    }
	
	
	
	
	
	/**
	 * @return array
	 */
	private function getConfigMap () {
	
		if (!isset ($this->_configMap)) {
	
			/** @var array $result  */
			$result = $this->getCompileConfig()->getNode('includes')->asArray();
	
			df_assert_array ($result);
	
			$this->_configMap = $result;
		}
	
	
		df_result_array ($this->_configMap);
	
		return $this->_configMap;
	}
	
	
	/**
	* @var array
	*/
	private $_configMap;	
	
	

	
	
	/**
	 * @return string[]
	 */
	private function getScopes () {
	
		if (!isset ($this->_scopes)) {
	
			/** @var string[] $result  */
			$result =
				array_merge (
					array_keys ($this->getConfigMap())
					,
					array_keys (
						$this->getStatFiles()
					)
				)
			;
	
			df_assert_array ($result);
	
			$this->_scopes = $result;
		}
	
	
		df_result_array ($this->_scopes);
	
		return $this->_scopes;
	}
	
	
	/**
	* @var string[]
	*/
	private $_scopes;	
	



	/**
	 * @return string[]
	 */
	private function getStatFiles () {

		if (!isset ($this->_statFiles)) {

			/** @var string[] $result  */
			$result = array ();

			if (is_dir($this->_statDir)) {
				$dir = dir($this->_statDir);
				while (false !== ($file = $dir->read())) {
					if ('.' === ($file[0])) {
						continue;
					}
					$result[str_replace('.csv', '', $file)] = $this->_statDir.DS.$file;
				}
			}

			df_assert_array ($result);

			$this->_statFiles = $result;
		}


		df_result_array ($this->_statFiles);

		return $this->_statFiles;
	}


	/**
	* @var string[]
	*/
	private $_statFiles;

}