<?php

class Df_NovaPoshta_Model_Carrier extends Df_Shipping_Model_Carrier {


	/**
	 * @override
	 * @return string
	 */
	public function getRmId () {
		return self::RM__ID;
	}



	/**
	 * @override
	 * @return bool
	 */
	public function isTrackingAvailable () {
		return true;
	}




	/**
	 * @override
	 * @return string
	 */
	protected function getRmFeatureCode () {
 		return Df_Core_Feature::NOVA_POSHTA;
	}





	const RM__ID = 'nova-poshta';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_NovaPoshta_Model_Carrier';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


