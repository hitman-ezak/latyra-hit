<?php

class Df_NovaPoshta_Model_Request_Locations extends Df_NovaPoshta_Model_Request {


	/**
	 * @return array
	 */
	public function getLocations () {
	
		if (!isset ($this->_locations)) {
	
			/** @var array $result  */
			$result = null;


			/**
			 * Надо бы кэшировать результат
			 */

			/**
			 * Обратите внимание, что не используем в качестве ключа __METHOD__,
			 * потому что данный метод может находиться
			 * в родительском по отношени к другим классе.
			 *
			 * @var string $cacheKey
			 */
			$cacheKey =
				implode (
					'::'
					,
					array (
						get_class ($this)
						,
						__FUNCTION__
					)
				)
			;


			/** @var string|bool $resultSerialized  */
			$resultSerialized =
				$this->getCache()->load (
					$cacheKey
				)
			;

			if (false !== $resultSerialized) {
				$result = @unserialize ($resultSerialized);
			}


			if (!is_array ($result)) {

				/** @var array $locations  */
				$result = $this->parseLocations();

				df_assert_array ($result);

				$resultSerialized = serialize ($result);

				$this->getCache()
					->save (
						$resultSerialized
						,
						$cacheKey
					)
				;
			}
	
			df_assert_array ($result);
	
			$this->_locations = $result;
		}
	
	
		df_result_array ($this->_locations);
	
		return $this->_locations;
	}
	
	
	/**
	* @var array
	*/
	private $_locations;




	/**
	 * @return array
	 */
	private function parseLocations () {

		/** @var array $result  */
		$result = array ();

		/** @var string $pattern */
		$pattern = "#var cities \= \[([^\]]+)\];#mui";

		/** @var array $matches  */
		$matches = array ();


		/** @var int $r */
		$r = preg_match ($pattern, $this->getResponseAsText(), $matches);

		df_assert (1 === $r);


		/** @var string $matchedResult */
		$matchedResult = df_a ($matches, 1);

		df_assert_string ($matchedResult);


		/** @var string $matchedResultTrimmed  */
		$matchedResultTrimmed = df_trim ($matchedResult, "\r\n, ");

		df_assert_string ($matchedResultTrimmed);


		/** @var string $locationsAsJson */
		$locationsAsJson =
			sprintf (
				'[%s]'
				,
				strtr (
					$matchedResultTrimmed
					,
					array (
						'id' => '"id"'
						,
						'value' => '"value"'
					)
				)
			)
		;

		df_assert_string ($locationsAsJson);


		/** @var array $locationsAsAssocArray */
		$locationsAsAssocArray = json_decode ($locationsAsJson, $assoc = true);

		df_assert_array ($locationsAsAssocArray);


		foreach ($locationsAsAssocArray as $locationData) {
			/** @var array $locationData */
			df_assert_array ($locationData);

			/** @var string $locationName */
			$locationName = df_a ($locationData, 'value');

			df_assert_string ($locationName);


			/** @var int $locationId */
			$locationId = intval (df_a ($locationData, 'id'));

			df_assert_integer ($locationId);
			df_assert_between ($locationId, 1);


			/** @var string $locationNameKey */
			$locationNameKey = mb_strtoupper ($locationName);

			df_assert_string ($locationNameKey);

			$result [$locationNameKey]= $locationId;
		}

		df_result_array ($result);

		return $result;
	}





	/**
	 * @return Df_NovaPoshta_Model_Request_Locations
	 */
	public static function i () {

		/** @var Df_NovaPoshta_Model_Request_Locations $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_NovaPoshta_Model_Request_Locations $result  */
			$result = df_model (self::getNameInMagentoFormat());

			df_assert ($result instanceof Df_NovaPoshta_Model_Request_Locations);
		}

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_NovaPoshta_Model_Request_Locations';
	}



	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


