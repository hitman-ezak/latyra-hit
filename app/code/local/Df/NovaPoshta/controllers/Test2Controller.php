<?php

class Df_NovaPoshta_Test2Controller extends Mage_Core_Controller_Front_Action {


	/**
	/**
	 * @return void
	 */
    public function indexAction() {

		try {

			/** @var Df_NovaPoshta_Model_Request_RateAndDeliveryTime $api */
			$api =
				df_model (
					Df_NovaPoshta_Model_Request_RateAndDeliveryTime::getNameInMagentoFormat()
				)
			;

			df_assert ($api instanceof Df_NovaPoshta_Model_Request_RateAndDeliveryTime);

			$this
				->getResponse()
				->setHeader (
					'Content-Type', 'text/plain; charset=UTF-8'
				)
				->setBody (
					print_r (
						array (
							'Срок' => $api->getDeliveryTime()
							,
							'Стоимость' => $api->getRate()
						)
						,
						true
					)
				)
			;
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e, false);
			echo $e->getMessage();
		}

    }

}


