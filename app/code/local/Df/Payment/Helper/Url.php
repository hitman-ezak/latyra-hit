<?php

class Df_Payment_Helper_Url extends Mage_Payment_Helper_Data {


	/**
	 * @return string
	 */
	public function getCheckoutFail () {

		if (!isset ($this->_checkoutFail)) {

			/** @var string $result  */
			$result =
				Mage::getUrl (
					'df-payment/cancel'
					,
					/**
					 * Без _nosid система будет формировать ссылку вида
					 * http://localhost.com:656/df-payment/cancel/?___SID=U,
					 * и тогда Единая Касса неверно вычисляет ЭЦП
					 */
					array ('_nosid' => true)
				)
			;


			df_assert_string ($result);

			$this->_checkoutFail = $result;
		}


		df_result_string ($this->_checkoutFail);

		return $this->_checkoutFail;

	}


	/**
	* @var string
	*/
	private $_checkoutFail;	
	




	/**
	 * @return string
	 */
	public function getCheckoutSuccess () {

		if (!isset ($this->_checkoutSuccess)) {

			/** @var string $result  */
			$result =
				Mage::getUrl (
					'checkout/onepage/success'
					,
					/**
					 * Без _nosid система будет формировать ссылку вида
					 * http://localhost.com:656/checkout/onepage/success/?___SID=U,
					 * и тогда Единая Касса неверно вычисляет ЭЦП
					 */
					array ('_nosid' => true)
				)
			;


			df_assert_string ($result);

			$this->_checkoutSuccess = $result;
		}


		df_result_string ($this->_checkoutSuccess);

		return $this->_checkoutSuccess;

	}


	/**
	* @var string
	*/
	private $_checkoutSuccess;
	



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Helper_Url';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}