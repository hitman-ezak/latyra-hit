<?php

abstract class Df_Payment_Model_Method_WithRedirect extends Df_Payment_Model_Method_Base {



	/**
	 * @override
	 * @return bool
	 */
    public function canCapture() {
        return true;
    }



	/**
	 * @return bool
	 */
    public function canOrder() {
        return true;
    }



	/**
	 * @override
	 * @return string|bool
	 */
	public function getConfigPaymentAction () {

		/** @var bool $result  */
		$result = true;

		df_result_boolean ($result);

		return $result;
	}




    /**
	 * Вызывается из Mage_Checkout_Model_Type_Onepage::saveOrder()
	 *
	 * [code]
            $redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();

            if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
                try {
                    $order->sendNewOrderEmail();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

            // add order information to the session
            $this->_checkoutSession->setLastOrderId($order->getId())
                ->setRedirectUrl($redirectUrl)
                ->setLastRealOrderId($order->getIncrementId());
	 * [/code]
	 *
	 * @override
     * @return string
     */
    public function getOrderPlaceRedirectUrl () {

        $result =
			Mage::getUrl(
				implode (
					Df_Core_Const::T_URL_PATH_SEPARATOR
					,
					array (
						self::RM__ROUTER_NAME
						,
						self::RM__REDIRECT_CONTROLLER_SHORT_NAME
					)
				)
				,
				array (
					'_secure' => true
				)
			)
		;

		df_result_string ($result);

		return $result;
    }



	/**
	 * @return array
	 */
	public function getPaymentPageParams () {

		/** @var array $result  */
		$result = $this->getRequestPayment()->getParams();

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getPaymentPageUrl () {

		/** @var string $result  */
		$result = $this->getRmConfig()->service()->getUrlPaymentPage();

		df_result_string ($result);

		return $result;
	}




    /**
     * Method that will be executed instead of authorize or capture
     * if flag isInitilizeNeeded set to true
     *
     * @param string|bool|null $paymentAction
	 * @param Varien_Object $stateObject
     * @return Mage_Payment_Model_Method_Abstract
     */
	public function initialize ($paymentAction, $stateObject) {

		df_assert ($stateObject instanceof Varien_Object);

		parent::initialize ($paymentAction, $stateObject);


		$stateObject
			->addData (
				array (
					self::INITIALIZE_PARAM__STATE =>
						Mage_Sales_Model_Order::STATE_PENDING_PAYMENT
					,
					self::INITIALIZE_PARAM__STATUS =>
						Mage_Sales_Model_Order::STATE_PENDING_PAYMENT
					,
					self::INITIALIZE_PARAM__IS_NOTIFIED =>
						false
				)
			)
		;

		return $this;
	}



	/**
	 * @override
	 * @return bool
	 */
	public function isInitializeNeeded() {

		/** @var bool $result  */
		$result = true;

		df_result_boolean ($result);

		return $result;
	}



	const INITIALIZE_PARAM__STATE = 'state';
	const INITIALIZE_PARAM__STATUS = 'status';
	const INITIALIZE_PARAM__IS_NOTIFIED = 'is_notified';


	const RM__ROUTER_NAME = 'df-payment';
	const RM__REDIRECT_CONTROLLER_SHORT_NAME = 'redirect';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Method_WithRedirect';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


