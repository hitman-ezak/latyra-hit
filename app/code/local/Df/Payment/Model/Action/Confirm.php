<?php


abstract class Df_Payment_Model_Action_Confirm extends Df_Payment_Model_Action_Abstract {
	


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getSignatureFromOwnCalculations ();



	/**
	 * Вынуждены делать метод абстрактным.
	 * Использовать getConst нельзя из-за рекурсии.
	 *
	 * @abstract
	 * @return string
	 */
	abstract protected function getRequestKeyOrderIncrementId ();



	/**
	 * @abstract
	 * @param Exception $e
	 * @return string
	 */
	abstract protected function getResponseTextForError (Exception $e);



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getResponseTextForSuccess ();




	/**
	 * @return Df_Payment_Model_Action_Confirm
	 */
	public function process () {

		try {

			/**
			 * Флаг Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
			 * предназначен для отслеживания возвращения покупателя
			 * с сайта платёжной системы без оплаты.
			 *
			 * Если этот флаг установлен — значит, покупатель был перенаправлен
			 * на сайт платёжной системы.
			 */
			df_helper()->checkout()->sessionSingleton()
				->unsetData (
					Df_Checkout_Const::SESSION_PARAM__RM__REDIRECTED_TO_PAYMENT_SYSTEM
				)
			;
			
			$this->getResponse ()
				->setHeader (
					Zend_Http_Client::CONTENT_TYPE
					,
					$this->getResponseContentType ()
				)
			;

			$this->checkSignature ();

			if (!$this->isItPreliminaryNotification() && !$this->getOrder()->canInvoice()) {

				/**
				 * Бывают платёжные системы (например, «Единая касса»),
				 * которые, согласно их документации,
				 * могут несколько раз присылать подтверждение оплаты покупателем
				 * одного и того же заказа.
				 *
				 * Так вот, данная проверка гарантирует, что платёжный модуль не будет пытаться
				 * принять повторно оплату за уже оплаченный заказ.
				 *
				 * Обратите внимание, что проверку заказа на оплаченность
				 * надо сделать до вызова метода checkPaymentAmount,
				 * потому что иначе требуемая к оплате сумма будет равна нулю,
				 * и checkPaymentAmount будет сравнивать сумму от платёжной системы с нулём.
				 */
				$this->processOrderCanNotInvoice ();
			}
			else {

				$this->checkPaymentAmount();

				if ($this->isItPreliminaryNotification()) {
					$this->processPreliminaryNotification();
				}
				else {

					/** @var Mage_Sales_Model_Order_Invoice $invoice */
					$invoice = $this->getOrder ()->prepareInvoice();

					df_assert ($invoice instanceof Mage_Sales_Model_Order_Invoice);


					$invoice->register();

					$invoice->capture();



					/** @var Mage_Core_Model_Resource_Transaction $transaction */
					$transaction =
						df_model (
							Df_Core_Const::CORE_RESOURCE_TRANSACTION_CLASS_MF
						)
					;

					df_assert ($transaction instanceof Mage_Core_Model_Resource_Transaction);



					$transaction
						->addObject ($invoice)
						->addObject ($invoice->getOrder())
						->save ()
					;



					$this->getOrder ()
						->setState (
							Mage_Sales_Model_Order::STATE_PROCESSING
							,
							Mage_Sales_Model_Order::STATE_PROCESSING
							,
							sprintf (
								$this->getMessage (self::CONFIG_KEY__MESSAGE__SUCCESS)
								,
								$invoice->getIncrementId()
							)
							,
							true
						)
					;

					$this->getOrder ()->save();
					$this->getOrder ()->sendNewOrderEmail();
				}


				$this->processResponseForSuccess ();

			}
		}

		catch (Exception $e) {

			$this->processException ($e);
		}


		return $this;
	}




	/**
	 * @return Df_Payment_Model_Action_Confirm
	 * @throws Mage_Core_Exception
	 */
	protected function checkPaymentAmount () {

			df_assert (
					$this->getRequestValuePaymentAmount()->getAsString()
				===
					$this->getPaymentAmountFromOrder()->getAsString()
				,
				sprintf (
					$this->getMessage (self::CONFIG_KEY__MESSAGE__INVALID__PAYMENT_AMOUNT)
					,
					$this->getPaymentAmountFromOrder()->getAsString()
					,
					$this->getServiceConfig()->getCurrencyCode()
					,
					$this->getRequestValuePaymentAmount()->getAsString()
					,
					$this->getServiceConfig()->getCurrencyCode()
				)
			)
		;

		return $this;
	}




	/**
	 * @return Df_Payment_Model_Action_Confirm
	 * @throws Mage_Core_Exception
	 */
	protected function checkSignature () {

		if (
			!df_text()->areEqualCI (
				$this->getSignatureFromOwnCalculations()
				,
				$this->getRequestValueSignature()
			)
		)  {
			df_error (
				$this->getMessage (self::CONFIG_KEY__MESSAGE__INVALID__SIGNATURE)
			);
		}

		return $this;
	}



	/**
	 * @param string $configKey
	 * @return string
	 */
	protected function getMessage ($configKey) {

		df_param_string ($configKey, 0);

		/** @var string $result  */
		$result =
			str_replace (
				'\n'
				,
				"<br/>"
				,
				$this->getConst ($configKey)
			)
		;


		df_result_string ($result);

		return $result;
	}





	/**
	 * @override
	 * @return Mage_Sales_Model_Order
	 */
	protected function getOrder () {

		if (!isset ($this->_order)) {


			/** @var Mage_Sales_Model_Order $result  */
			$result =
				df_model (
					Df_Sales_Const::ORDER_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Sales_Model_Order);


			$result
				->loadByIncrementId (
					$this->getRequestValueOrderIncrementId()
				)
			;


			df_assert (!is_null ($result->getId ()));


			$this->_order = $result;
		}


		df_assert ($this->_order instanceof Mage_Sales_Model_Order);

		return $this->_order;

	}


	/**
	* @var Mage_Sales_Model_Order
	*/
	private $_order;



	/**
	 * @return Df_Core_Model_Money
	 */
	protected function getPaymentAmountFromOrder () {

		if (!isset ($this->_paymentAmountFromOrder)) {

			/** @var Df_Core_Model_Money $result  */
			$result =
				$this->getServiceConfig()->getOrderAmountInServiceCurrency (
					$this->getOrder()
				)
			;


			df_assert ($result instanceof Df_Core_Model_Money);

			$this->_paymentAmountFromOrder = $result;
		}


		df_assert ($this->_paymentAmountFromOrder instanceof Df_Core_Model_Money);

		return $this->_paymentAmountFromOrder;

	}


	/**
	* @var Df_Core_Model_Money
	*/
	private $_paymentAmountFromOrder;




	/**
	 * @return Df_Payment_Model_Config_Area_Service
	 */
	protected function getServiceConfig () {

		/** @var Df_Payment_Model_Config_Area_Service $result  */
		$result = $this->getPaymentMethod()->getRmConfig()->service();

		df_assert ($result instanceof Df_Payment_Model_Config_Area_Service);

		return $result;
	}





	/**
	 * @return Zend_Controller_Request_Abstract
	 */
	protected function getRequest () {

		/** @var Zend_Controller_Request_Abstract $result  */
		$result = Mage::app()->getRequest();

		df_assert ($result instanceof Zend_Controller_Request_Abstract);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getRequestKeyCustomerEmail () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__CUSTOMER__EMAIL);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getRequestKeyCustomerName () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__CUSTOMER__NAME);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestKeyCustomerPhone () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__CUSTOMER__PHONE);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestKeyPaymentAmount () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT__AMOUNT);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getRequestKeyPaymentCurrencyCode () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT__CURRENCY_CODE);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getRequestKeyPaymentTest () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT__TEST);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getRequestKeyServicePaymentDate () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT_SERVICE__PAYMENT__DATE);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestKeyServicePaymentId () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT_SERVICE__PAYMENT__ID);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestKeyServicePaymentState () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT_SERVICE__PAYMENT__STATE);

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getRequestKeyShopId () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__PAYMENT_SERVICE__SHOP__ID);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestKeySignature () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__REQUEST__SIGNATURE);

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueCustomerEmail () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyCustomerEmail ()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueCustomerName () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyCustomerName ()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueOrderCustomerPhone () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyCustomerPhone ()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueOrderIncrementId () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyOrderIncrementId()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return Df_Core_Model_Money
	 */
	protected function getRequestValuePaymentAmount () {

		if (!isset ($this->_requestValuePaymentAmount)) {

			/** @var Df_Core_Model_Money $result  */
			$result =
				df_model (
					Df_Core_Model_Money::getNameInMagentoFormat()
					,
					array (
						Df_Core_Model_Money::PARAM__AMOUNT =>
							floatval (
								$this->getRequestValuePaymentAmountAsString ()
							)
					)
				)
			;


			df_assert ($result instanceof Df_Core_Model_Money);

			$this->_requestValuePaymentAmount = $result;
		}


		df_assert ($this->_requestValuePaymentAmount instanceof Df_Core_Model_Money);

		return $this->_requestValuePaymentAmount;

	}


	/**
	* @var Df_Core_Model_Money
	*/
	private $_requestValuePaymentAmount;




	/**
	 * @return string
	 */
	protected function getRequestValuePaymentAmountAsString () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyPaymentAmount()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValuePaymentCurrencyCode () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyPaymentCurrencyCode ()
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getRequestValuePaymentTest () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyPaymentTest()
			)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getRequestValueServicePaymentDate () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyServicePaymentDate ()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueServicePaymentId () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyServicePaymentId()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueServicePaymentState () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyServicePaymentState()
			)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getRequestValueShopId () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeyShopId()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	protected function getRequestValueSignature () {

		/** @var string $result  */
		$result =
			$this->getRequest()->getParam (
				$this->getRequestKeySignature()
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getResponsePassword () {

		/** @var string $result  */
		$result = $this->getServiceConfig()->getResponsePassword();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	protected function isItPreliminaryNotification () {

		/** @var bool $result  */
		$result = false;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @param Exception $e
	 * @return Df_Payment_Model_Action_Confirm
	 */
	protected function processException (Exception $e) {

		df_handle_entry_point_exception ($e, false);

		df_log (
			implode (
				"\n\n"
				,
				array (
					'Дополнительная информация о сбое при оплате:'
					,
					print_r ($this->getRequest()->getParams(), true)
				)
			)
		);

		if (!is_null ($this->_order)) {

			$this->getOrder()
				->addStatusHistoryComment(
					$e->getMessage()
				)
			;

			$this->getOrder()
				->setData (
					Df_Sales_Const::ORDER_PARAM__IS_CUSTOMER_NOTIFIED
					,
					false
				)
			;

			$this->getOrder()->save();
		}


		$this->getResponse ()
			->setBody (
				$this->getResponseTextForError($e)
			)
		;

		return $this;
	}



	/**
	 * @return Df_Payment_Model_Action_Confirm
	 * @throws Mage_Core_Exception
	 */
	protected function processOrderCanNotInvoice () {

		/**
		 * Потомки могут перекрывать это поведение.
		 * Так делает Единая Касса.
		 */
		df_error ('Платёжная система зачем-то повторно прислала оповещение об оплате.');

		return $this;
	}



	/**
	 * @return Df_Payment_Model_Action_Confirm
	 */
	protected function processPreliminaryNotification () {

		return $this;
	}





	/**
	 * @return Df_Payment_Model_Action_Confirm
	 */
	protected function processResponseForSuccess () {

		$this->getResponse ()
			->setBody (
				$this->getResponseTextForSuccess ()
			)
		;

		return $this;
	}





	/**
	 * @return string
	 */
	private function getResponseContentType () {

		/** @var string $result  */
		$result = $this->getConst (self::CONFIG_KEY__RESPONSE__CONTENT_TYPE);

		df_result_string ($result);

		return $result;
	}





	const CONFIG_KEY__ADMIN__ORDER__INCREMENT_ID = 'admin/order/increment-id';

	const CONFIG_KEY__CUSTOMER__EMAIL = 'customer/email';
	const CONFIG_KEY__CUSTOMER__NAME = 'customer/name';
	const CONFIG_KEY__CUSTOMER__PHONE = 'customer/phone';

	const CONFIG_KEY__MESSAGE__INVALID__ORDER = 'message/invalid/order';
	const CONFIG_KEY__MESSAGE__INVALID__PAYMENT_AMOUNT = 'message/invalid/payment-amount';
	const CONFIG_KEY__MESSAGE__INVALID__SIGNATURE = 'message/invalid/signature';
	const CONFIG_KEY__MESSAGE__SUCCESS = 'message/success';

	const CONFIG_KEY__PAYMENT__AMOUNT = 'payment/amount';
	const CONFIG_KEY__PAYMENT__CURRENCY_CODE = 'payment/currency-code';
	const CONFIG_KEY__PAYMENT__TEST = 'payment/test';

	const CONFIG_KEY__PAYMENT_SERVICE__PAYMENT__DATE = 'payment_service/payment/date';
	const CONFIG_KEY__PAYMENT_SERVICE__PAYMENT__ID = 'payment_service/payment/id';
	const CONFIG_KEY__PAYMENT_SERVICE__PAYMENT__STATE = 'payment_service/payment/state';
	const CONFIG_KEY__PAYMENT_SERVICE__SHOP__ID = 'payment_service/shop/id';

	const CONFIG_KEY__REQUEST__SIGNATURE = 'request/signature';

	const CONFIG_KEY__RESPONSE__CONTENT_TYPE = 'response/content-type';







	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Action_Confirm';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


