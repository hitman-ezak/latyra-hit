<?php

abstract class Df_Payment_Model_Config_Abstract extends Df_Core_Model_Abstract {


	/**
	 * @param string $key
	 * @return bool
	 */
	public function canProcessUncertainKey ($key) {

		df_param_string ($key, 0);

		/** @var bool $result  */
		$result =
			in_array (
				$key
				,
				$this->getUncertainKeys()
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getConst (
		$key
		,
		$canBeTest = true
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $result  */
		$result = $this->getConstManager()->getValue ($key, $canBeTest, $defaultValue);


		df_result_string ($result);

		return $result;
	}



	/**
	 * @return Df_Payment_Model_ConfigManager_Const
	 */
	public function getConstManager () {

		/** @var Df_Payment_Model_ConfigManager_Const $result  */
		$result = $this->cfg (self::PARAM__CONST_MANAGER);

		df_assert ($result instanceof Df_Payment_Model_ConfigManager_Const);

		return $result;
	}




	/**
	 * Получаем заданное ранее администратором
	 * значение конкретной настройки платёжного способа
	 *
	 * @param string $key
	 * @param mixed $defaultValue [optional]
	 * @return mixed
	 */
	public function getVar ($key, $defaultValue = null) {

		df_param_string ($key, 0);

		/** @var mixed $result  */
		$result = $this->getVarManager()->getValue($key, $defaultValue);

		return $result;
	}




	/**
	 * @return Df_Payment_Model_ConfigManager_Var
	 */
	public function getVarManager () {

		/** @var Df_Payment_Model_ConfigManager_Var $result  */
		$result = $this->cfg (self::PARAM__VAR_MANAGER);

		df_assert ($result instanceof Df_Payment_Model_ConfigManager_Var);

		return $result;
	}



	/**
	 * @param  string $paramName
	 * @return string
	 */
	protected function decrypt ($value) {

		/** @var string $result  */
		$result =
			df_mage()->coreHelper()->decrypt (
				$value
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * Стандартные параметры, которые ядро Magento запрашивает через getConfigData.
	 * Например: «sort_order».
	 *
	 * У наших модулей все свойства имеют приставку в соответствии с областью настроек.
	 * Например, «frontend__sort_order».
	 *
	 * Посредством метода getUncertainKeys область настроек указывает стандартные ключи,
	 * которые она в состоянии обрабатывать.
	 *
	 * @return array
	 */
	protected function getUncertainKeys () {

		/** @var array $result  */
		$result = array ();

		df_result_array ($result);

		return $result;
	}



	/**
	 * @param  string $value
	 * @return bool
	 */
	protected function parseYesNo ($value) {

		df_assert_string ($value, 0);

		/** @var bool $result */
		$result = df_cfg()->parseYesNo ($value);

		df_result_boolean ($result);

		return $result;
	}





	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__CONST_MANAGER, Df_Payment_Model_ConfigManager_Const::getClass()
			)
			->validateClass (
				self::PARAM__VAR_MANAGER, Df_Payment_Model_ConfigManager_Var::getClass()
			)
		;
	}



	const PARAM__CONST_MANAGER = 'const_manager';
	const PARAM__VAR_MANAGER = 'var_manager';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


