<?php

/**
 * @singleton
 * В этом классе нельзя кешировать результаты вычислений!
 */
class Df_Payment_Model_Config_Source_PaymentPage_Locale extends Df_Payment_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
	protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result =
			$this->getPaymentMethod()->getRmConfig()->service()
				->getAllowedLocalesAsOptionArray()
		;


		df_result_array ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Source_PaymentPage_Language';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


