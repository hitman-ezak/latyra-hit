<?php

class Df_Payment_Model_Config_Source_Service_FeePayer extends Df_Payment_Model_Config_Source {


	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
	protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result =
			array (
				array (
					self::OPTION_KEY__VALUE => self::VALUE__SHOP
					,
					self::OPTION_KEY__LABEL =>
						df_helper()->payment()->__ ('магазин')
				)
				,
				array (
					self::OPTION_KEY__VALUE => self::VALUE__BUYER
					,
					self::OPTION_KEY__LABEL =>
						df_helper()->payment()->__ ('покупатель')
				)
			)
		;


		df_result_array ($result);

		return $result;
	}


	const VALUE__SHOP = 'shop';
	const VALUE__BUYER = 'buyer';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Source_Service_FeePayer';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


