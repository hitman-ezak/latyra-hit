<?php

abstract class Df_Payment_Model_Config_Area_Abstract
	extends Df_Payment_Model_Config_Abstract {



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getAreaPrefix ();



	/**
	 * Получаем заданное ранее администратором
	 * значение конкретной настройки платёжного способа
	 *
	 * @override
	 * @param string $key
	 * @param mixed $defaultValue [optional]
	 * @return mixed
	 */
	public function getVar ($key, $defaultValue = null) {

		df_param_string ($key, 0);

		/** @var mixed $result  */
		$result =
			parent::getVar (
				$this->preprocessKey (
					$key
				)
				,
				$defaultValue
			)
		;

		return $result;
	}



	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKey ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			implode (
				'__'
				,
				df_clean (
					array (
						$this->getAreaPrefix()
						,
						$key
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Area_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


