<?php

class Df_Payment_Model_Config_Area_Admin
	extends Df_Payment_Model_Config_Area_Abstract {



	/**
	 * @override
	 * @return string
	 */
	protected function getAreaPrefix () {

		/** @var string $result  */
		$result = self::AREA_PREFIX;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @override
	 * @return array
	 */
	protected function getUncertainKeys () {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getUncertainKeys()
				,
				array (
 					'order_status'
					,
					'payment_action'
				)
			)
		;

		df_result_array ($result);

		return $result;
	}




	const AREA_PREFIX = 'admin';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Area_Admin';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


