<?php

/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */

abstract class Df_Payment_Model_Config_Source extends Df_Admin_Model_Config_Source {



	/**
	 * @return Df_Payment_Model_Method_Base
	 */
	protected function getPaymentMethod () {

		/** @var Df_Payment_Model_Method_Base $result  */
		$result =
			df_mage()->paymentHelper()->getMethodInstance (
				$this->getPaymentMethodCode()
			)
		;

		df_assert ($result instanceof Df_Payment_Model_Method_Base);

		return $result;
	}


	
	
	
	/**
	 * @return string
	 */
	private function getPaymentMethodCode () {
	
		/** @var string $result  */
		$result =
			Df_Payment_Model_Method_Base::getCodeByRmId (
				$this->getPaymentMethodRmId ()
			)
		;

		df_assert_string ($result);
	
		return $result;
	}




	/**
	 * @return string
	 */
	private function getPaymentMethodRmId () {

		/** @var string $result  */
		$result = df_a ($this->getPathExploded(), 1);

		df_result_string ($result);

		return $result;
	}
	
	



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_Config_Source';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


