<?php

class Df_Payment_Model_ConfigManager_Const extends Df_Core_Model_Abstract {



	/**
	 * @return array
	 */
	public function getAllowedCurrencyCodes () {

		if (!isset ($this->_allowedCurrencyCodes)) {

			/** @var array $result  */
			$result =
				df_parse_csv (
					$this->getAllowedCurrencyCodesAsString()
				)
			;


			df_assert_array ($result);

			$this->_allowedCurrencyCodes = $result;
		}


		df_result_array ($this->_allowedCurrencyCodes);

		return $this->_allowedCurrencyCodes;

	}


	/**
	* @var array
	*/
	private $_allowedCurrencyCodes;
	
	
	
	
	
	
	
	/**
	 * @return array
	 */
	public function getAllowedLocaleCodes () {

		if (!isset ($this->_allowedLocaleCodes)) {

			/** @var array $result  */
			$result =
				df_parse_csv (
					$this->getAllowedLocaleCodesAsString()
				)
			;


			df_assert_array ($result);

			$this->_allowedLocaleCodes = $result;
		}


		df_result_array ($this->_allowedLocaleCodes);

		return $this->_allowedLocaleCodes;

	}


	/**
	* @var array
	*/
	private $_allowedLocaleCodes;	
	
	



	/**
	 * @return array
	 */
	public function getAvailablePaymentMethodsAsCanonicalConfigArray () {

		if (!isset ($this->_availablePaymentMethodsAsCanonicalConfigArray)) {


			/** @var string $configKey  */
			$configKey =
				df()->config()->implodeKey (
					array (
						self::KEY__PAYMENT_METHODS
					)
				)
			;

			df_assert_string ($configKey);


			/** @var Mage_Core_Model_Config_Element|null $node */
			$node =
				$this->getNode (
					$configKey
					,
					false
				)
			;


			/** @var array $result  */
			$result =
					is_null ($node)
				?
					array ()
				:
					$node->asCanonicalArray()
			;

			df_assert_array ($result);

			$this->_availablePaymentMethodsAsCanonicalConfigArray = $result;
		}


		df_result_array ($this->_availablePaymentMethodsAsCanonicalConfigArray);

		return $this->_availablePaymentMethodsAsCanonicalConfigArray;

	}


	/**
	* @var array
	*/
	private $_availablePaymentMethodsAsCanonicalConfigArray;




	/**
	 * Способы оплаты, предоставляемые данной платёжной системой
	 * @return array
	 */
	public function getAvailablePaymentMethodsAsOptionArray () {

		if (!isset ($this->_availablePaymentMethodsAsOptionArray)) {

			/** @var array $result  */
			$result = array ();


			foreach ($this->getAvailablePaymentMethodsAsCanonicalConfigArray ()
				as $methodCode => $methodOptions) {

				/** @var string $methodCode */
				/** @var array $methodOptions */

				df_assert_string ($methodCode);
				df_assert_array ($methodOptions);


				/** @var string $methodTitle */
				$methodTitle =
					df_a ($methodOptions, self::KEY__TITLE)
				;

				df_assert_string ($methodTitle);


				$result []=
					array (
						Df_Admin_Model_Config_Source::OPTION_KEY__LABEL => $methodTitle
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE => $methodCode
					)
				;

			}


			df_assert_array ($result);

			$this->_availablePaymentMethodsAsOptionArray = $result;
		}


		df_result_array ($this->_availablePaymentMethodsAsOptionArray);

		return $this->_availablePaymentMethodsAsOptionArray;

	}


	/**
	* @var array
	*/
	private $_availablePaymentMethodsAsOptionArray;






	/**
	 * @param string $requestVar
	 * @return int
	 */
	public function getRequestVarMaxLength ($requestVar) {

		df_param_string ($requestVar, 0);

		if (!isset ($this->_requestVarMaxLength[$requestVar])) {

			/** @var int $result  */
			$result =
				intval (
					$this->getValue (
						df()->config()->implodeKey (
							array (
								self::KEY__REQUEST
								,
								self::KEY__PAYMENT_PAGE
								,
								self::KEY__PARAMS
								,
								$requestVar
								,
								self::KEY__MAX_LENGTH
							)
						)
						,
						false
					)
				)
			;

			df_assert_integer ($result);

			$this->_requestVarMaxLength[$requestVar] = $result;
		}


		df_result_integer ($this->_requestVarMaxLength[$requestVar]);

		return $this->_requestVarMaxLength[$requestVar];

	}


	/**
	* @var int[]
	*/
	private $_requestVarMaxLength = array ();



	
	
	
	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @return Mage_Core_Model_Config_Element|null
	 */
	public function getNode (
		$key
		,
		$canBeTest = true
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);


		if ($canBeTest) {
			$key =
				df()->config()->implodeKey (
					array (
							$this->getPaymentMethod()->isTestMode()
						?
							self::KEY__TEST
						:
							self::KEY__PRODUCTION
						,
						$key
					)
				)
			;
		}

	
		if (!isset ($this->_node[$key][$canBeTest])) {
	
			/** @var Mage_Core_Model_Config_Element|null $result  */
			$result =
				df()->config()->getNodeByKey (
					$this->preprocessKey (
						$key
					)
				)
			;


			if (is_null ($result)) {

				/**
				 * Пробуем получить стандартное значение параметра:
				 * из настроек модуля Df_Payment
				 */

				$result =
					df()->config()->getNodeByKey (
						$this->preprocessKeyDefault (
							$key
						)
					)
				;

			}


			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Core_Model_Config_Element);
			}

	
			$this->_node[$key][$canBeTest] = $result;
		}
	
		if (!is_null ($this->_node[$key][$canBeTest])) {
			df_assert ($this->_node[$key][$canBeTest] instanceof Mage_Core_Model_Config_Element);
		}
	
		return $this->_node[$key][$canBeTest];
	}
	
	
	/**
	* @var mixed[]
	*/
	private $_node = array ();




	/**
	 * @param string $key
	 * @param bool $canBeTest [optional]
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getUrl (
		$key
		,
		$canBeTest = true
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $result  */
		$result =
			$this->getValue (
				df()->config()->implodeKey (
					array (
						self::KEY__URL
						,
						$key
					)
				)
				,
				$canBeTest
			)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @param string $key    
	 * @param bool $canBeTest
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	public function getValue (
		$key
		,
		$canBeTest
		,
		$defaultValue = Df_Core_Const::T_EMPTY
	) {

		df_param_string ($key, 0);
		df_param_boolean ($canBeTest, 1);
		df_param_string ($defaultValue, 2);


		/** @var string $result  */
		$result =
			df()->config()->getNodeValueAsString (
				$this->getNode ($key, $canBeTest)
			)
		;

		if (df_empty ($result)) {

			if ($canBeTest) {

				/**
				 * Пробуем получить значение без приставок test/production
				 */
				$result =
					df()->config()->getNodeValueAsString (
						$this->getNode ($key, !$canBeTest)
					)
				;

			}
		}


		if (df_empty ($result)) {
			$result = $defaultValue;
		}

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function hasCurrencySetRestriction () {

		if (!isset ($this->_currencySetRestriction)) {

			/** @var bool $result  */
			$result =
				!df_empty (
					$this->getAllowedCurrencyCodes()
				)
			;


			df_assert_boolean ($result);

			$this->_currencySetRestriction = $result;
		}


		df_result_boolean ($this->_currencySetRestriction);

		return $this->_currencySetRestriction;

	}


	/**
	* @var bool
	*/
	private $_currencySetRestriction;




	/**
	 * Переводит код валюты из стандарта Magento в стандарт платёжной системы.
	 *
	 * Обратите внимание, что конкретный платёжный модуль
	 * использует либо метод translateCurrencyCode, либо метод translateCurrencyCodeReversed,
	 * но никак не оба вместе!
	 *
	 * Например, модуль WebMoney использует метод translateCurrencyCodeReversed,
	 * потому что кодам в формате платёжной системы «U» и «D»
	 * соответствует единый код в формате Magento — «USD» — и использование translateCurrencyCode
	 * просто невозможно в силу необдозначности перевода «USD» (неясно, переводить в «U» или в «D»).
	 *
	 * @param string $currencyCodeInMagentoFormat
	 * @return string
	 */
	public function translateCurrencyCode ($currencyCodeInMagentoFormat) {

		df_param_string ($currencyCodeInMagentoFormat, 0);

		if (!isset ($this->_currencyCodesTranslationMap[$currencyCodeInMagentoFormat])) {

			/** @var string $result  */
			$result =
				$this->getValue (
					df()->config()->implodeKey (
						array (
							self::KEY__CURRENCIES
							,
							self::KEY__CODE_TRANSLATION
							,
							$currencyCodeInMagentoFormat
						)
					)
					,
					false
				)
			;

			if (df_empty ($result)) {
				$result = $currencyCodeInMagentoFormat;
			}

			df_assert_string ($result);

			$this->_currencyCodesTranslationMap[$currencyCodeInMagentoFormat] = $result;
		}


		df_result_string ($this->_currencyCodesTranslationMap[$currencyCodeInMagentoFormat]);

		return $this->_currencyCodesTranslationMap[$currencyCodeInMagentoFormat];

	}


	/**
	* @var string[]
	*/
	private $_currencyCodesTranslationMap = array ();





	/**
	 * Переводит код валюты из стандарта платёжной системы в стандарт Magento.
	 *
	 * Обратите внимание, что конкретный платёжный модуль
	 * использует либо метод translateCurrencyCode, либо метод translateCurrencyCodeReversed,
	 * но никак не оба вместе!
	 *
	 * Например, модуль WebMoney использует метод translateCurrencyCodeReversed,
	 * потому что кодам в формате платёжной системы «U» и «D»
	 * соответствует единый код в формате Magento — «USD» — и использование translateCurrencyCode
	 * просто невозможно в силу необдозначности перевода «USD» (неясно, переводить в «U» или в «D»).
	 *
	 * @param string $currencyCodeInMagentoFormat
	 * @return string
	 */
	public function translateCurrencyCodeReversed ($currencyCodeInPaymentSystemFormat) {

		df_param_string ($currencyCodeInPaymentSystemFormat, 0);

		if (!isset ($this->_currencyCodesReversedTranslationMap[$currencyCodeInPaymentSystemFormat])) {

			/** @var string $result  */
			$result =
				$this->getValue (
					df()->config()->implodeKey (
						array (
							self::KEY__CURRENCIES
							,
							self::KEY__CODE_TRANSLATION_REVERSED
							,
							$currencyCodeInPaymentSystemFormat
						)
					)
					,
					false
				)
			;

			if (df_empty ($result)) {
				$result = $currencyCodeInPaymentSystemFormat;
			}

			df_assert_string ($result);

			$this->_currencyCodesReversedTranslationMap[$currencyCodeInPaymentSystemFormat] = $result;
		}


		df_result_string ($this->_currencyCodesReversedTranslationMap[$currencyCodeInPaymentSystemFormat]);

		return $this->_currencyCodesReversedTranslationMap[$currencyCodeInPaymentSystemFormat];

	}


	/**
	* @var string[]
	*/
	private $_currencyCodesReversedTranslationMap = array ();
	
	
	
	
	
	
	
	/**
	 * Переводит код локали из стандарта Magento в стандарт платёжной системы
	 * @param string $localeCodeInMagentoFormat
	 * @return string
	 */
	public function translateLocaleCode ($localeCodeInMagentoFormat) {

		df_param_string ($localeCodeInMagentoFormat, 0);

		if (!isset ($this->_localeCodesTranslationMap[$localeCodeInMagentoFormat])) {

			/** @var string $result  */
			$result =
				$this->getValue (
					df()->config()->implodeKey (
						array (
							self::KEY__LOCALES
							,
							self::KEY__CODE_TRANSLATION
							,
							$localeCodeInMagentoFormat
						)
					)
					,
					false
				)
			;

			if (df_empty ($result)) {
				$result = $localeCodeInMagentoFormat;
			}

			df_assert_string ($result);

			$this->_localeCodesTranslationMap[$localeCodeInMagentoFormat] = $result;
		}


		df_result_string ($this->_localeCodesTranslationMap[$localeCodeInMagentoFormat]);

		return $this->_localeCodesTranslationMap[$localeCodeInMagentoFormat];

	}


	/**
	* @var string[]
	*/
	private $_localeCodesTranslationMap = array ();	
	
	





	/**
	 * @return Df_Payment_Model_Method_Base
	 */
	protected function getPaymentMethod () {

		/** @var Df_Payment_Model_Method_Base $result  */
		$result = $this->cfg (self::PARAM__PAYMENT_METHOD);

		df_assert ($result instanceof Df_Payment_Model_Method_Base);

		return $result;
	}




	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKey ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					self::KEY__BASE
					,
					$this->getPaymentMethod()->getRmId()
					,
					$key
				)
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKeyDefault ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					self::KEY__BASE
					,
					self::KEY__DEFAULT
					,
					$key
				)
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getAllowedCurrencyCodesAsString () {

		if (!isset ($this->_allowedCurrencyCodesAsString)) {

			/** @var string $result  */
			$result =
				$this->getValue (
					df()->config()->implodeKey (
						array (
							self::KEY__CURRENCIES
							,
							self::KEY__ALLOWED
						)
					)
					,
					false
				)
			;


			df_assert_string ($result);

			$this->_allowedCurrencyCodesAsString = $result;
		}


		df_result_string ($this->_allowedCurrencyCodesAsString);

		return $this->_allowedCurrencyCodesAsString;

	}


	/**
	* @var string
	*/
	private $_allowedCurrencyCodesAsString;
	
	
	
	
	
	
	
	/**
	 * @return string
	 */
	private function getAllowedLocaleCodesAsString () {

		if (!isset ($this->_allowedLocaleCodesAsString)) {

			/** @var string $result  */
			$result =
				$this->getValue (
					df()->config()->implodeKey (
						array (
							self::KEY__LOCALES
							,
							self::KEY__ALLOWED
						)
					)
					,
					false
				)
			;


			df_assert_string ($result);

			$this->_allowedLocaleCodesAsString = $result;
		}


		df_result_string ($this->_allowedLocaleCodesAsString);

		return $this->_allowedLocaleCodesAsString;

	}


	/**
	* @var string
	*/
	private $_allowedLocaleCodesAsString;	
	




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__PAYMENT_METHOD, Df_Payment_Model_Method_Base::getClass()
			)
		;
	}




	const KEY__ALLOWED = 'allowed';
	const KEY__BASE = 'df/payment';
	const KEY__CODE_TRANSLATION = 'code-translation';
	const KEY__CODE_TRANSLATION_REVERSED = 'code-translation-reversed';
	const KEY__CURRENCIES = 'currencies';
	const KEY__DEFAULT = 'default';
	const KEY__LOCALES = 'locales';
	const KEY__PAYMENT_METHODS = 'payment-methods';
	const KEY__PRODUCTION = 'production';
	const KEY__TEST = 'test';
	const KEY__TITLE = 'title';
	const KEY__URL = 'url';



	/**
	 * Для getRequestVarMaxLength()
	 */
	const KEY__MAX_LENGTH = 'max_length';
	const KEY__PARAMS = 'params';
	const KEY__PAYMENT_PAGE = 'payment_page';
	const KEY__REQUEST = 'request';


	const PARAM__PAYMENT_METHOD = 'payment_method';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Model_ConfigManager_Const';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


