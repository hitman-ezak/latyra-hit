<?php

class Df_Payment_Block_Redirect extends Mage_Page_Block_Redirect {


	/**
	 * @override
	 * @return array
	 */
	public function getFormFields () {

		/** @var array $result  */
		$result = $this->getPaymentMethod()->getPaymentPageParams ();

		df_result_array ($result);

		return $result;
	}



	/**
	 * @override
	 * @return string
	 */
	public function getFormId () {

		/** @var string $result  */
		$result = get_class ($this);

		df_result_string ($result);

		return $result;
	}
	
	

	
	
	/**                    
	 * @override
	 * @return string
	 */
	public function getHtmlFormRedirect () {

		/** @var string $result  */
		$result = Df_Core_Const::T_EMPTY;

		try {
			$result =
				implode (
					Df_Core_Const::T_EMPTY
					,
					array (
						$this->getForm()->toHtml()
						,
						sprintf (
							'<script type="text/javascript">document.getElementById("%s").submit();</script>'
							,
							$this->getFormId()
						)
					)
				)
			;
		}
		catch (Exception $e) {
			df_handle_entry_point_exception ($e, true);
		}

	
		df_result_string ($result);
	
		return $result;
	}
	



	/**
	 * @override
	 * @return string
	 */
	public function getMethod () {

		/** @var string $result  */
		$result =
			$this->getPaymentMethod()->getConst(
				'request/method'
				,
				false
				,
				Zend_Form::METHOD_POST
			)
		;

		df_result_string ($result);

		return $result;
	}
	
	



	/**
	 * @override
	 * @return string
	 */
	public function getTargetURL () {

		/** @var string $result  */
		$result = $this->getPaymentMethod()->getPaymentPageUrl();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return Df_Varien_Data_Form
	 */
	private function getForm () {

		if (!isset ($this->_form)) {

			/** @var Df_Varien_Data_Form $result  */
			$result = new Df_Varien_Data_Form ();

			$result->setId ($this->getFormId());

			$result
				->setAction ($this->getTargetURL())
				->setName ($this->getFormId())
				->setMethod ($this->getMethod())
				->setUseContainer (true)
				->addHiddenFields ($this->_getFormFields())
				->addAdditionalHtmlAttribute ('accept-charset', 'UTF-8')
			;

			df_assert ($result instanceof Df_Varien_Data_Form);

			$this->_form = $result;
		}


		df_assert ($this->_form instanceof Df_Varien_Data_Form);

		return $this->_form;

	}


	/**
	* @var Varien_Data_Form
	*/
	private $_form;





	/**
	 * @return Mage_Sales_Model_Order
	 */
	private function getOrder () {

		if (!isset ($this->_order)) {

			/** @var Mage_Sales_Model_Order $result  */
			$result =
				df_model (
					Df_Sales_Const::ORDER_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Sales_Model_Order);


			$result
				->loadByIncrementId (
					df_helper()->checkout()->sessionSingleton()->getDataUsingMethod (
						Df_Checkout_Const::SESSION_PARAM__LAST_REAL_ORDER_ID
					)
				)
			;

			df_assert (!is_null ($result->getId ()));


			$this->_order = $result;
		}


		df_assert ($this->_order instanceof Mage_Sales_Model_Order);

		return $this->_order;

	}


	/**
	* @var Mage_Sales_Model_Order
	*/
	private $_order;




	/**
	 * @return Df_Payment_Model_Method_WithRedirect
	 */
	private function getPaymentMethod () {

		/** @var Df_Payment_Model_Method_WithRedirect $result  */
		$result =
			$this->getOrder()->getPayment()->getMethodInstance()
		;

		df_assert ($result instanceof Df_Payment_Model_Method_WithRedirect);

		return $result;
	}


	
	


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Block_Redirect';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	
	
	
}


