<?php

class Df_Payment_Block_Form extends Mage_Payment_Block_Form {


	/**
	 * @override
	 * @return string
	 */
	public function getArea() {

		/** @var string $result  */
		$result = Df_Core_Const_Design_Area::FRONTEND;

		df_result_string ($result);

		return $result;
	}


	/**
	 * @return string
	 */
	public function getDescription () {

		/** @var string $result  */
		$result = $this->getMethod()->getRmConfig()->frontend()->getDescription();

		df_result_string ($result);

		return $result;
	}



    /**
     * Перекрываем метод лишь для того,
	 * чтобы среда разработки знала класс способа оплаты
     *
	 * @override
     * @return Df_Payment_Model_Method_Base
     */
	public function getMethod() {

		/** @var Df_Payment_Model_Method_Base $result  */
		$result = parent::getMethod();

		df_assert ($result instanceof Df_Payment_Model_Method_Base);

		return $result;
	}



	/**
	 * @abstract
	 * @return string
	 */
	public function getTemplate () {

		df_abstract (__METHOD__);

		return Df_Core_Const::T_EMPTY;

	}




	/**
	 * @return bool
	 */
	public function isTestMode () {

		/** @var bool $result  */
		$result = $this->getMethod()->getRmConfig()->service()->isTestMode();

		df_result_boolean ($result);

		return $result;
	}






	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
        parent::_construct();
        $this
			->setTemplate (
				$this->getTemplate()
			)
		;
    }



	/**
	 * @return Mage_Sales_Model_Quote
	 */
	protected function getQuote () {

		/** @var Mage_Sales_Model_Quote $result  */
		$result =
			df_helper()->checkout()->sessionSingleton()->getQuote()
		;

		df_assert ($result instanceof Mage_Sales_Model_Quote);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Block_Form';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	
	
	
}


