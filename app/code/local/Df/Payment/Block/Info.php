<?php

class Df_Payment_Block_Info extends Mage_Payment_Block_Info {


	/**
	 * @override
	 * @return string
	 */
	public function getArea() {

		/** @var string $result  */
		$result = Df_Core_Const_Design_Area::FRONTEND;

		df_result_string ($result);

		return $result;
	}



    /**
     * Перекрываем метод лишь для того,
	 * чтобы среда разработки знала класс способа оплаты
	 *
	 * @override
     * @return Df_Payment_Model_Method_Base
     */
	public function getMethod() {

		/** @var Df_Payment_Model_Method_Base $result  */
		$result = parent::getMethod();

		df_assert ($result instanceof Df_Payment_Model_Method_Base);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getMethodTitle () {

		/** @var string $result  */
		$result =
			$this->getMethod()->getTitle()
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @abstract
	 * @return string
	 */
	public function getTemplate () {

		df_abstract (__METHOD__);

		return Df_Core_Const::T_EMPTY;

	}




	/**
	 * @return Mage_Sales_Model_Order|null
	 */
	protected function getOrder () {

		if (!isset ($this->_order)) {


			/** @var Mage_Payment_Model_Info $paymentInfo  */
			$paymentInfo = $this->getInfo();



			/** @var Mage_Sales_Model_Order|null $result  */
			$result =

					!($paymentInfo instanceof Mage_Sales_Model_Order_Payment)
				?
					null
				:
					/** @var Mage_Sales_Model_Order_Payment $paymentInfo  */
					$paymentInfo->getOrder ()
			;


			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Sales_Model_Order);
			}


			$this->_order = $result;
		}


		if (!is_null ($this->_order)) {
			df_assert ($this->_order instanceof Mage_Sales_Model_Order);
		}

		return $this->_order;

	}


	/**
	* @var Mage_Sales_Model_Order|null
	*/
	private $_order;





	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
        parent::_construct();
        $this
			->setTemplate (
				$this->getTemplate()
			)
		;
    }



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Payment_Block_Info';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


