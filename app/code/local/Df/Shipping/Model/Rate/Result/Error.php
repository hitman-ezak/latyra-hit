<?php

class Df_Shipping_Model_Rate_Result_Error extends Mage_Shipping_Model_Rate_Result_Error {



	const PARAM__CARRIER = 'carrier';
	const PARAM__CARRIER_TITLE = 'carrier_title';
	const PARAM__ERROR = 'error';
	const PARAM__ERROR_MESSAGE = 'error_message';



	/**
	 * @static
	 * @param Df_Shipping_Model_Carrier $carrier
	 * @param $message
	 * @return Df_Shipping_Model_Rate_Result_Error
	 */
	public static function create (Df_Shipping_Model_Carrier $carrier, $message) {

		df_param_string ($message, 1);

		/** @var Df_Shipping_Model_Rate_Result_Error $result */
		$result =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM__CARRIER => $carrier->getCarrierCode()
					,
					self::PARAM__CARRIER_TITLE => $carrier->getTitle()
					,
					self::PARAM__ERROR => true
					,
					self::PARAM__ERROR_MESSAGE => $message
				)
			)
		;

		df_assert ($result instanceof Df_Shipping_Model_Rate_Result_Error);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Rate_Result_Error';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


