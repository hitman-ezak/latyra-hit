<?php

class Df_Shipping_Model_Rate_Request extends Mage_Shipping_Model_Rate_Request {


	/**
	 * @return float
	 */
	public function getDeclaredValue () {

		/** @var float $result  */
		$result =
				$this->getPackageValue()
			*
				$this->getCarrier()->getRmConfig()->admin()->getDeclaredValuePercent()
			/
				100
		;

		df_result_float ($result);

		return $result;
	}



	/**
	 * @return string|null
	 */
	public function getDestinationCity () {

		/** @var string|null $result  */
		$result = $this->getData (self::PARAM__DESTINATION__CITY);

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @return string|null
	 */
	public function getDestinationCountryId () {

		/** @var string|null $result  */
		$result = $this->getData (self::PARAM__DESTINATION__COUNTRY_ID);

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @return string|null
	 */
	public function getDestinationPostalCode () {

		/** @var string|null $result  */
		$result = $this->getData (self::PARAM__DESTINATION__POSTAL_CODE);

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}
	
	
	
	
	/**
	 * @return Mage_Directory_Model_Region|null
	 */
	public function getDestinationRegion () {
		if (!isset ($this->_destinationRegion) && !$this->_destinationRegionIsNull) {
	
			/** @var Mage_Directory_Model_Region|null $result */
			$result =
				df_helper()->directory()->getRegions()->getItemById (
					$this->getDestinationRegionId()
				)
			;
	
			if (is_null ($result)) {
				$this->_destinationRegionIsNull = true;
			}
			else {
				df_assert ($result instanceof Mage_Directory_Model_Region);
			}
			$this->_destinationRegion = $result;
		}
		return $this->_destinationRegion;
	}
	/** @var Mage_Directory_Model_Region|null */
	private $_destinationRegion;
	
	/** @var bool */
	private $_destinationRegionIsNull = false;




	/**
	 * @return string|null
	 */
	public function getDestinationRegionalCenter () {

		/** @var string|null $result  */
		$result =
				is_null ($this->getDestinationRegion())
			?
				null
			:
				$this->getDestinationRegion()->getData ('df_capital')
		;

		return $result;
	}



	/**
	 * @return int|null
	 */
	public function getDestinationRegionId () {

		/** @var int|null $result  */
		$result = $this->getData (self::PARAM__DESTINATION__REGION_ID);

		if (!is_null ($result)) {
			df_result_integer ($result);
		}

		return $result;
	}



	/**
	 * @return string|null
	 */
	public function getDestinationRegionName () {
		/** @var string|null $result  */
		$result =
			df_helper()->directory()->getRegionNameById (
				$this->getDestinationRegionId ()
			)
		;
		return $result;
	}




	/**
	 * @return string
	 */
	public function getOriginCity () {

		/** @var string $result  */
		$result = $this->getData (self::PARAM__ORIGIN__CITY);

		if (df_empty ($result)) {

			df_error (
				'Администратор магазина должен указать город магазина в графе
				«Система» → «Настройки» → «Продажи» → «Доставка:
				общие настройки»→ «Расположение магазина» → «Город».'
			);
		}

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	public function getOriginCountryId () {

		/** @var string $result  */
		$result = $this->getData (self::PARAM__ORIGIN__COUNTRY_ID);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string|null
	 */
	public function getOriginPostalCode () {

		/** @var string|null $result  */
		$result = $this->getData (self::PARAM__ORIGIN__POSTAL_CODE);

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}

	
	
	
	/**
	 * @return Mage_Directory_Model_Region
	 */
	public function getOriginRegion () {
	
		if (!isset ($this->_originRegion)) {


			/** @var Mage_Directory_Model_Region $result */
			$result =
				df_helper()->directory()->getRegions()->getItemById (
					$this->getOriginRegionId()					
				)
			;

			df_assert ($result instanceof Mage_Directory_Model_Region);
	
			$this->_originRegion = $result;
		}
	
		df_assert ($this->_originRegion instanceof Mage_Directory_Model_Region);
	
		return $this->_originRegion;
	}
	
	
	/**
	* @var Mage_Directory_Model_Region
	*/
	private $_originRegion;



	/**
	 * @return string
	 */
	public function getOriginRegionalCenter () {

		/** @var string $result  */
		$result =
			df_convert_null_to_empty_string (
				$this->getOriginRegion()->getData ('df_capital')
			)
		;

		df_result_string ($result);

		return $result;
	}
	



	/**
	 * @param bool $throwExceptionIfNotFound [optional]
	 * @return int
	 */
	public function getOriginRegionId ($throwExceptionIfNotFound = true) {

		/** @var int $result  */
		$result = $this->getData (self::PARAM__ORIGIN__REGION_ID);

		/**
		 * Если указан регион такой страны, для которой в Magento отсутствует перечень регионов,
		 * то $result здесь будет равно названию региона.
		 *
		 * По этой причине прежняя проверка df_empty ($result) является непраивльной.
		 */
		if (!df_check_integer($result)) {

			if ($throwExceptionIfNotFound) {
				df_error (
					'Администратор магазина должен указать регион магазина в графе
					«Система» → «Настройки» → «Продажи» → «Доставка:
					общие настройки»→ «Расположение магазина» → «Область, край, республика».'
				);
			}
			else {
				$result = 0;
			}
		}

		return $result;
	}




	/**
	 * @return string
	 */
	public function getOriginRegionName () {

		/** @var string $result  */
		$result =
			df_helper()->directory()->getRegionNameById (
				$this->getOriginRegionId ()
			)
		;

		if (is_null ($result)) {
			/**
			 * Сюда мы можем попасть после пересоздания базы регионов
			 * (у них при этом меняются идентификаторы)
			 */
			df_error (
				'Администратор магазина должен указать субъект РФ магазина в графе
				«Система» → «Настройки» → «Продажи» → «Доставка:
				общие настройки»→ «Расположение магазина» → «Область, край, республика».'
			);
		}
		df_result_string ($result);

		return $result;
	}




	/**
	 * @param Mage_Catalog_Model_Product $product
	 * @param bool $useSystemNames [optional]
	 * @return array
	 */
	public function getProductDimensions (
		Mage_Catalog_Model_Product $product, $useSystemNames = false
	) {

		df_param_boolean ($useSystemNames, 1);

		/** @var array $result  */
		$result = array ();


		/** @var Mage_Catalog_Model_Product $productWithDimensions  */
		$productWithDimensions =
			$this->getProductsWithDimensions()->getItemById (
				$product->getId ()
			)
		;

		df_assert ($productWithDimensions instanceof Mage_Catalog_Model_Product);


		foreach ($this->getDimensionNames() as $systemDimensionName => $customDimensionName) {

			/** @var string $systemDimensionName */
			/** @var string $customDimensionName */

			/** @var float $dimensionValue  */
			$dimensionValue =
				floatval (
					$productWithDimensions->getData (
						$customDimensionName
					)
				)
			;

			df_assert_float ($dimensionValue);


			if (0.0 === $dimensionValue) {
				$dimensionValue =
					df_a (
						$this->getDimensionDefaultValues()
						,
						$customDimensionName
					)
				;
			}

			df_assert (0.0 < $dimensionValue);


			/** @var string $key */
			$key = $useSystemNames ? $systemDimensionName : $customDimensionName;

			df_assert_string ($key);


			$result [$key] = $dimensionValue;
		}

		df_result_array ($result);

		df_assert (3 === count ($result));

		return $result;
	}



	/**
	 * @return int
	 */
	public function getQty () {

		if (!isset ($this->_qty)) {

			/** @var int $result  */
			$result = 0;

			foreach ($this->getQuoteItemsSimple() as $item) {
				/** @var Mage_Sales_Model_Quote_Item $item */
				df_assert ($item instanceof Mage_Sales_Model_Quote_Item);

				$result += intval ($item->getQty());
			}

			df_assert_integer ($result);

			$this->_qty = $result;
		}

		df_result_integer ($this->_qty);

		return $this->_qty;
	}


	/**
	* @var int
	*/
	private $_qty;



	
	
	/**
	 * Обратите внимание, что если заказ содержит настраиваемый товар,
	 * то на программном в объекте «quote»
	 * ему соответствует не один объект «строка заказа»,
	 * а два: один — для настраиваемого товара,
	 * и второй — для простого товара,
	 * являющегося выбранным покупателем вариантом настраиваемого товара.
	 *
	 * Если заказ содержит товарный комплект («bundle»),
	 * то на программном в объекте «quote»
	 * ему соответствует не один объект «строка заказа»,
	 * а несколько: один — для товарного комплекта,
	 * и другие — для простого товара из товарного комплекта.
	 *
	 * Аналогично — для сгруппированных товаров.
	 *
	 * @link http://stackoverflow.com/a/8301170/254475
	 *
	 * Поэтому мы при комплектации груза учитываем только простые товары.
	 *
	 * Обратите также внимание, что та строка заказа,
	 * которая описывает простой вариант составного товара — не содержит в себе цену товара
	 * (а также вес строки — row_weight),
	 * и цену нужно узнавать из той строки, которая соответствует всему составному товару.
	 *
	 * Обратите также внимание, что та строка заказа,
	 * которая описывает простой вариант составного товара,
	 * хранит неправильное количество заказанного товара — 1,
	 * и количество нужно узнавать из той строки, которая соответствует всему составному товару.
	 *
	 * Обратите также внимание что связь между родительским и дочерники строками заказа
	 * устанавливается автоматически при загрузке коллекции:
	 * @see Mage_Sales_Model_Resource_Order_Item_Collection::_afterLoad
	 *
	 * @return Mage_Sales_Model_Quote_Item[]
	 */
	public function getQuoteItemsSimple () {
	
		if (!isset ($this->_quoteItemsSimple)) {
	
			/** @var Mage_Sales_Model_Quote_Item[] $result  */
			$result = array ();


			foreach ($this->getAllItems() as $quoteItem) {

				/** @var Mage_Sales_Model_Quote_Item $quoteItem */
				df_assert ($quoteItem instanceof Mage_Sales_Model_Quote_Item);

				if (Mage_Catalog_Model_Product_Type::TYPE_SIMPLE === $quoteItem->getProductType()) {
					$result []= $quoteItem;
				}

			}

	
			df_assert_array ($result);
	
			$this->_quoteItemsSimple = $result;
		}
	
	
		df_result_array ($this->_quoteItemsSimple);
	
		return $this->_quoteItemsSimple;
	}
	
	
	/**
	* @var Mage_Sales_Model_Quote_Item[]
	*/
	private $_quoteItemsSimple;	



	/**
	 * @return float
	 */
	public function getVolumeInCubicMetres () {

		if (!isset ($this->_volumeInCubicMetres)) {

			/** @var float $result  */
			$result = 0.0;

			foreach ($this->getQuoteItemsSimple() as $quoteItem) {

				/** @var Mage_Sales_Model_Quote_Item $quoteItem */
				df_assert ($quoteItem instanceof Mage_Sales_Model_Quote_Item);


				/** @var Df_Sales_Model_Quote_Item_Extended $quoteItemExtended  */
				$quoteItemExtended =
					Df_Sales_Model_Quote_Item_Extended::create (
						$quoteItem
					)
				;

				df_assert ($quoteItemExtended instanceof Df_Sales_Model_Quote_Item_Extended);


				/** @var Mage_Catalog_Model_Product $product */
				$product = $quoteItem->getProduct();

				df_assert ($product instanceof Mage_Catalog_Model_Product);


				/** @var float $productVolume  */
				$productVolume = 1.0;

				/** @var array $productDimensions  */
				$productDimensions = array_values ($this->getProductDimensions ($product));

				df_assert_array ($productDimensions);

				foreach ($productDimensions as $productDimension) {
					/** @var float $productDimension */
					df_assert_float ($productDimension);

					$productVolume *=
						df()->units()->length()->convertToMetres (
							$productDimension
						)
					;
				}

				$result += ($productVolume * $quoteItemExtended->getQty());
			}


			df_assert_float ($result);

			$this->_volumeInCubicMetres = $result;
		}

		df_result_float ($this->_volumeInCubicMetres);

		return $this->_volumeInCubicMetres;
	}


	/**
	* @var float
	*/
	private $_volumeInCubicMetres;

	

	
	
	/**
	 * @return float
	 */
	public function getWeightInGrammes () {
	
		if (!isset ($this->_weightInGrammes)) {
	
			/** @var float $result  */
			$result = 
				df()->units()->weight()->convertToGrammes (
					$this->getPackageWeight()
				)
			;
	
	
			df_assert_float ($result);
	
			$this->_weightInGrammes = $result;
		}
	
	
		df_result_float ($this->_weightInGrammes);
	
		return $this->_weightInGrammes;
	}
	
	
	/**
	* @var float
	*/
	private $_weightInGrammes;	
	
	
	
	
	
	/**
	 * @return float
	 */
	public function getWeightInKilogrammes () {
	
		if (!isset ($this->_weightInKilogrammes)) {
	
			/** @var float $result  */
			$result = 
				$this->getWeightInGrammes () / 1000
			;
	
	
			df_assert_float ($result);
	
			$this->_weightInKilogrammes = $result;
		}
	
	
		df_result_float ($this->_weightInKilogrammes);
	
		return $this->_weightInKilogrammes;
	}
	
	
	/**
	* @var float
	*/
	private $_weightInKilogrammes;	
		



	/**
	 * @return bool
	 */
	public function isDestinationMoscow () {

		if (!isset ($this->_destinationMoscow)) {

			/** @var bool $result  */
			$result =
				in_array (
					'МОСКВА'
					,
					array_map (
						'mb_strtoupper'
						,
						array (
							$this->getDestinationCity()
							,
							$this->getDestinationRegionName()
						)
					)
				)
			;


			df_assert_boolean ($result);

			$this->_destinationMoscow = $result;
		}


		df_result_boolean ($this->_destinationMoscow);

		return $this->_destinationMoscow;

	}


	/**
	* @var bool
	*/
	private $_destinationMoscow;



	/**
	 * @return bool
	 */
	public function isDestinationCityRegionalCenter () {

		/** @var bool $result  */
		$result =
			df_helper()->directory()->country()->russia()->isRegionalCenter (
				$this->getDestinationCity()
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return bool
	 */
	public function isOriginMoscow () {

		if (!isset ($this->_originMoscow)) {

			/** @var bool $result  */
			$result =
				in_array (
					'МОСКВА'
					,
					array_map (
						'mb_strtoupper'
						,
						array (
							$this->getOriginCity()
							,
							$this->getOriginRegionName()
						)
					)
				)
			;


			df_assert_boolean ($result);

			$this->_originMoscow = $result;
		}


		df_result_boolean ($this->_originMoscow);

		return $this->_originMoscow;

	}


	/**
	* @var bool
	*/
	private $_originMoscow;



	/**
	 * @return bool
	 */
	public function isOriginCityRegionalCenter () {

		/** @var bool $result  */
		$result =
			df_helper()->directory()->country()->russia()->isRegionalCenter (
				$this->getOriginCity()
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function isOriginTheSameAsDestination () {
	
		if (!isset ($this->_originTheSameAsDestination)) {
	
			/** @var bool $result  */
			$result =
				df_text()->areEqualCI (
					$this->getDestinationCity()
					,
					$this->getOriginCity()
				)
			;
	
	
			df_assert_boolean ($result);
	
			$this->_originTheSameAsDestination = $result;
		}
	
	
		df_result_boolean ($this->_originTheSameAsDestination);
	
		return $this->_originTheSameAsDestination;
	}
	
	
	/**
	* @var bool
	*/
	private $_originTheSameAsDestination;




	/**
	 * @return Df_Shipping_Model_Carrier
	 */
	private function getCarrier () {

		/** @var Df_Shipping_Model_Carrier $result  */
		$result = $this->getData (self::PARAM__CARRIER);

		df_assert ($result instanceof Df_Shipping_Model_Carrier);

		return $result;
	}




	/**
	 * @return array
	 */
	private function getDimensionDefaultValues () {
	
		if (!isset ($this->_dimensionDefaultValues)) {
	
			/** @var array $result  */
			$result =
				array (
						df_cfg()->shipping()->product()->getAttributeNameWidth()
					=>
						df_cfg()->shipping()->product()->getDefaultWidth()

					,
						df_cfg()->shipping()->product()->getAttributeNameHeight()
					=>
						df_cfg()->shipping()->product()->getDefaultHeight()

					,
						df_cfg()->shipping()->product()->getAttributeNameLength()
					=>
						df_cfg()->shipping()->product()->getDefaultLength()
				)
			;
	
	
			df_assert_array ($result);
	
			$this->_dimensionDefaultValues = $result;
		}
	
	
		df_result_array ($this->_dimensionDefaultValues);
	
		return $this->_dimensionDefaultValues;
	}
	
	
	/**
	* @var array
	*/
	private $_dimensionDefaultValues;



	/**
	 * @return array
	 */
	private function getDimensionNames () {

		if (!isset ($this->_dimensionNames)) {

			/** @var array $result  */
			$result =
				array (
					self::DIMENSION__WIDTH => df_cfg()->shipping()->product()->getAttributeNameWidth()
					,
					self::DIMENSION__HEIGHT => df_cfg()->shipping()->product()->getAttributeNameHeight()
					,
					self::DIMENSION__LENGTH => df_cfg()->shipping()->product()->getAttributeNameLength()
				)
			;


			df_assert_array ($result);

			$this->_dimensionNames = $result;
		}


		df_result_array ($this->_dimensionNames);

		return $this->_dimensionNames;

	}


	/**
	* @var array
	*/
	private $_dimensionNames;






	/**
	 * @return int[]
	 */
	private function getProductIds () {

		if (!isset ($this->_productIds)) {

			/** @var int[] $result  */
			$result = array ();


			foreach ($this->getAllItems() as $quoteItem) {

				/** @var Mage_Sales_Model_Quote_Item $quoteItem */
				df_assert ($quoteItem instanceof Mage_Sales_Model_Quote_Item);


				/** @var Mage_Catalog_Model_Product $product */
				$product = $quoteItem->getProduct();

				df_assert ($product instanceof Mage_Catalog_Model_Product);


				$result[]= intval ($product->getId ());

			}


			df_assert_array ($result);

			$this->_productIds = $result;
		}


		df_result_array ($this->_productIds);

		return $this->_productIds;

	}


	/**
	* @var int[]
	*/
	private $_productIds;




	/**
	 * @return Mage_Catalog_Model_Resource_Product_Collection
	 */
	private function getProductsWithDimensions () {

		if (!isset ($this->_productsWithDimensions)) {


			/** @var Mage_Catalog_Model_Resource_Product_Collection $result  */
			$result =
				Mage::getResourceModel (
					Df_Catalog_Const::PRODUCT_COLLECTION_CLASS_MF
				)
			;

			df_helper()->catalog()->assert()->productCollection ($result);



			$result
				->addIdFilter (
					$this->getProductIds()
				)
				->addAttributeToSelect (
					$this->getDimensionNames ()
				)
			;

			$this->_productsWithDimensions = $result;
		}


		df_helper()->catalog()->assert()->productCollection ($this->_productsWithDimensions);

		return $this->_productsWithDimensions;

	}


	/**
	* @var Mage_Catalog_Model_Resource_Product_Collection
	*/
	private $_productsWithDimensions;

	

	const DIMENSION__HEIGHT = 'height';
	const DIMENSION__LENGTH = 'length';
	const DIMENSION__WIDTH = 'width';

	const PARAM__CARRIER = 'carrier';

	const PARAM__DESTINATION__CITY = 'dest_city';
	const PARAM__DESTINATION__COUNTRY_ID = 'dest_country_id';
	const PARAM__DESTINATION__POSTAL_CODE = 'dest_postcode';
	const PARAM__DESTINATION__REGION_ID = 'dest_region_id';


	const PARAM__ORIGIN__CITY = 'city';
	const PARAM__ORIGIN__COUNTRY_ID = 'country_id';
	const PARAM__ORIGIN__POSTAL_CODE = 'postcode';
	const PARAM__ORIGIN__REGION_ID = 'region_id';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Rate_Request';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


