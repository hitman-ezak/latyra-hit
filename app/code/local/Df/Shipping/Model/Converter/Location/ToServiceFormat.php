<?php

abstract class Df_Shipping_Model_Converter_Location_ToServiceFormat extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return string
	 * @throws Exception
	 */
	abstract public function getResult ();


	/**
	 * @return string|null
	 */
	protected function getCity () {

		/** @var string|null $result  */
		$result = $this->cfg (self::PARAM__CITY);

		if (is_null ($result)) {
			/**
			 * В справочнике субъектов федерации некоторых служб доставки
			 * (в частности, EMS) отсутствуют регионы Москва и Санкт-Петербург.
			 *
			 * Стандартный калькулятор стоимости доставки на странице корзины
			 * не позволяет указать город, но позволяет указать субъект федерации.
			 *
			 * И, когда покупатель указывает субъект федерации "Москва",
			 * то модуль не в состоянии рассчитать стоимость доставки,
			 * потому что субъекта федерации "Москва" в справочнике EMS отсутствует,
			 * а информации о городе в данном случае нет.
			 *
			 * Чтобы всё-таки рассчитать тариф в данной ситуации,
			 * искусственно устанавливаем в качестве города Москву,
			 * когда в качестве субъекта федерации выбрана Москва.
			 */
			if (
				in_array (
					$this->getRegionName()
					,
					array ('Москва', 'Санкт-Петербург')
				)
			) {
				$result = $this->getRegionName();
			}
		}


		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @return string|null
	 */
	protected function getCountryId () {

		/** @var string|null $result  */
		$result = $this->cfg (self::PARAM__COUNTRY_ID);

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @return Mage_Directory_Model_Country
	 */
	protected function getCountry () {

		if (!isset ($this->_country)) {

			/** @var Mage_Directory_Model_Country $result  */
			$result =
				df_model (
					Df_Directory_Const::COUNTRY_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Directory_Model_Country);


			$result->loadByCode ($this->getCountryId());


			df_assert ($result instanceof Mage_Directory_Model_Country);

			$this->_country = $result;
		}


		df_assert ($this->_country instanceof Mage_Directory_Model_Country);

		return $this->_country;

	}


	/**
	* @var Mage_Directory_Model_Country
	*/
	private $_country;





	/**
	 * @return string|null
	 */
	protected function getCountryName () {

		if (!isset ($this->_countryName) && !$this->_countryNameIsNull) {

			/** @var string|null $result  */
			$result =
					is_null ($this->getCountry()->getId())
				?
					null
				:
					$this->getCountry()->getName()
			;


			if (!is_null ($result)) {
				df_assert_string ($result);
			}
			else {
				$this->_countryNameIsNull = true;
			}

			$this->_countryName = $result;
		}


		if (!is_null ($this->_countryName)) {
			df_result_string ($this->_countryName);
		}

		return $this->_countryName;

	}


	/**
	* @var string|null
	*/
	private $_countryName;

	/**
	 * @var bool
	 */
	private $_countryNameIsNull = false;




	/**
	 * @return string|null
	 */
	protected function getCountryNameEnglish () {
	
		if (!isset ($this->_countryNameEnglish) && !$this->_countryNameEnglishIsNull) {
	
			/** @var string|null $result  */
			$result = null;

			if (!is_null ($this->getCountry()->getId())) {

				$result =
					df_helper()->directory()->country()->getNameEnglish (
						$this->getCountry()
					)
				;

			}
	
	
			if (!is_null ($result)) {
				df_assert_string ($result);
			}
			else {
				$this->_countryNameEnglishIsNull = true;			
			}
	
			$this->_countryNameEnglish = $result;
		}
	
	
		if (!is_null ($this->_countryNameEnglish)) {
			df_result_string ($this->_countryNameEnglish);
		}
	
	
		return $this->_countryNameEnglish;
	}
	
	
	/**
	* @var string|null
	*/
	private $_countryNameEnglish;
	
	/**
	 * @var bool
	 */
	private $_countryNameEnglishIsNull = false;




	/**
	 * @return int|null
	 */
	protected function getRegionId () {

		/** @var int|null $result  */
		$result = $this->cfg (self::PARAM__REGION_ID);

		if (!is_null ($result)) {
			df_result_integer ($result);
		}

		return $result;
	}




	/**
	 * @return string|null
	 */
	protected function getRegionName () {

		/** @var string|null $result  */
		$result =
			df_helper()->directory()->getRegionNameById (
				$this->getRegionId()
			)
		;


		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}



	/**
	 * @return Df_Shipping_Model_Converter_Location_ToServiceFormat
	 */
	protected function throwError_invalidLocation () {

		/** @var string $message  */
		$message =
			sprintf (
					"Служба доставки не понимает местоположения:
					\nстрана: «%s»,
					\nобласть, край, республика: «%s»,
					\nгород: «%s»."
				,
						is_null ($this->getCountryName())
					?
						$this->getCountryId()
					:
						$this->getCountryName()
				,
						is_null ($this->getRegionName())
					?
						$this->getRegionId()
					:
						$this->getRegionName()
				,
				$this->getCity()
			)
		;

		df_assert_string ($message);


		if (
				is_null ($this->getRegionName())
			&&
				is_null ($this->getCity())
		) {
			$message = 'Укажите область и город';
		}
		else if (is_null ($this->getRegionName())) {
			$message = 'Укажите область';
		}
		else if (is_null ($this->getCity())) {
			$message = 'Укажите город';
		}

		df_error ($message);

		return $this;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CITY, new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM__COUNTRY_ID, new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM__REGION_ID, new Df_Zf_Validate_Int(), false
			)
		;
	}



	const PARAM__CITY = 'city';
	const PARAM__COUNTRY_ID = 'country_id';
	const PARAM__REGION_ID = 'region_id';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Converter_Location_ToServiceFormat';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}

