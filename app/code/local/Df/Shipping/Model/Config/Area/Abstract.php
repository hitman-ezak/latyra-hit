<?php

abstract class Df_Shipping_Model_Config_Area_Abstract extends Df_Shipping_Model_Config_Abstract {



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getAreaPrefix ();



	/**
	 * Ключи, значения которых хранятся по стандартному для Magento пути,
	 * в отличие от стандартного для Российской сборки пути.
	 *
	 * Например, ключ title хранится по пути carriers/df-ems/title,
	 * а не по пути df_shipping/ems/title
	 *
	 * @return array
	 */
	protected function getLegacyKeys () {
		return array ();
	}






	/**
	 * Получаем заданное ранее администратором
	 * значение конкретной настройки способа доставки
	 *
	 * @override
	 * @param string $key
	 * @param mixed $defaultValue [optional]
	 * @return mixed
	 */
	public function getVar ($key, $defaultValue = null) {

		df_param_string ($key, 0);

		/**
		 * Иногда возникает потребность давать ключу другое имя,
		 * нежели стандартное для Magento CE.
		 *
		 * Например, такая потребность возникает
		 * для стандартного ключа «showmethod»,
		 * потому что для ключа с этим именем ядро Magento
		 * выполняет нежелательную для нас обработку на JavaScript
		 * (а именно: скрывает данное поле,
		 * если в качестве значения опции
		 * «Ограничить область доставки конкретными странами?»
		 * указано «нет»).
		 */
		$key = $this->translateUncertainKey ($key);


		/** @var mixed $result  */
		$result =
				in_array ($key, $this->getLegacyKeys())
			?
				$this->getVarManager()->getValueLegacy ($key, $defaultValue)
			:
				parent::getVar (
					$this->preprocessKey (
						$key
					)
					,
					$defaultValue
				)
		;

		return $result;
	}



	/**
	 * @param string $key
	 * @return string
	 */
	protected function preprocessKey ($key) {

		df_param_string ($key, 0);

		/** @var string $result  */
		$result =
			implode (
				'__'
				,
				df_clean (
					array (
						$this->getAreaPrefix()
						,
						$key
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Area_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


