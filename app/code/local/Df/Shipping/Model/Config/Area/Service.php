<?php

class Df_Shipping_Model_Config_Area_Service	extends Df_Shipping_Model_Config_Area_Abstract {



	/**
	 * @return bool
	 */
	public function enableSmsNotification () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__ENABLE_SMS_NOTIFICATION
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	
	/**
	 * Варинты доставки, предоставляемые данной службой доставки
	 * @return array
	 */
	public function getAvailableShippingMethodsAsOptionArray () {
	
		/** @var array $result  */
		$result = $this->getConstManager()->getAvailableShippingMethodsAsOptionArray();

		df_result_array ($result);
	
		return $result;
	}


	
	
	/**
	 * @return array
	 */
	public function getDisabledShippingMethods () {
	
		if (!isset ($this->_disabledShippingMethods)) {

			/** @var string $resultAsString  */
			$resultAsString =
				$this->getVar (
					self::KEY__VAR__SHIPPING_METHODS
				)
			;

			df_assert_string ($resultAsString);

	
			/** @var array $result  */
			$result =
				array_diff (
					df_column (
						$this->getAvailableShippingMethodsAsOptionArray()
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE
					)
					,
					$this->getSelectedShippingMethods()
				)
			;
	
	
			df_assert_array ($result);
	
			$this->_disabledShippingMethods = $result;
		}
	
	
		df_result_array ($this->_disabledShippingMethods);
	
		return $this->_disabledShippingMethods;
	}
	
	
	/**
	* @var array
	*/
	private $_disabledShippingMethods;




	/**
	 * @return string|null
	 */
	public function getSelectedShippingMethod () {

		if (!isset ($this->_selectedShippingMethod)) {

			/** @var string|null $result  */
			$result = $this->getVar (self::KEY__VAR__SHIPPING_METHOD);

			if (self::KEY__VAR__SHIPPING_METHOD__NO === $result) {
				$result = null;
			}

			if (!is_null ($result)) {
				df_assert_string ($result);
			}

			$this->_selectedShippingMethod = $result;
		}

		if (!is_null ($this->_selectedShippingMethod)) {
			df_result_string ($this->_selectedShippingMethod);
		}

		return $this->_selectedShippingMethod;

	}


	/**
	* @var string|null
	*/
	private $_selectedShippingMethod;
	
	
	
	
	
	
	/**
	 * @return string|null
	 */
	public function getSelectedShippingMethodCode () {
	
		if (!isset ($this->_selectedShippingMethodCode)) {


			/** @var string|null $result  */
			$result =
				df_a (
					df_a (
						$this->getConstManager()
							->getAvailableShippingMethodsAsCanonicalConfigArray ()
						,
						$this->getSelectedShippingMethod()
						,
						array ()
					)
					,
					'code'
				)
			;

			if (!is_null ($result)) {
				df_assert_string ($result);
			}
	
			$this->_selectedShippingMethodCode = $result;
		}
	
		if (!is_null ($this->_selectedShippingMethodCode)) {
			df_result_string ($this->_selectedShippingMethodCode);
		}
	
		return $this->_selectedShippingMethodCode;
	}
	
	
	/**
	* @var string|null
	*/
	private $_selectedShippingMethodCode;
	
	
	

	
	
	
	/**
	 * @return array
	 */
	public function getSelectedShippingMethods () {
	
		if (!isset ($this->_selectedShippingMethods)) {

			/** @var string $resultAsString  */
			$resultAsString =
				$this->getVar (
					self::KEY__VAR__SHIPPING_METHODS
				)
			;

			df_assert_string ($resultAsString);

	
			/** @var array $result  */
			$result =
					(Df_Admin_Model_Config_Form_Element_Multiselect::RM__ALL === $resultAsString)
				?
					df_column (
						$this->getAvailableShippingMethodsAsOptionArray()
						,
						Df_Admin_Model_Config_Source::OPTION_KEY__VALUE
					)
				:
					df_parse_csv (
						$resultAsString
					)
			;
	
	
			df_assert_array ($result);
	
			$this->_selectedShippingMethods = $result;
		}
	
	
		df_result_array ($this->_selectedShippingMethods);
	
		return $this->_selectedShippingMethods;
	}
	
	
	/**
	* @var array
	*/
	private $_selectedShippingMethods;





	/**
	 * Возвращает значения поля code способа оплаты.
	 * Данный метод имеет смысл, когда значения поля code — числовые
	 *
	 * @return array
	 */
	public function getSelectedShippingMethodCodes () {
	
		if (!isset ($this->_selectedShippingMethodCodes)) {
	
			/** @var array $result  */
			$result =
				df_column (
					array_intersect_key (
						$this->getConstManager()->getAvailableShippingMethodsAsCanonicalConfigArray ()
						,
						array_flip ($this->getSelectedShippingMethods ())
					)
					,
					'code'
				)
			;
	
	
			df_assert_array ($result);
	
			$this->_selectedShippingMethodCodes = $result;
		}
	
	
		df_result_array ($this->_selectedShippingMethodCodes);
	
		return $this->_selectedShippingMethodCodes;
	}
	
	
	/**
	* @var array
	*/
	private $_selectedShippingMethodCodes;
	






	/**
	 * Работает ли модуль в тестовом режиме?
	 * Обратите внимание, что если в настройках отсутствует ключ «test»,
	 * то модуль будет всегда находиться в рабочем режиме.
	 *
	 * @return bool
	 */
    public function isTestMode () {

		/** @var bool $result */
        $result =
			$this->getVar (
				self::KEY__VAR__TEST
			)
		;

		$result =
				is_null ($result)
			?
				/**
				 * Eсли в настройках отсутствует ключ «test»,
				 * то модуль будет всегда находиться в рабочем режиме.
				 */
				false
			:
				$this->parseYesNo($result)
		;

		df_result_boolean ($result);

		return $result;
    }



	/**
	 * @override
	 * @return string
	 */
	protected function getAreaPrefix () {

		/** @var string $result  */
		$result = self::AREA_PREFIX;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function makeAccompanyingForms () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__MAKE_ACCOMPANYING_FORMS
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function needAcceptCashOnDelivery () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__NEED_ACCEPT_CASH_ON_DELIVERY
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return bool
	 */
	public function needDeliverCargoToTheBuyerHome () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__NEED_DELIVER_CARGO_TO_THE_BUYER_HOME
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function needGetCargoFromTheShopStore () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__NEED_GET_CARGO_FROM_THE_SHOP_STORE
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function needPacking () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__NEED_PACKING
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	const AREA_PREFIX = 'service';


	const KEY__VAR__ENABLE_SMS_NOTIFICATION = 'enable_sms_notification';
	const KEY__VAR__NEED_ACCEPT_CASH_ON_DELIVERY = 'need_accept_cash_on_delivery';
	const KEY__VAR__NEED_DELIVER_CARGO_TO_THE_BUYER_HOME = 'need_deliver_cargo_to_the_buyer_home';
	const KEY__VAR__NEED_GET_CARGO_FROM_THE_SHOP_STORE = 'need_get_cargo_from_the_shop_store';
	const KEY__VAR__MAKE_ACCOMPANYING_FORMS = 'make_accompanying_forms';
	const KEY__VAR__NEED_PACKING = 'need_packing';

	const KEY__VAR__SHIPPING_METHOD = 'shipping_method';
	const KEY__VAR__SHIPPING_METHOD__NO = 'no';

	const KEY__VAR__SHIPPING_METHODS = 'shipping_methods';

	const KEY__VAR__TEST = 'test';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Area_Service';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


