<?php

class Df_Shipping_Model_Config_Area_Frontend extends Df_Shipping_Model_Config_Area_Abstract {



	/**
	 * @return string
	 */
	public function getDescription () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__DESCRIPTION
				,
				Df_Core_Const::T_EMPTY
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getTitle () {

		/** @var string $result  */
		$result =
			$this->getVar (
				self::KEY__VAR__TITLE
				,
				Df_Core_Const::T_EMPTY
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function needDisableForShopCity () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__DISABLE_FOR_SHOP_CITY
					,
					false
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return bool
	 */
	public function needDisplayDiagnosticMessages () {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getVar (
					self::KEY__VAR__DISPLAY_DIAGNOSTIC_MESSAGES
					,
					true
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}





	/**
	 * @override
	 * @return string
	 */
	protected function getAreaPrefix () {

		/** @var string $result  */
		$result = self::AREA_PREFIX;

		df_result_string ($result);

		return $result;
	}



	/**
	 * Ключи, значения которых хранятся по стандартному для Magento пути,
	 * в отличие от стандартного для Российской сборки пути.
	 *
	 * Например, ключ title хранится по пути carriers/df-ems/title,
	 * а не по пути df_shipping/ems/title
	 *
	 * @override
	 * @return array
	 */
	protected function getLegacyKeys () {

		/** @var array $result  */
		$result = array ('active', 'title');

		df_result_array ($result);

		return $result;
	}





	/**
	 * @override
	 * @return array
	 */
	protected function getUncertainKeys () {

		/** @var array $result  */
		$result =
			array_merge (
				parent::getUncertainKeys()
				,
				array (
					'active'
					,
 					'sallowspecific'

					,
					/**
					 * Иногда возникает потребность давать ключу другое имя,
					 * нежели стандартное для Magento CE.
					 *
					 * Например, такая потребность возникает
					 * для стандартного ключа «showmethod»,
					 * потому что для ключа с этим именем ядро Magento
					 * выполняет нежелательную для нас обработку на JavaScript
					 * (а именно: скрывает данное поле,
					 * если в качестве значения опции
					 * «Ограничить область доставки конкретными странами?»
					 * указано «нет»).
					 */
					self::KEY__VAR__DISPLAY_DIAGNOSTIC_MESSAGES => 'showmethod'
					,
					'sort_order'
					,
					'specificcountry'
					,
					self::KEY__VAR__TITLE
				)
			)
		;

		df_result_array ($result);

		return $result;
	}



	const AREA_PREFIX = 'frontend';


	const KEY__VAR__DESCRIPTION = 'description';
	const KEY__VAR__DISABLE_FOR_SHOP_CITY = 'disable_for_shop_city';
	const KEY__VAR__DISPLAY_DIAGNOSTIC_MESSAGES = 'display_diagnostic_messages';

	const KEY__VAR__TITLE = 'title';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Area_Frontend';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


