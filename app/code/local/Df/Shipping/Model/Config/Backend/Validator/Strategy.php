<?php

abstract class Df_Shipping_Model_Config_Backend_Validator_Strategy
	extends Df_Admin_Model_Config_Backend_Validator_Strategy {


	/**
	 * Система показывает администратору диагностические сообщения над настройками всех модулей.
	 * Чтобы администратор знал, к какому конкретно модулю относится диагностическое сообщение,
	 * надо определить название модуля, что и делает данный метод.
	 *
	 * @return string
	 */
	protected function getModuleTitle () {

		/** @var string $result  */
		$result =
			df_mage()->adminhtml()->getConfig()
				->getSystemConfigNodeLabel (
					'df_shipping'
					,
					$this->getBackend()->getData('group_id')
				)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Backend_Validator_Strategy';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}

