<?php

class Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin
	extends Df_Shipping_Model_Config_Backend_Validator_Strategy {



	/**
	 * @override
	 * @return bool
	 */
	public function validate () {

		/** @var bool $result */
		$result = $this->getStrategy()->validate();

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return Df_Shipping_Model_Origin
	 */
	protected function getOrigin () {

		if (!isset ($this->_origin)) {

			/** @var string|int $region */
			$region =
				$this->getShippingOriginParam ('region_id')
			;

			/** @var bool $hasRegionId */
			$hasRegionId = df_check_integer ($region);

			/** @var Df_Shipping_Model_Origin $result  */
			$result =
				df_model (
					Df_Shipping_Model_Origin::getNameInMagentoFormat()
					,
					array (
						Df_Shipping_Model_Origin::PARAM__CITY =>
							$this->getShippingOriginParam ('city')
						,
						Df_Shipping_Model_Origin::PARAM__COUNTRY_ID =>
							$this->getShippingOriginParam ('country_id')
						,
						Df_Shipping_Model_Origin::PARAM__POSTAL_CODE =>
							$this->getShippingOriginParam ('postcode')
						,
						Df_Shipping_Model_Origin::PARAM__REGION_ID =>
								$hasRegionId
							?
								intval ($region)
							:
								null
						,
						Df_Shipping_Model_Origin::PARAM__REGION_NAME =>
								$hasRegionId
							?
								null
							:
								$region
					)
				)
			;

			df_assert ($result instanceof Df_Shipping_Model_Origin);

			$this->_origin = $result;
		}

		df_assert ($this->_origin instanceof Df_Shipping_Model_Origin);

		return $this->_origin;
	}


	/**
	* @var Df_Shipping_Model_Origin
	*/
	private $_origin;
	




	/**
	 * @param string $paramName
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	private function getShippingOriginParam ($paramName, $defaultValue = Df_Core_Const::T_EMPTY) {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				df()->config()->implodeKey (
					array (
						'shipping/origin'
						,
						$paramName
					)
				)
				,
				$this->getStore()
			)
		;

		if (df_empty ($result)) {
			$result = $defaultValue;
		}

		df_result_string ($result);

		return $result;
	}




	/**
	 * У стратегии тоже есть стратегии
	 *
	 * @return Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin
	 */
	private function getStrategy () {

		if (!isset ($this->_strategy)) {

			/** @var Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin $result  */
			$result =
				df_model (
					$this->getStrategyClassMf()
					,
					$this->getData()
				)
			;

			df_assert ($result instanceof Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin);

			$this->_strategy = $result;
		}

		df_assert ($this->_strategy instanceof Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin);

		return $this->_strategy;
	}


	/**
	* @var Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin
	*/
	private $_strategy;




	/**
	 * @return string
	 */
	private function getStrategyClassMf () {

		/** @var string $result  */
		$result = $this->getBackend()->getFieldConfigParam ('df_origin_validator');

		df_result_string ($result);

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Shipping_Model_Config_Backend_Validator_Strategy_Origin';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}
