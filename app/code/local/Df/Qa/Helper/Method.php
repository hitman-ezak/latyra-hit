<?php

class Df_Qa_Helper_Method extends Mage_Core_Helper_Abstract {

	/**
	 * @param  array|Traversable $paramValue	Значение параметра
	 * @param string $className
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertParamIsCollection ($paramValue, $className, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Df_Zf_Validate_Collection ($className)
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  array|Traversable $resultValue	Значение параметра
	 * @param string $className
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultIsCollection ($resultValue, $className, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Collection ($className)
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  string $paramValue	Значение параметра
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertParamIsString ($paramValue, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Df_Zf_Validate_String()
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  string $resultValue	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultIsString ($resultValue, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_String()
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  string $value	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertValueIsString ($value, $stackLevel = 0) {

		$this
			->validateValue (
			new Df_Zf_Validate_String()
				,
				$value
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  int $paramValue	Значение параметра
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertParamIsInteger ($paramValue, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Zend_Validate_Int ()
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  int $resultValue	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultIsInteger ($resultValue, $stackLevel = 0) {
		$this
			->validateResult (
				new Zend_Validate_Int ()
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  int $value	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertValueIsInteger ($value, $stackLevel = 0) {
		$this
			->validateValue (
				new Zend_Validate_Int ()
				,
				$value
				,
				$stackLevel + 1
			)
		;
	}


	
	
	
	
	/**
	 * @param  float $paramValue	Значение параметра
	 * @param  float $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertParamIsFloat ($paramValue, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Zend_Validate_Float ()
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  float $resultValue	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultIsFloat ($resultValue, $stackLevel = 0) {
		$this
			->validateResult (
				new Zend_Validate_Float ()
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  float $value	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertValueIsFloat ($value, $stackLevel = 0) {
		$this
			->validateValue (
				new Zend_Validate_Float ()
				,
				$value
				,
				$stackLevel + 1
			)
		;
	}	
	


	/**
	 * @param  array $paramValue	Значение параметра
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertParamIsArray ($paramValue, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Df_Zf_Validate_Array ()
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param  array $resultValue	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultIsArray ($resultValue, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Array ()
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}

	/**
	 * @param  array $resultValue	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertValueIsArray ($resultValue, $stackLevel = 0) {
		$this
			->validateValue (
				new Df_Zf_Validate_Array ()
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  bool $paramValue	Значение параметра
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertParamIsBoolean ($paramValue, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Df_Zf_Validate_Boolean ()
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  bool $resultValue	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultIsBoolean ($resultValue, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Boolean ()
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  bool $value	Значение параметра
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertValueIsBoolean ($value, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Boolean ()
				,
				$value
				,
				$stackLevel + 1
			)
		;
	}




	/**
	 * @param mixed $paramValue
	 * @param string $className
	 * @param int $paramOrdering
	 * @param int $stackLevel [optional]
	 * @return void
	 * @throws Exception
	 */
	public function validateParamClass ($paramValue, $className, $paramOrdering, $stackLevel = 0) {
		$this
			->validateParam (
				new Df_Zf_Validate_Class ($className)
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}



	/**
	 * @param mixed $resultValue
	 * @param string $className
	 * @param int $stackLevel [optional]
	 * @return void
	 * @throws Exception
	 */
	public function validateResultClass ($resultValue, $className, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Class ($className)
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}




	/**
	 * @param mixed $value
	 * @param string $className
	 * @param int $stackLevel [optional]
	 * @return void
	 * @throws Exception
	 */
	public function validateValueClass ($value, $className, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Class ($className)
				,
				$value
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param mixed $paramValue
	 * @param int $paramOrdering
	 * @param int|float $min [optional]
	 * @param int|float $max [optional]
	 * @param int $stackLevel [optional]
	 * @return void
	 * @throws Exception
	 */
	public function assertParamBetween (
		$paramValue, $paramOrdering, $min = null, $max = null, $stackLevel = 0
	) {
		$this
			->validateParam (
				new Df_Zf_Validate_Between (
					array (
						"min" => is_null ($min) ? PHP_INT_MIN : $min
						,
						"max" => is_null ($max) ? PHP_INT_MAX : $max
						,
						"inclusive" => true
					)
				)
				,
				$paramValue
				,
				$paramOrdering
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param  int|float  $resultValue	Значение параметра
	 * @param int|float $min [optional]
	 * @param int|float $max [optional]
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertResultBetween ($resultValue, $min = null, $max = null, $stackLevel = 0) {
		$this
			->validateResult (
				new Df_Zf_Validate_Between (
					array (
						"min" => is_null ($min) ? PHP_INT_MIN : $min
						,
						"max" => is_null ($max) ? PHP_INT_MAX : $max
						,
						"inclusive" => true
					)
				)
				,
				$resultValue
				,
				$stackLevel + 1
			)
		;
	}





	/**
	 * @param  int|float $value	Значение параметра
	 * @param int|float $min [optional]
	 * @param int|float $max [optional]
	 * @param  int $stackLevel [optional]      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function assertValueIsBetween ($value, $min = null, $max = null, $stackLevel = 0) {
		$this
			->validateValue (
				new Df_Zf_Validate_Between (
					array (
						"min" => is_null ($min) ? PHP_INT_MIN : $min
						,
						"max" => is_null ($max) ? PHP_INT_MAX : $max
						,
						"inclusive" => true
					)
				)
				,
				$value
				,
				$stackLevel + 1
			)
		;
	}


	/**
	 * @param string $method
	 * @return Df_Qa_Helper_Method
	 */
	public function raiseErrorAbstract ($method) {

			df_error (
				sprintf (
					'Метод должен быть явно определён: «%s»'
					,
					$method
				)
			)
		;

		return $this;
	}




	/**
	 * @param string $validatorClass
	 * @param array $messages
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return Df_Qa_Helper_Method
	 */
	public function raiseErrorParam (
		$validatorClass
		,
		array $messages
		,
		$paramOrdering
		,
		$stackLevel = 1
	) {
		/** @var array $bt */
		$bt = debug_backtrace ();

		/** @var array $caller */
		$caller = df_a ($bt, $stackLevel + 1);

		/** @var Df_Qa_Model_Debug_Execution_State $state */
		$state = df_model (Df_Qa_Const::DEBUG_EXECUTION_STATE_CLASS_MF, $caller);

		/** @var string $methodNameWithClassName */
		$methodNameWithClassName =
			implode (
				"::"
				,
				array (
					$state->getClassName ()
					,
					$state->getFunctionName ()
				)
			)
		;

		/** @var string $paramName */
		$paramName = 'Неизвестный параметр';

		if (!is_null ($paramOrdering)) {
			if ($state->getMethod ()) {
				/** @var ReflectionParameter $methodParameter */
				$methodParameter = $state->getMethodParameter ($paramOrdering);

				if ($methodParameter instanceof ReflectionParameter) {
					$paramName = $methodParameter->getName ();
				}
			}
		}

		/** @var string $errorMessage */
		$errorMessage =
			strtr (
					"[%method%]\nПараметр «%paramName%» забракован проверяющим «%validatorClass%»."
				.
					"\nСообщения проверяющего:\n%messages%\n\n"
				,
				array (
					'%method%' => $methodNameWithClassName
					,
					'%paramName%' => $paramName
					,
					'%validatorClass%' => $validatorClass
					,
					'%messages%' =>
						implode (
							"\n"
							,
							$messages
						)
				)
			)
		;
		$this->throwException ($errorMessage, $stackLevel);
		return $this;
	}





	/**
	 * @param string $validatorClass
	 * @param array $messages
	 * @param  int $stackLevel      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return Df_Qa_Helper_Method
	 */
	public function raiseErrorResult ($validatorClass, array $messages, $stackLevel = 1) {

		/** @var array $bt */
		$bt = debug_backtrace ();

		/** @var array $caller */
		$caller = df_a ($bt, $stackLevel + 1);

		/** @var Df_Qa_Model_Debug_Execution_State $state */
		$state = df_model (Df_Qa_Const::DEBUG_EXECUTION_STATE_CLASS_MF, $caller);

		/** @var string $methodNameWithClassName */
		$methodNameWithClassName =
			implode (
				"::"
				,
				array (
					$state->getClassName ()
					,
					$state->getFunctionName ()
				)
			)
		;

		/** @var string $errorMessage */
		$errorMessage =
			strtr (
					"[%method%]\nРезультат метода забракован проверяющим «%validatorClass%»."
				.
					"\nСообщения проверяющего:\n%messages%\n\n"
				,
				array (
					'%method%' => $methodNameWithClassName
					,
					'%validatorClass%' => $validatorClass
					,
					'%messages%' =>
						implode (
							"\n"
							,
							$messages
						)
				)
			)
		;

		$this->throwException ($errorMessage, $stackLevel);

		return $this;
	}




	/**
	 * @param  Zend_Validate_Interface $validator
	 * @param  mixed $resultValue	Значение параметра
	 * @param  int $stackLevel      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function validateResult (Zend_Validate_Interface $validator, $resultValue, $stackLevel = 1) {
		if (!$validator->isValid ($resultValue)) {
			$this
				->raiseErrorResult (
					$validatorClass = get_class ($validator)
					,
					$messages = $validator->getMessages()
					,
					++$stackLevel
				)
			;
		}
	}



	/**
	 * @param  Zend_Validate_Interface $validator
	 * @param  mixed $value	Значение переменной
	 * @param  int $stackLevel      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function validateValue (Zend_Validate_Interface $validator, $value, $stackLevel = 1) {

		if (!$validator->isValid ($value)) {

			/** @var string $errorMessage */
			$errorMessage =
				strtr (
						"Значение переменной забраковано проверяющим «%validatorClass%»."
					.
						"\nСообщения проверяющего:\n%messages%"
					,
					array (
						'%validatorClass%' => get_class ($validator)
						,
						'%messages%' =>
							implode (
								"\n"
								,
								$validator->getMessages()
							)
					)
				)
			;

			$this->throwException ($errorMessage, $stackLevel);
		}
	}



	/**
	 * @param  Zend_Validate_Interface $validator
	 * @param  mixed $paramValue	Значение параметра
	 * @param  int $paramOrdering	Порядковый номер параметра при вызове метода
	 * @param  int $stackLevel      <p>
	 * Какой уровень стека вызовов функций (в глубину, начиная от текущего уровня) смотреть
	 * </p>
	 * @return void
	 * @throws Exception
	 */
	public function validateParam (Zend_Validate_Interface $validator, $paramValue, $paramOrdering, $stackLevel = 1) {
		if (!$validator->isValid ($paramValue)) {
			$this
				->raiseErrorParam (
					$validatorClass = get_class ($validator)
					,
					$messages = $validator->getMessages()
					,
					$paramOrdering
					,
					++$stackLevel
				)
			;
		}
	}


	/**
	 * @param string $message
	 * @param int $stackLevel [optional]
	 * @throws Df_Core_Exception_Internal
	 * @return void
	 */
	private function throwException ($message, $stackLevel = 0) {

		/** @var Df_Core_Exception_Internal $exception  */
		$exception = new Df_Core_Exception_Internal ($message);

		$exception->setStackLevelsCountToSkip ($stackLevel + 1);

		throw $exception;

	}

	/**
	 * Обратите внимание, что применение метода getNameInMagentoFormat()
	 * привело бы к рекурсии!
	 */
}


