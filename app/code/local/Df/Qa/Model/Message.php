<?php

abstract class Df_Qa_Model_Message extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getTemplate();




	/**
	 * @return Df_Qa_Model_Message
	 */
	public function log () {

		if ($this->needLogToFile()) {
			$this->writeToFile ();
		}

		if ($this->needNotifyDeveloper() && !df_is_it_my_local_pc ())  {
			$this->mail ();
		}
	
		return $this;
	}



	/**
	 * @return string
	 */
	protected function getBlockClassMf () {
		return 'core/template';
	}
	



	/**
	 * @return string
	 */
	private function getFileName () {
		return $this->cfg (self::PARAM__FILE_NAME, self::DEFAULT__FILE_NAME);
	}


	
	
	
	/**
	 * @return Zend_Mail
	 */
	private function getMail () {

		if (!isset ($this->_mail)) {

			/** @var Zend_Mail $result */
			$result = new Zend_Mail ('utf-8');

			$result
				->setFrom (
					df()->mail()->getCurrentStoreMailAddress()
				)
				->addTo(
					self::DEVELOPER__MAIL_ADDRESS
				)
				->setSubject(
					$this->getMailSubject()
				)
			;

			$this->_mail = $result;
		}

		return $this->_mail;
	}


	/** @var Zend_Mail */
	private $_mail;	

	
	


	/**
	 * @return Zend_Log
	 */
	private function getMailLogger () {
	
		if (!isset ($this->_mailLogger)) {
	
			/** @var Zend_Log $result */
			$result = new Zend_Log ();

			$result
				->addWriter (
					$this->getMailWriter ()
				)
			;
	
			df_assert ($result instanceof Zend_Log);
	
			$this->_mailLogger = $result;
		}
	
		df_assert ($this->_mailLogger instanceof Zend_Log);
	
		return $this->_mailLogger;
	}
	
	
	/**
	* @var Zend_Log
	*/
	private $_mailLogger;




	/**
	 * @return string
	 */
	private function getMailSubject () {

		/** @var string $result  */
		$result =
			strtr (
				self::T__MAIL__SUBJECT
				,
				array (
					'%domain%' => df()->mail()->getCurrentStoreDomain()
				)
			)
		;


		df_result_string ($result);

		return $result;
	}

	
	
	
	
	
	/**
	 * @return Zend_Log_Writer_Mail
	 */
	private function getMailWriter () {

		if (!isset ($this->_mailWriter)) {

			/** @var Zend_Log_Writer_Mail $result */
			$result = 
				new Zend_Log_Writer_Mail (
					$this->getMail ()
				)
			;		    

			$this->_mailWriter = $result;
		}

		return $this->_mailWriter;
	}


	/** @var Zend_Log_Writer_Mail */
	private $_mailWriter;	
	



	/**
	 * @return string
	 */
	private function getMessage () {

		if (!isset ($this->_message)) {

			/** @var Mage_Core_Block_Template $block  */
			$block =
				df_mage()->core()->layout()->createBlock (
					$this->getBlockClassMf()
					,
					get_class ($this)
					,
					array (
						self::BLOCK_PARAM__MODEL => $this
						,
						'area' => 'frontend'
					)
				)
			;

			df_assert ($block instanceof Mage_Core_Block_Template);


			$block
				->setTemplate (
					$this->getTemplate()
				)
			;


			/** @var string $result  */
			$result =
				$block->toHtml()
			;


			df_assert_string ($result);

			$this->_message = $result;
		}


		df_result_string ($this->_message);

		return $this->_message;

	}


	/**
	* @var string
	*/
	private $_message;




	/**
	 * @return Df_Qa_Model_Message
	 */
	private function mail () {

		$this->getMailLogger()
			->log (
			    $this->getMessage()
				,
				Zend_Log::INFO
			)
		;
		return $this;
	}

	
	


	/**
	 * @return bool
	 */
	private function needLogToFile () {
		return $this->cfg (self::PARAM__NEED_LOG_TO_FILE, false);
	}




	/**
	 * @return bool
	 */
	private function needNotifyDeveloper () {
		return $this->cfg (self::PARAM__NEED_NOTIFY_DEVELOPER, false);
	}





	/**
	 * @return Df_Qa_Model_Message
	 */
	private function writeToFile () {

		Mage
			::log (
			    $message = $this->getMessage()
				,
				$level = Zend_Log::INFO
				,
				$file = $this->getFileName()
				,
				$forceLog = true
			)
		;

		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__FILE_NAME,	new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM__NEED_LOG_TO_FILE, new Df_Zf_Validate_Boolean()
			)
			->addValidator (
				self::PARAM__NEED_NOTIFY_DEVELOPER, new Df_Zf_Validate_Boolean()
			)

		;
	}


	const BLOCK_PARAM__MODEL = 'model';

	const DEFAULT__FILE_NAME = 'rm.log';
	const DEVELOPER__MAIL_ADDRESS = 'reklama@podolsk.ru';

	const PARAM__FILE_NAME = 'file_name';
	const PARAM__NEED_LOG_TO_FILE = 'need_log_to_file';
	const PARAM__NEED_NOTIFY_DEVELOPER = 'need_notify_developer';


	const T__MAIL__SUBJECT = 'RM log: %domain%';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Message';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

