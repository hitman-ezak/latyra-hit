<?php

class Df_Qa_Model_Message_Failure_Error extends Df_Qa_Model_Message_Failure {

	
	/**
	 * @override
	 * @return string
	 */
	public function getFailureMessage () {
	
		if (!isset ($this->_failureMessage)) {
	
			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_NEW_LINE
					,
					array (
						sprintf (
							'[%s] %s'
							,
							$this->getErrorTypeAsString()
							,
							$this->getErrorMessage()
						)
						,
						sprintf (
							'File: %s', $this->getErrorFile()
						)
						,
						sprintf (
							'Line: %s', $this->getErrorLine()
						)
					)
				)
			;
	
			df_assert_string ($result);
	
			$this->_failureMessage = $result;
		}
	
		df_result_string ($this->_failureMessage);
	
		return $this->_failureMessage;
	}
	
	
	/**
	* @var string
	*/
	private $_failureMessage;	




	/**
	 * @return bool
	 */
	public function isFatal () {

		/** @var bool $result  */
		$result =
			in_array (
				$this->getErrorType()
				,
				$this->getFatalErrorTypes()
			)
		;

		df_result_boolean ($result);

		return $result;
	}


	
	
	/**
	 * @override
	 * @return array
	 */
	protected function getTrace () {
	
		if (!isset ($this->_trace)) {
	
			/** @var array $result  */
			$result = array ();

			/**
			 * debug_backtrace не работает в функции-обработчике register_shutdown_function.
			 * Однако xdebug_get_function_stack — работает.
			 */
			if (extension_loaded('xdebug')) {
				$result =
					array_reverse (
						xdebug_get_function_stack()
					)
				;
			}
	
			df_assert_array ($result);
	
			$this->_trace = $result;
		}
	
	
		df_result_array ($this->_trace);
	
		return $this->_trace;
	}
	
	
	/**
	* @var array
	*/
	private $_trace;	




	/**
	 * @param int $errorType
	 * @return string
	 */
	private function convertErrorTypeToString ($errorType) {

		/** @var string $result  */
		$result =
			df_a (
				$this->getMapFromErrorTypeToLabel()
				,
				$errorType
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getErrorFile () {

		/** @var string $result  */
		$result =
			df_a (
				$this->getErrorLast()
				,
				self::ERROR___FILE
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return int
	 */
	private function getErrorLine() {

		/** @var int $result  */
		$result =
			intval (
				df_a (
					$this->getErrorLast()
					,
					self::ERROR___LINE
				)
			)
		;

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getErrorMessage () {

		/** @var string $result  */
		$result =
			df_a (
				$this->getErrorLast()
				,
				self::ERROR___MESSAGE
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return int
	 */
	private function getErrorType () {

		/** @var int $result  */
		$result =
			intval (
				df_a (
					$this->getErrorLast()
					,
					self::ERROR___TYPE
				)
			)
		;

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getErrorTypeAsString () {

		/** @var string $result  */
		$result =
			$this->convertErrorTypeToString (
				$this->getErrorType()
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return array
	 */
	private function getErrorLast () {

		if (!isset ($this->_errorLast)) {

			/** @var array $result  */
			$result = error_get_last ();

			if (is_null ($result)) {
				$result = array ();
			}

			df_assert_array ($result);

			$this->_errorLast = $result;
		}


		df_result_array ($this->_errorLast);

		return $this->_errorLast;
	}


	/**
	* @var array
	*/
	private $_errorLast;



	/**
	 * @return array
	 */
	private function getFatalErrorTypes () {

		if (!isset ($this->_fatalErrorTypes)) {

			/** @var array $result  */
			$result =
				array (
					E_ERROR
					,E_PARSE
					,E_CORE_ERROR
					,E_CORE_WARNING
					,E_COMPILE_ERROR
					,E_COMPILE_WARNING
				)
			;

			df_assert_array ($result);

			/**
			 * xDebug при E_RECOVERABLE_ERROR останавивает работу интерпретатора
			 */
			if (extension_loaded ('xdebug')) {
				$result []= E_RECOVERABLE_ERROR;
			}

			$this->_fatalErrorTypes = $result;
		}


		df_result_array ($this->_fatalErrorTypes);

		return $this->_fatalErrorTypes;
	}


	/**
	* @var array
	*/
	private $_fatalErrorTypes;



	/**
	 * @return array
	 */
	private function getMapFromErrorTypeToLabel () {

		if (!isset ($this->_mapFromErrorTypeToLabel)) {

			/** @var array $result  */
			$result =
				array (
					E_ERROR => 'E_ERROR'
					,E_WARNING => 'E_WARNING'
					,E_PARSE => 'E_PARSE'
					,E_NOTICE => 'E_NOTICE'
					,E_CORE_ERROR => 'E_CORE_ERROR'
					,E_CORE_WARNING => 'E_CORE_WARNING'
					,E_COMPILE_ERROR => 'E_COMPILE_ERROR'
					,E_COMPILE_WARNING => 'E_COMPILE_WARNING'
					,E_USER_ERROR => 'E_USER_ERROR'
					,E_USER_WARNING => 'E_USER_WARNING'
					,E_USER_NOTICE => 'E_USER_NOTICE'
					,E_STRICT => 'E_STRICT'
					,E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR'
				)
			;

			df_assert_array ($result);

			if (defined ('E_DEPRECATED')) {
				$result [E_DEPRECATED] = 'E_DEPRECATED';
			}

			if (defined ('E_USER_DEPRECATED')) {
				$result [E_USER_DEPRECATED] = 'E_USER_DEPRECATED';
			}

			$this->_mapFromErrorTypeToLabel = $result;
		}

		df_result_array ($this->_mapFromErrorTypeToLabel);

		return $this->_mapFromErrorTypeToLabel;
	}


	/**
	* @var array
	*/
	private $_mapFromErrorTypeToLabel;



	/**
	 * @override
	 * @return int
	 */
	protected function getStackLevel () {
		return $this->cfg (self::PARAM__STACK_LEVEL, 13);
	}



	const ERROR___FILE = 'file';
	const ERROR___LINE = 'line';
	const ERROR___MESSAGE = 'message';
	const ERROR___TYPE = 'type';





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Qa_Model_Message_Failure_Error';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

