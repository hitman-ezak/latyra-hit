<?php

class Df_Adminhtml_Block_Tax_Rule_Edit_Form extends Mage_Adminhtml_Block_Tax_Rule_Edit_Form {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @override
	 * @return Df_Adminhtml_Block_Tax_Rule_Edit_Form
	 */
	protected function _prepareForm () {

		parent::_prepareForm();

		/** @var Varien_Data_Form $form */
		$form = $this->getForm();

		df_assert ($form instanceof Varien_Data_Form);


		/** @var Varien_Data_Form_Element_Fieldset $fieldset */
		$fieldset = $form->getElement ('base_fieldset');

		df_assert ($fieldset instanceof Varien_Data_Form_Element_Fieldset);


		/** @var Varien_Data_Form_Element_Abstract $taxRateField */
		$taxRateField = $fieldset->getElements()->searchById('tax_rate');

		df_assert ($taxRateField instanceof Varien_Data_Form_Element_Abstract);


		$taxRateField
			->setData (
				'note'
				,
				'Если Вы выберите несколько ставок — система применит только ставку с наибольшим процентом.'
			)
		;

		return $this;
	}
}


