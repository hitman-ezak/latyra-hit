<?php

class Df_Adminhtml_Block_Widget_Grid_Column_RendererDf extends Df_Core_Block_Template {



	/**
	 * @return Varien_Object
	 */
	protected function getColumn () {

		/** @var Varien_Object $result  */
		$result =
			$this->getOriginalRenderer()->getColumn()
		;

		df_assert ($result instanceof Varien_Object);

		return $result;
	}



	/**
	 * @param string $paramName
	 * @param mixed $defaultValue
	 * @return mixed
	 */
	protected function getColumnParam ($paramName, $defaultValue = null) {

		/** @var string $result  */
		$result =
			df_a (
				$this->getColumn()
				,
				$paramName
				,
				$defaultValue
			)
		;

		return $result;
	}



	/**
	 * @return Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
	 */
	protected function getOriginalRenderer () {

		/** @var Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract $result  */
		$result =
			$this->getData (self::PARAM__ORIGINAL_RENDERER)
		;

		df_assert ($result instanceof Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract);

		return $result;
	}




	/**
	 * @param string $paramName
	 * @param mixed $defaultValue
	 * @return mixed
	 */
	protected function getRowParam ($paramName, $defaultValue = null) {

		/** @var string $result  */
		$result =
			df_a (
				$this->getRow()
				,
				$paramName
				,
				$defaultValue
			)
		;

		return $result;
	}




	/**
	 * @return Varien_Object
	 */
	protected function getRow () {

		/** @var Varien_Object $result  */
		$result =
			$this->cfg (self::PARAM__ROW)
		;

		df_assert ($result instanceof Varien_Object);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__ROW, 'Varien_Object'
			)
			->validateClass (
				self::PARAM__ORIGINAL_RENDERER
				,
				'Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract'
			)
		;
	}






	const PARAM__ROW = 'row';
	const PARAM__ORIGINAL_RENDERER = 'original_renderer';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Block_Widget_Grid_Column_RendererDf';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}



