<?php

/**
 * Обратите внимание, что этот класс перекрывает стандартный не посредством rewrite,
 * а иначе:

	<config>
		<sections>
			<df_shipping translate='label' module='df_shipping'>
				<frontend_model>df_adminhtml/system_config_form</frontend_model>
			</df_shipping>
			<df_payment translate='label' module='df_payment'>
				<frontend_model>df_adminhtml/system_config_form</frontend_model>
			</df_payment>
		</sections>
	</config>
 */
class Df_Adminhtml_Block_System_Config_Form extends Mage_Adminhtml_Block_System_Config_Form {


	/**
	 * @override
	 *
	 * @param Varien_Data_Form_Element_Fieldset $fieldset
	 * @param Varien_Simplexml_Element $group
	 * @param Varien_Simplexml_Element $section
	 * @param string $fieldPrefix
	 * @param string $labelPrefix
	 * @return Mage_Adminhtml_Block_System_Config_Form
	 */
	public function initFields ($fieldset, $group, $section, $fieldPrefix='', $labelPrefix='') {

		/** @var bool $patchNeeded */
		static $patchNeeded;

		if (!isset ($patchNeeded)) {
			$patchNeeded = df_magento_version ('1.4.1.0', '<');
		}

		if (!$patchNeeded) {
			parent::initFields ($fieldset, $group, $section, $fieldPrefix='', $labelPrefix='');
		}
		else {
			$this->initFields_1_4_0_1 ($fieldset, $group, $section, $fieldPrefix='', $labelPrefix='');
		}

		return $this;
	}

	
	
	/**
	 * @override
     * @return array
	 */
	protected function _getAdditionalElementTypes () {
	
		if (!isset ($this->_rmAdditionalElementTypes)) {
	
			/** @var array $result  */
			$result =
				array_merge (
					parent::_getAdditionalElementTypes()
					,
					$this->getRmElementTypes()
				)
			;
	
	
			df_assert_array ($result);
	
			$this->_rmAdditionalElementTypes = $result;
		}
	
	
		df_result_array ($this->_rmAdditionalElementTypes);
	
		return $this->_rmAdditionalElementTypes;
	}
	
	
	/**
	* @var array
	*/
	private $_rmAdditionalElementTypes;



	/**
	 * @return array
	 */
	private function getRmElementTypes () {

		if (!isset ($this->_rmElementTypes)) {


			/** @var array $result  */
			$result = array ();

			if ($this->getRmNode()) {

				foreach ($this->getRmNode()->asCanonicalArray() as $fieldType => $modelNameMf) {

					/** @var string $fieldType */
					/** @var string $modelNameMf */

					df_assert_string ($fieldType);
					df_assert_string ($modelNameMf);

					$result [$fieldType]=
						Mage::getConfig()->getModelClassName ($modelNameMf)
					;

				}

			}


			df_assert_array ($result);

			$this->_rmElementTypes = $result;
		}


		df_result_array ($this->_rmElementTypes);

		return $this->_rmElementTypes;

	}


	/**
	* @var array
	*/
	private $_rmElementTypes;
	
	
	
	
	
	/**
	 * @return Mage_Core_Model_Config_Element|null
	 */
	private function getRmNode () {
	
		if (!isset ($this->_rmNode) && !$this->_rmNodeIsNull) {
	
			/** @var Mage_Core_Model_Config_Element|null $result  */
			$result =
				df()->config()->getNodeByKey (
					df()->config()->implodeKey (
						array (
							'df'
							,
							'admin'
							,
							'config-form'
							,
							'element-types'
						)
					)
				)
			;

	
			if (!is_null ($result)) {
				df_assert ($result instanceof Mage_Core_Model_Config_Element);
			}
			else {
				$this->_rmNodeIsNull = true;
			}
	
			$this->_rmNode = $result;
		}
	
	
		if (!is_null ($this->_rmNode)) {
			df_assert ($this->_rmNode instanceof Mage_Core_Model_Config_Element);
		}		
		
	
		return $this->_rmNode;
	}
	
	
	/**
	* @var Mage_Core_Model_Config_Element|null
	*/
	private $_rmNode;	
	
	/**
	 * @var bool
	 */
	private $_rmNodeIsNull = false;	




	/**
	 * @param Varien_Data_Form_Element_Fieldset $fieldset
	 * @param Varien_Simplexml_Element $group
	 * @param Varien_Simplexml_Element $section
	 * @param string $fieldPrefix
	 * @param string $labelPrefix
	 * @return Mage_Adminhtml_Block_System_Config_Form
	 */
	private function initFields_1_4_0_1 ($fieldset, $group, $section, $fieldPrefix='', $labelPrefix='') {

		/** @var Df_Adminhtml_Model_Config_Data_1401 $dfConfig  */
		$dfConfig =
			Mage::getSingleton (
				Df_Adminhtml_Model_Config_Data_1401::getNameInMagentoFormat()
			)
		;

		$dfConfig->load();


		foreach ($group->fields as $elements) {

			$elements = (array)$elements;
			// sort either by sort_order or by child node values bypassing the sort_order
			if ($group->sort_fields && $group->sort_fields->by) {
				$fieldset->setSortElementsByAttribute((string)$group->sort_fields->by,
					($group->sort_fields->direction_desc ? SORT_DESC : SORT_ASC)
				);
			} else {
				usort($elements, array($this, '_sortForm'));
			}

			foreach ($elements as $e) {
				if (!$this->_canShowField($e)) {
					continue;
				}



				/**********************************
				 * BEGIN PATCH
				 *********************************/

				//$path = $section->getName() . '/' . $group->getName() . '/' . $fieldPrefix . $e->getName();


				/**
				 * Look for custom defined field path
				 */
				$path = (string)$e->config_path;
				if (empty($path)) {
					$path = $section->getName() . '/' . $group->getName() . '/' . $fieldPrefix . $e->getName();
				} elseif (strrpos($path, '/') > 0) {
					// Extend config data with new section group
					$groupPath = substr($path, 0, strrpos($path, '/'));
					if (!isset($configDataAdditionalGroups[$groupPath])) {

						$this->_configData = $dfConfig->extendConfig (
							$groupPath,
							false,
							$this->_configData
						);
						$configDataAdditionalGroups[$groupPath] = true;
					}
				}

				/**********************************
				 * END PATCH
				 *********************************/



				$id = $section->getName() . '_' . $group->getName() . '_' . $fieldPrefix . $e->getName();

				if (isset($this->_configData[$path])) {
					$data = $this->_configData[$path];
					$inherit = false;
				} else {
					$data = $this->_configRoot->descend($path);
					$inherit = true;
				}
				if ($e->frontend_model) {
					$fieldRenderer = Mage::getBlockSingleton((string)$e->frontend_model);
				} else {
					$fieldRenderer = $this->_defaultFieldRenderer;
				}

				$fieldRenderer->setForm($this);
				$fieldRenderer->setConfigData($this->_configData);

				$helperName = $this->_configFields->getAttributeModule($section, $group, $e);
				$fieldType  = (string)$e->frontend_type ? (string)$e->frontend_type : 'text';
				$name       = 'groups['.$group->getName().'][fields]['.$fieldPrefix.$e->getName().'][value]';
				$label      =  Mage::helper($helperName)->__($labelPrefix).' '.Mage::helper($helperName)->__((string)$e->label);
				$comment    = (string)$e->comment ? Mage::helper($helperName)->__((string)$e->comment) : '';
				$hint       = (string)$e->hint ? Mage::helper($helperName)->__((string)$e->hint) : '';

				if ($e->backend_model) {
					$model = Mage::getModel((string)$e->backend_model);
					if (!$model instanceof Mage_Core_Model_Config_Data) {
						Mage::throwException('Invalid config field backend model: '.(string)$e->backend_model);
					}
					$model->setPath($path)->setValue($data)->afterLoad();
					$data = $model->getValue();
				}

				if ($e->depends) {
					foreach ($e->depends->children() as $dependent) {
						$dependentId = $section->getName() . '_' . $group->getName() . '_' . $fieldPrefix . $dependent->getName();
						$dependentValue = (string) $dependent;
						$this->_getDependence()
							->addFieldMap($id, $id)
							->addFieldMap($dependentId, $dependentId)
							->addFieldDependence($id, $dependentId, $dependentValue);
					}
				}

				$field = $fieldset->addField($id, $fieldType, array(
					'name'                  => $name,
					'label'                 => $label,
					'comment'               => $comment,
					'hint'                  => $hint,
					'value'                 => $data,
					'inherit'               => $inherit,
					'class'                 => $e->frontend_class,
					'field_config'          => $e,
					'scope'                 => $this->getScope(),
					'scope_id'              => $this->getScopeId(),
					'scope_label'           => $this->getScopeLabel($e),
					'can_use_default_value' => $this->canUseDefaultValue((int)$e->show_in_default),
					'can_use_website_value' => $this->canUseWebsiteValue((int)$e->show_in_website),
				));

				if (isset($e->validate)) {
					$field->addClass($e->validate);
				}

				if (isset($e->frontend_type) && 'multiselect' === (string)$e->frontend_type && isset($e->can_be_empty)) {
					$field->setCanBeEmpty(true);
				}

				$field->setRenderer($fieldRenderer);

				if ($e->source_model) {
					// determine callback for the source model
					$factoryName = (string)$e->source_model;
					$method = false;
					if (preg_match('/^([^:]+?)::([^:]+?)$/', $factoryName, $matches)) {
						array_shift($matches);
						list($factoryName, $method) = array_values($matches);
					}

					$sourceModel = Mage::getSingleton($factoryName);
					if ($sourceModel instanceof Varien_Object) {
						$sourceModel->setPath($path);
					}
					if ($method) {
						if ('multiselect' === $fieldType) {
							$optionArray = $sourceModel->$method();
						} else {
							$optionArray = array();
							foreach ($sourceModel->$method() as $value => $label) {
								$optionArray[] = array('label' => $label, 'value' => $value);
							}
						}
					} else {
						$optionArray =
							$sourceModel
								->toOptionArray (
									'multiselect' === $fieldType
								)
						;
					}
					$field->setValues($optionArray);
				}
			}
		}
		return $this;
	}


	
	
	
	
	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Block_System_Config_Form';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	

}


