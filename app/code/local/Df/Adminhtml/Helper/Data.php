<?php

class Df_Adminhtml_Helper_Data extends Mage_Adminhtml_Helper_Data {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @param Mage_Adminhtml_Controller_Action $controller
	 * @return string
	 */
	public function getTranslatorByController (Mage_Adminhtml_Controller_Action $controller) {

		/** @var string $controllerClass  */
		$controllerClass = get_class ($controller);

		df_assert_string ($controllerClass);

		if (!isset ($this->_translatorByController[$controllerClass])) {

			/** @var array $classNameParts  */
			$classNameParts =
				explode (
					Df_Core_Model_Reflection::PARTS_SEPARATOR
					,
					$controllerClass
				)
			;


			if ('Mage' !== df_a ($classNameParts, 0)) {

				$result =
					df()->reflection()->getModuleName (
						$controllerClass
					)
				;

			}

			else {
				$result =
					implode (
						Df_Core_Model_Reflection::PARTS_SEPARATOR
						,
						array (
							df_a ($classNameParts, 0)
							,
							df_a ($classNameParts, 2)
						)
					)
				;


				/**
				 * Однако же, данного модуля может не существовать.
				 * Например, для адреса http://localhost.com:656/index.php/admin/system_design/
				 * алгоритм возвращает название несуществующего модуля «Mage_System».
				 *
				 * В таком случае возвращаемся к алторитму из первой ветки
				 * (по сути, для стандартного кода возвращаем «Mage_Adminhtml»)
				 */

				if (!df_module_enabled ($result)) {

					$result =
						df()->reflection()->getModuleName (
							$controllerClass
						)
					;
				}

			}

			df_assert_string ($result);

			$this->_translatorByController[$controllerClass] = $result;
		}


		df_result_string ($this->_translatorByController[$controllerClass]);

		return $this->_translatorByController[$controllerClass];

	}


	/**
	* @var array
	*/
	private $_translatorByController = array ();







    /**
	 * @param array $args
     * @return string
     */
    public function translateByParent (array $args) {

		/** @var string $result  */
        $result =
			df_helper()->localization()->translation()->translateByModule (
				$args, self::DF_PARENT_MODULE
			)
		;

		df_result_string ($result);

	    return $result;
    }




	/**
	 * @return string
	 */
	private function getModuleNameForTranslation () {

		if (!isset ($this->_moduleNameForTranslation)) {

			/** @var string $result  */
			$result = self::DF_PARENT_MODULE;


			if (
					df_enabled(Df_Core_Feature::LOCALIZATION)
				&&
					df_cfg()->localization()->translation()->admin()->isEnabled()
				&&
					(df()->request()->getController() instanceof Mage_Adminhtml_Controller_Action)
			) {

				/** @var Mage_Adminhtml_Controller_Action $controller  */
				$controller = df()->request()->getController();

				$result =
					df_helper()->adminhtml()->getTranslatorByController (
						$controller
					)
				;

			}


			df_assert_string ($result);

			$this->_moduleNameForTranslation = $result;
		}


		df_result_string ($this->_moduleNameForTranslation);

		return $this->_moduleNameForTranslation;

	}


	/**
	* @var string
	*/
	private $_moduleNameForTranslation;





	const DF_PARENT_MODULE = 'Mage_Adminhtml';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Helper_Data';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}