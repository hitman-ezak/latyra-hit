<?php

/**
 * Cообщение:		«adminhtml_block_salesrule_actions_prepareform»
 * Источник:		Mage_Adminhtml_Block_Promo_Quote_Edit_Tab_Actions::_prepareForm()
 * [code]
		Mage::dispatchEvent('adminhtml_block_salesrule_actions_prepareform', array('form' => $form));
 * [/code]
 *
 * Назначение:		Обработчик может изменить вкладку «Actions»
 * 					формы редактирования ценовых правил
 */
class Df_Adminhtml_Model_Event_Block_SalesRule_Actions_PrepareForm extends Df_Core_Model_Event {



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Event_Block_SalesRule_Actions_PrepareForm';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	/**
	 * Возвращает элемент формы: выпадающий список типов действий (типов ценовых правил)
	 *
	 * @return Varien_Data_Form_Element_Select
	 */
	public function getActionElement () {
		if (!isset ($this->_actionElement)) {

			$this->_actionElement =

				$this->getActionsFieldset()->getElements ()->searchById (
					self::DF_ELEMENT_SIMPLE_ACTION
				)
			;

			df_assert ($this->_actionElement instanceof Varien_Data_Form_Element_Select);
		}
		return $this->_actionElement;
	}

	/**
	 * @var Varien_Data_Form_Element_Select
	 */
	private $_actionElement;





	/**
	 * Возвращает элемент формы: набор полей вкладки «Actions»
	 *
	 * @return Varien_Data_Form_Element_Fieldset
	 */
	public function getActionsFieldset () {
		if (!isset ($this->_actionsFieldset)) {


			$this->_actionsFieldset =
				$this->getForm()->getElements ()->searchById (
					self::DF_ELEMENT_FIELDSET_ACTIONS
				)
			;

			df_assert ($this->_actionsFieldset instanceof Varien_Data_Form_Element_Fieldset);
		}
		return $this->_actionsFieldset;
	}


	/**
	 *
	 * @var Varien_Data_Form_Element_Fieldset
	 */
	private $_actionsFieldset;




	/**
	 * Возвращает форму «Actions»
	 *
	 * @return Varien_Data_Form
	 */
	public function getForm () {
		$result = $this->getObserver()->getData (self::PARAM_FORM);

		df_assert ($result instanceof Varien_Data_Form);

		return $result;
	}



	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}


	const PARAM_FORM = 'form';
	const DF_ELEMENT_FIELDSET_ACTIONS = 'action_fieldset';
	const DF_ELEMENT_SIMPLE_ACTION = 'simple_action';

	const EXPECTED_EVENT_PREFIX = 'adminhtml_block_salesrule_actions_prepareform';
}


