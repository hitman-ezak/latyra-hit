<?php

class Df_Adminhtml_Model_System_Config_Source_Catalog_Search_Type
	extends  Mage_Adminhtml_Model_System_Config_Source_Catalog_Search_Type {


	/**
	 * @return array
	 */
	public function toOptionArray () {

		/** @var array $result  */
		$result =
			array_map (
				array ($this, 'translate')
				,
				parent::toOptionArray ()
			)
		;

		df_result_array ($result);

		return $result;
	}



	/**
	 * @param array $option
	 * @return array
	 */
	public function translate (array $option) {

		/** @var array $result  */
		$result =
			array_merge (
				$option
				,
				array (
					Df_Admin_Model_Config_Source::OPTION_KEY__LABEL =>
						df_helper()->catalogSearch()->__ (
							df_a ($option, Df_Admin_Model_Config_Source::OPTION_KEY__LABEL)
						)
				)
			)
		;

		df_result_array ($result);

		return $result;
	}



}


