<?php

/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Adminhtml_Model_System_Config_Source_Currency_Service extends Df_Admin_Model_Config_Source {
	
	
	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
    protected function toOptionArrayInternal ($isMultiSelect = false) {
		return $this->getAsOptionArray();
    }
	
	
	/**
	 * @return array
	 */
	private function getAsOptionArray () {
		if (!isset ($this->_asOptionArray)) {

			/** @var array $result  */
			$result = array ();

			/** @var array $services */
			$services = Mage::getConfig()->getNode('global/currency/import/services')->asArray();
			df_assert_array ($result);

			foreach ($services as $code => $service) {
				/** @var array $service */
				df_assert_array ($service);

				/** @var string $code */
				df_assert_string ($code);

				/** @var string $name */
				$name = df_a ($service, 'name');
				df_assert_string ($name);

				/** @var int $ordering */
				$ordering = intval (df_a ($service, 'ordering', 0));

				$result [$ordering]=
					array (
						self::OPTION_KEY__LABEL => $name
						,
						self::OPTION_KEY__VALUE => $code
					)
				;
			}

			/**
			 * Вот ради этого мы перекрыли родительский класс
			 */
			ksort ($result);

			df_result_array ($result);
			$this->_asOptionArray = $result;
		}
		return $this->_asOptionArray;
	}
	/** @var array */
	private $_asOptionArray;	
	


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_System_Config_Source_Currency_Service';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


