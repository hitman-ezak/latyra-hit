<?php

class Df_Adminhtml_Model_Handler_AdjustLabels_Button extends Df_Core_Model_Handler {



	/**
	 * Метод-обработчик события
	 *
	 * @override
	 * @return void
	 */
	public function handle () {

		if (df_enabled (Df_Core_Feature::LOCALIZATION)) {

			$this->getBlockAsButton()
				->setData (
					Df_Adminhtml_Const::BUTTON_PROPERTY_LABEL
					,
					df_text()->formatCase (
						df_convert_null_to_empty_string (
							$this->getBlockAsButton()->getData(
								Df_Adminhtml_Const::BUTTON_PROPERTY_LABEL
							)
						)
						,
						df_cfg()->admin()->_interface()->getButtonLabelFont()->getLetterCase()
					)
				)
			;
		}

	}




	/**
	 * @return Mage_Adminhtml_Block_Widget_Button
	 */
	private function getBlockAsButton () {

		/** @var Mage_Adminhtml_Block_Widget_Button $result  */
		$result = $this->getEvent()->getBlock();

		df_assert ($result instanceof Mage_Adminhtml_Block_Widget_Button);

		return $result;
	}





	/**
	 * Объявляем метод заново, чтобы IDE знала настоящий тип объекта-события
	 *
	 * @return Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before
	 */
	protected function getEvent () {

		/** @var Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before $result  */
		$result = parent::getEvent();

		df_assert ($result instanceof Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before);

		return $result;
	}





	/**
	 * Класс события (для валидации события)
	 *
	 * @override
	 * @return string
	 */
	protected function getEventClass () {

		/** @var string $result  */
		$result = Df_Core_Model_Event_CoreBlockAbstract_ToHtml_Before::getClass();

		df_result_string ($result);

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Adminhtml_Model_Handler_AdjustLabels_Button';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


