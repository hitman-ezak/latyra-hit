<?php

class Df_Tweaks_Helper_Settings_Header extends Df_Core_Helper_Settings {


	/**
	 * @return Df_Admin_Model_Config_Extractor_Font
	 */
	public function getFont () {
	
		if (!isset ($this->_font)) {
	
			/** @var Df_Admin_Model_Config_Extractor_Font $result  */
			$result = 	
				df_model (
					Df_Admin_Model_Config_Extractor_Font::getNameInMagentoFormat()
					,
					array (
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_GROUP_PATH =>
							'df_tweaks/header'

						,
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_KEY_PREFIX =>
							Df_Core_Const::T_EMPTY
					)
				)
			;
	
			df_assert ($result instanceof Df_Admin_Model_Config_Extractor_Font);
	
			$this->_font = $result;
		}
	
		df_assert ($this->_font instanceof Df_Admin_Model_Config_Extractor_Font);
	
		return $this->_font;
	}
	
	
	/**
	* @var Df_Admin_Model_Config_Extractor_Font
	*/
	private $_font;	




	/**
	 * @return boolean
	 */
	public function hideAccountLinkFromAnonymousShopper () {
		return
			$this->getYesNo (
				'df_tweaks/header/hide_account_link_from_anonymous_shopper'
			)
		;
	}




	/**
	 * @return boolean
	 */
	public function hideCartLink () {
		return
			$this->getYesNo (
				'df_tweaks/header/remove_cart_link'
			)
		;
	}




	/**
	 * @return boolean
	 */
	public function hideCheckoutLink () {
		return
			$this->getYesNo (
				'df_tweaks/header/remove_checkout_link'
			)
		;
	}




	/**
	 * @return boolean
	 */
	public function hideWelcomeFromLoggedIn () {
		return
			$this->getYesNo (
				'df_tweaks/header/remove_welcome_for_logged_in'
			)
		;
	}




	/**
	 * @return string
	 */
	public function hideWishlistLink () {
		return
			Mage::getStoreConfig (
				'df_tweaks/header/remove_wishlist_link'
			)
		;
	}




	/**
	 * @return boolean
	 */
	public function replaceAccountLinkTitleWithCustomerName () {
		return
			$this->getYesNo (
				'df_tweaks/header/replace_account_link_title_with_customer_name'
			)
		;
	}



	/**
	 * @return boolean
	 */
	public function showOnlyFirstName () {
		return
			$this->getYesNo (
				'df_tweaks/header/show_only_first_name'
			)
		;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Tweaks_Helper_Settings_Header';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}