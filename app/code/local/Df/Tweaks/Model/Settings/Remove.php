<?php

class Df_Tweaks_Model_Settings_Remove extends Df_Core_Model_Abstract {


	/**
	 * Метод возвращает значение типа string, а не boolean,
	 * потому что допустимых значений три:
	 * «удалить», «не удалять», «удалить, если пуст»
	 *
	 * @return string
	 */
	public function getRemoveFromAccount () {
		return
			$this->getConfigValue(
				'remove_from_account'
				,
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__NO_REMOVE
			)
		;
	}



	/**
	 * Метод возвращает значение типа string, а не boolean,
	 * потому что допустимых значений три:
	 * «удалить», «не удалять», «удалить, если пуст»
	 *
	 * @return string
	 */
	public function getRemoveFromAll () {
		return
			$this->getConfigValue(
				'remove_from_all'
				,
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__NO_REMOVE
			)
		;
	}




	/**
	 * Метод возвращает значение типа string, а не boolean,
	 * потому что допустимых значений три:
	 * «удалить», «не удалять», «удалить, если пуст»
	 *
	 * @return string
	 */
	public function getRemoveFromCatalogSearchResult () {
		return
			$this->getConfigValue(
				'remove_from_catalog_search_result'
				,
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__NO_REMOVE
			)
		;
	}




	/**
	 * Метод возвращает значение типа string, а не boolean,
	 * потому что допустимых значений три:
	 * «удалить», «не удалять», «удалить, если пуст»
	 *
	 * @return string
	 */
	public function getRemoveFromFrontpage () {
		return
			$this->getConfigValue(
				'remove_from_frontpage'
				,
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__NO_REMOVE
			)
		;
	}



	/**
	 * Метод возвращает значение типа string, а не boolean,
	 * потому что допустимых значений три:
	 * «удалить», «не удалять», «удалить, если пуст»
	 *
	 * @return string
	 */
	public function getRemoveFromCatalogProductList () {
		return
			$this->getConfigValue(
				'remove_from_catalog_product_list'
				,
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__NO_REMOVE
			)
		;
	}



	/**
	 * Метод возвращает значение типа string, а не boolean,
	 * потому что допустимых значений три:
	 * «удалить», «не удалять», «удалить, если пуст»
	 *
	 * @return string
	 */
	public function getRemoveFromCatalogProductView () {
		return
			$this->getConfigValue(
				'remove_from_catalog_product_view'
				,
				Df_Admin_Model_Config_Source_RemoveIfEmpty::VALUE__NO_REMOVE
			)
		;
	}



	/**
	 * @param string $shortKey
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	private function getConfigValue ($shortKey, $defaultValue = Df_Core_Const::T_EMPTY) {

		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				$this->translateConfigKeyFromShortToFull (
					$shortKey
				)
			)
		;

		if (is_null ($result)) {
			$result = $defaultValue;
		}

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	private function getSection () {
		return $this->cfg (self::PARAM__SECTION);
	}



	/**
	 * @param string $shortKey
	 * @return string
	 */
	private function translateConfigKeyFromShortToFull ($shortKey) {

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					'df_tweaks'
					,
					$this->getSection()
					,
					$shortKey
				)
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__SECTION, new Df_Zf_Validate_String()
			)
		;
	}



	const PARAM__SECTION = 'section';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Tweaks_Model_Settings_Remove';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

