<?php

class Df_Tweaks_Model_Handler_AdjustCartMini extends Df_Tweaks_Model_Handler_Remover {



	/**
	 * @override
	 * @return string
	 */
	protected function getBlockName () {
		return 'cart_sidebar';
	}



	/**
	 * @override
	 * @return Df_Tweaks_Model_Settings_Remove
	 */
	protected function getSettings() {
		return df_cfg()->tweaks()->cart();
	}




	/**
	 * @override
	 * @return bool
	 */
	protected function hasDataToShow () {

		/** @var bool $result */
		$result =
				!is_null ($this->getBlock())
			&&
				(0 < $this->getBlock()->getSummaryCount())
			&&
				($this->getBlock()->getIsNeedToDisplaySideBar())
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @return Mage_Checkout_Block_Cart_Sidebar|null
	 */
	private function getBlock () {

		/** @var Mage_Checkout_Block_Cart_Sidebar|null $result  */
		$result = df_mage()->core()->layout()->getBlock ($this->getBlockName());

		if (!($result instanceof Mage_Checkout_Block_Cart_Sidebar)) {
			$result = null;
		}

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Tweaks_Model_Handler_AdjustCartMini';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


