<?php

class Df_Client_Model_DelayedMessage extends Df_Core_Model_Abstract {


	/**
	 * @return string
	 */
	public function getBody () {
		return $this->cfg (self::PARAM__BODY);
	}



	/**
	 * @return string
	 */
	public function getClassName () {
		return $this->cfg (self::PARAM__CLASS_NAME);
	}



	/**
	 * @return string
	 */
	public function getCreationTime () {
		return $this->cfg (self::PARAM__CREATION_TIME);
	}



	/**
	 * @return Zend_Date
	 */
	public function getCreationTimeAsDateTime () {

		if (!isset ($this->_creationTimeAsDateTime)) {

			/** @var Zend_Date $result  */
			$result =
				df_parse_mysql_datetime (
					$this->getCreationTime()
				)
			;

			df_assert ($result instanceof Zend_Date);

			$this->_creationTimeAsDateTime = $result;
		}

		df_assert ($this->_creationTimeAsDateTime instanceof Zend_Date);

		return $this->_creationTimeAsDateTime;
	}


	/**
	* @var Zend_Date
	*/
	private $_creationTimeAsDateTime;



	/**
	 * @return string
	 */
	public function getLastRetryTime () {
		return $this->cfg (self::PARAM__LAST_RETRY_TIME);
	}



	/**
	 * @return Zend_Date
	 */
	public function getLastRetryTimeAsDateTime () {

		if (!isset ($this->_lastRetryTimeAsDateTime)) {

			/** @var Zend_Date $result  */
			$result =
				df_parse_mysql_datetime (
					$this->getLastRetryTime()
				)
			;

			df_assert ($result instanceof Zend_Date);

			$this->_lastRetryTimeAsDateTime = $result;
		}

		df_assert ($this->_lastRetryTimeAsDateTime instanceof Zend_Date);

		return $this->_lastRetryTimeAsDateTime;
	}


	/**
	* @var Zend_Date
	*/
	private $_lastRetryTimeAsDateTime;




	/**
	 * @return Df_Client_Model_Message_Request
	 */
	public function getMessage () {

		if (!isset ($this->_message)) {

			/** @var string $className */
			$className =
				Df_Core_Model_RemoteControl_Coder::i()->decodeClassName (
					$this->getClassName()
				)
			;

			df_assert_string ($className);


			/** @var array $messageData */
			$messageData =
				Df_Core_Model_RemoteControl_Coder::i()->decode (
					$this->getBody()
				)
			;

			df_assert_array ($messageData);



			/** @var Df_Client_Model_Message_Request $result  */
			$result =
				df_model (
					$className
					,
					$messageData
				)
			;

			df_assert ($result instanceof Df_Client_Model_Message_Request);

			$result->setDelayedMessage ($this);

			$this->_message = $result;
		}

		df_assert ($this->_message instanceof Df_Client_Model_Message_Request);

		return $this->_message;
	}


	/**
	* @var Df_Client_Model_Message_Request
	*/
	private $_message;




	/**
	 * @return int
	 */
	public function getNumRetries () {
		return intval ($this->cfg (self::PARAM__NUM_RETRIES));
	}
	



	/**
	 * @return Df_Client_Model_DelayedMessage
	 */
	public function updateNumRetries () {

		/** @var int $numRetries  */
		$numRetries =
			max (
				0
				,
					is_null ($this->getId())
				?
					self::MAX_RETRIES
				:
					$this->getNumRetries() - 1
			)
		;

		$this
			->setData (
				self::PARAM__NUM_RETRIES

				,
				$numRetries
			)
		;


		if (
				(0 >= $this->getNumRetries())
			&&
				(
						self::MAX_FAILURED_DAYS
					<=
						Df_Zf_Date::getNumberOfDaysBetweenTwoDates (
							Zend_Date::now()
							,
							$this->getCreationTimeAsDateTime()
						)
				)
		) {
			df_error (
				'Ваш сервер настроен неправильно. Обратитесь к специалисту'
			);
		}

		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
    protected function _beforeSave () {
		$this->initTime();
		parent::_beforeSave();
    }




	/**
	 * @return Df_Client_Model_DelayedMessage
	 */
	private function initTime () {

		/** @var string $timeAsMySqlString */
		$timeAsMySqlString =
			df_date_to_mysql_datetime (
				Df_Zf_Date::nowInCurrentTimeZone()
			)
		;

		if (is_null ($this->getData (self::PARAM__CREATION_TIME))) {
			$this
				->setData (
					self::PARAM__CREATION_TIME
					,
					$timeAsMySqlString
				)
			;
		}

		$this
			->setData (
				self::PARAM__LAST_RETRY_TIME
				,
				$timeAsMySqlString
			)
		;

		return $this;
	}




	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
		parent::_construct();
        $this->_init (Df_Client_Model_Resource_DelayedMessage::getNameInMagentoFormat());
		$this->initTime();
    }


	const MAX_FAILURED_DAYS = 3;
	const MAX_RETRIES = 10;

	const PARAM__BODY = 'body';
	const PARAM__CLASS_NAME = 'class_name';
	const PARAM__CREATION_TIME = 'creation_time';
	const PARAM__LAST_RETRY_TIME = 'last_retry_time';
	const PARAM__MESSAGE_ID = 'message_id';
	const PARAM__NUM_RETRIES = 'num_retries';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_DelayedMessage';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}
