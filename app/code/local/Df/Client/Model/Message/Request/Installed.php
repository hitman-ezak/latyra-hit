<?php

class Df_Client_Model_Message_Request_Installed extends Df_Client_Model_Message_Request {


	/**
	 * @override
	 * @return string
	 */
	public function getActionClassMf () {
		return Df_Server_Model_Action_Installed::getNameInMagentoFormat();
	}



	/**
	 * @return string
	 */
	public function getDomain () {
		return $this->cfg (self::PARAM__DOMAIN);
	}


	/**
	 * @return string
	 */
	public function getUrlBase () {
		return $this->cfg (self::PARAM__URL__BASE);
	}


	/**
	 * @return string
	 */
	public function getVersionMagento () {
		return $this->cfg (self::PARAM__VERSION__MAGENTO);
	}



	/**
	 * @return string
	 */
	public function getVersionPhp () {
		return $this->cfg (self::PARAM__VERSION__PHP);
	}


	/**
	 * @return string
	 */
	public function getVersionRm () {
		return $this->cfg (self::PARAM__VERSION__RM);
	}



	/**
	 * @return Df_Client_Model_Message_Request_Installed
	 */
	private function initEmpty () {

		$this
			->addData (
			    array (
					self::PARAM__DOMAIN => Mage::app()->getRequest()->getHttpHost()
					,
					self::PARAM__URL__BASE => Mage::getBaseUrl (Mage_Core_Model_Store::URL_TYPE_WEB)
					,
					self::PARAM__VERSION__MAGENTO => Mage::getVersion()
					,
					self::PARAM__VERSION__PHP => phpversion()
					,
					self::PARAM__VERSION__RM => rm_version()
				)
			)
		;

		return $this;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (self::PARAM__DOMAIN, new Df_Zf_Validate_String())
			->addValidator (self::PARAM__URL__BASE, new Df_Zf_Validate_String())
			->addValidator (self::PARAM__VERSION__MAGENTO, new Df_Zf_Validate_String())
			->addValidator (self::PARAM__VERSION__PHP, new Df_Zf_Validate_String())
			->addValidator (self::PARAM__VERSION__RM, new Df_Zf_Validate_String())
		;

		if (df_empty ($this->getData())) {
			$this->initEmpty();
		}
	}



	const PARAM__DOMAIN = 'domain';
	const PARAM__URL__BASE = 'url__base';
	const PARAM__VERSION__MAGENTO = 'version__magento';
	const PARAM__VERSION__PHP = 'version__php';
	const PARAM__VERSION__RM = 'version__rm';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Message_Request_Installed';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


