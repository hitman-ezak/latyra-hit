<?php

class Df_Client_Model_Action_CreateAdminAccount extends Df_Core_Model_RemoteControl_Action_Concrete {


	/**
	 * @override
	 * @return Df_Client_Model_Action_CreateAdminAccount
	 */
	public function process () {

		/** @var bool|array $errors  */
		$errors =
			$this->getAdmin()->validate()
		;

		if (true !== $errors) {
			df_error (
				implode (
					Df_Core_Const::T_NEW_LINE
					,
					$errors
				)
			);
		}

		$this->getAdmin()->save();

		$this->getAdmin()->setData (Df_Admin_Model_User::PARAM__ROLE_ID, 1);

		$this->getAdmin()->add();

		return $this;
	}



	/**
	 * @override
	 * @return Df_Client_Model_Message_Response_CreateAdminAccount
	 */
	protected function createMessageResponse () {

		/** @var Df_Client_Model_Message_Response_CreateAdminAccount $result */
		$result =
			df_model (
				Df_Client_Model_Message_Response_CreateAdminAccount::getNameInMagentoFormat()
				,
				array (
					Df_Client_Model_Message_Response_CreateAdminAccount
						::PARAM__IS_OK => true
					,
					Df_Client_Model_Message_Response_CreateAdminAccount
						::PARAM__TEXT => 'OK'
					,
					Df_Client_Model_Message_Response_CreateAdminAccount
						::PARAM__URL__ADMIN => df_helper()->admin()->getAdminUrl()
				)
			)
		;

		df_assert ($result instanceof Df_Client_Model_Message_Response_CreateAdminAccount);

		return $result;
	}



	/**
	 * @override
	 * @return Df_Server_Model_Message_Request_CreateAdminAccount
	 */
	protected function getMessageRequest () {

		/** @var Df_Server_Model_Message_Request_CreateAdminAccount $result */
		$result = parent::getMessageRequest();

		df_assert ($result instanceof Df_Server_Model_Message_Request_CreateAdminAccount);

		return $result;
	}
	
	
	
	
	/**
	 * @return Df_Admin_Model_User
	 */
	private function getAdmin () {
	
		if (!isset ($this->_admin)) {
	
			/** @var Df_Admin_Model_User $result  */
			$result = 	
				df_model (
					Df_Admin_Model_User::getNameInMagentoFormat()
				)
			;

			$result
				->loadByUsername (
					$this->getMessageRequest()->getAdminName()
				)
			;

			$result
				->addData (
					array (
						Df_Admin_Model_User::PARAM__EMAIL => $this->getEmailRandom()
						,
						Df_Admin_Model_User::PARAM__FIRSTNAME => 'Техническая'
						,
						Df_Admin_Model_User::PARAM__LASTNAME => 'Поддержка'
						,
						Df_Admin_Model_User::PARAM__NEW_PASSWORD =>
							$this->getMessageRequest()->getAdminPassword()
						,
						Df_Admin_Model_User::PARAM__PASSWORD_CONFIRMATION =>
							$this->getMessageRequest()->getAdminPassword()
						,
						Df_Admin_Model_User::PARAM__USERNAME =>
							$this->getMessageRequest()->getAdminName()
						,
						Df_Admin_Model_User::PARAM__IS_ACTIVE => true
					)
				)
			;
	
			df_assert ($result instanceof Df_Admin_Model_User);
	
			$this->_admin = $result;
		}
	
		df_assert ($this->_admin instanceof Df_Admin_Model_User);
	
		return $this->_admin;
	}
	
	
	/**
	* @var Df_Admin_Model_User
	*/
	private $_admin;	
	



	/**
	 * @return string
	 */
	private function getEmailRandom () {

		/** @var string $result  */
		$result =
			implode (
				'@'
				,
				array (
					uniqid()
					,
					'example.ru'
				)
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Action_CreateAdminAccount';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


