<?php

class Df_Client_Model_Dispatcher extends Df_Core_Model_Abstract {



	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function controller_action_postdispatch (
		Varien_Event_Observer $observer
	) {
		/**
		 * Иначе случится сбой «dbModel read resource does not implement Zend_Db_Adapter_Abstract»
		 */
		if (Mage::isInstalled()) {
			try {

				if (!df()->remoteControl()->isItServer()) {

					df_handle_event (
						Df_Client_Model_Handler_ResendDelayedMessages
							::getNameInMagentoFormat ()
						,
						Df_Core_Model_Event_Controller_Action_Postdispatch
							::getNameInMagentoFormat ()
						,
						$observer
					);

					/**
					 * Оповещать сервер об установке Российской сборки на мой локальный компьютер
					 * не будем, чтобы не засорять на сервере журнал установок.
					 */
					if (!df_is_it_my_local_pc()) {
						df_handle_event (
							Df_Client_Model_Handler_NotifyServerAboutInstallation
								::getNameInMagentoFormat ()
							,
							Df_Core_Model_Event_Controller_Action_Postdispatch
								::getNameInMagentoFormat ()
							,
							$observer
						);
					}

				}
			}

			catch (Exception $e) {
				df_handle_entry_point_exception ($e);
			}
		}
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Client_Model_Dispatcher';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


