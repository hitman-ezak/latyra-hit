<?php

class Df_Cms_Model_Dispatcher extends Df_Core_Model_Abstract {


	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function controller_action_layout_generate_blocks_after (
		Varien_Event_Observer $observer
	) {

		try {

			if (
					df_cfg()->cms()->hierarchy()->isEnabled()
				&&
					df_enabled (Df_Core_Feature::CMS_2)
			) {

				df_handle_event (
					Df_Cms_Model_Handler_ContentsMenu_Insert
						::getNameInMagentoFormat ()
					,
					Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter
						::getNameInMagentoFormat ()
					,
					$observer
				);

			}
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}





	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function page_block_html_topmenu_gethtml_before (
		Varien_Event_Observer $observer
	) {

		try {

			if (
					df_cfg()->cms()->hierarchy()->isEnabled()
				&&
					df_cfg()->cms()->hierarchy()->needAddToCatalogMenu()
				&&
					df_enabled (Df_Core_Feature::CMS_2)
			) {

				/** @var Varien_Data_Tree_Node $menu */
				$menu = $observer->getData ('menu');

				/** @var Df_Cms_Block_Frontend_Catalog_Navigation_Submenu $block */
				$block =
					df_mage()->core()->layout()->createBlock (
						Df_Cms_Block_Frontend_Catalog_Navigation_Submenu::getNameInMagentoFormat()
						,
						'df.cms.catalog.navigation.submenu'
					)
				;

				$block->appendMenu_1_7 ($menu);

			}
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}







	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function rm_menu_top_add_submenu (
		Varien_Event_Observer $observer
	) {

		try {

			if (
					df_cfg()->cms()->hierarchy()->isEnabled()
				&&
					df_cfg()->cms()->hierarchy()->needAddToCatalogMenu()
				&&
					df_enabled (Df_Core_Feature::CMS_2)
			) {

				/** @var Varien_Data_Tree $menu */
				$menu = $observer->getData ('menu');


				/** @var Df_Cms_Block_Frontend_Catalog_Navigation_Submenu $block */
				$block =
					df_mage()->core()->layout()->createBlock (
						Df_Cms_Block_Frontend_Catalog_Navigation_Submenu::getNameInMagentoFormat()
						,
						'df.cms.catalog.navigation.submenu'
					)
				;

				$block->appendMenu ($menu);

			}
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}







	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Cms_Model_Dispatcher';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


