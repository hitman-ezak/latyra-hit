<?php

class Df_Page_Block_Html_Topmenu extends Mage_Page_Block_Html_Topmenu {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * Обратите внимание, что Mage_Page_Block_Html_Topmenu::_construct
	 * по ошибке объявлен публичным методом, поэтому и у нас он должен быть публичным
	 *
	 * @override
	 * @return void
	 */
	public function _construct() {
		parent::_construct();
		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlTopmenu()
		) {
			$this->setData ('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}



	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result = parent::getCacheKeyInfo();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->pageHtmlTopmenu()
		) {
			$result =
				array_merge (
					$result
					,
					array (
						get_class ($this)
						,
						$this->getRequest()->getRequestUri()
					)
				)
			;
		}

		df_result_array ($result);

		return $result;
	}
}


