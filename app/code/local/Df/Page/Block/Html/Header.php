<?php

class Df_Page_Block_Html_Header extends Mage_Page_Block_Html_Header {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}


	/**
	 * @return string
	 */
    public function getWelcome()
    {
        $result = parent::getWelcome();

	    if (
				/**
				 * Избегаем зависимости модуля Df_Page от наличия модуля Df_Tweaks
				 */
				df_module_enabled (Df_Core_Module::TWEAKS)
			&&
				df_installed ()
			&&
				df_enabled (Df_Core_Feature::TWEAKS)
		) {
		    if (df_mage()->customer()->session()->isLoggedIn()) {
				if (df_cfg()->tweaks()->header()->hideWelcomeFromLoggedIn()) {
					$result = '';
				}
			    else {
					if (df_cfg()->tweaks()->header()->showOnlyFirstName()) {
						$result =
							$this->__(
								'Welcome, %s!'
								,
								$this->escapeHtml(
									df_helper()->tweaks()->customer()->getFirstNameWithPrefix ()
								)
							)
						;
					}
			    }
		    }
	    }

	    return $result;
    }

}
