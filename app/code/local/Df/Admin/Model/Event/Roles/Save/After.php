<?php



/**
 * Cообщение:		«admin_roles_save_after»
 * Источник:		Mage_Core_Model_Abstract::_afterSave()
 * [code]
        Mage::dispatchEvent('model_save_after', array('object'=>$this));
        Mage::dispatchEvent($this->_eventPrefix.'_save_after', $this->_getEventData());
 * [/code]
 *
 * Назначение:		Позволяет выполнить дополнительную обработку коллекции товарных разделов
 * 					перед её загрузкой
 */
class Df_Admin_Model_Event_Roles_Save_After extends Df_Core_Model_Event {



	/**
	 * @return Mage_Admin_Model_Roles
	 */
	public function getRole () {

		/** @var Mage_Admin_Model_Roles $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__ROLE)
		;

		df_assert ($result instanceof Mage_Admin_Model_Roles);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Event_Roles_Save_After';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'admin_roles_save_after';
	const EVENT_PARAM__ROLE = 'object';

}


