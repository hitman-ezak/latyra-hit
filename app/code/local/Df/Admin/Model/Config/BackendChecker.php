<?php

abstract class Df_Admin_Model_Config_BackendChecker
	extends Df_Core_Model_Abstract {



	abstract protected function checkInternal ();



	/**
	 * @return Df_Admin_Model_Config_BackendChecker
	 */
	public function check () {

		try {

			$this->checkInternal ();
		}
		catch (Exception $e) {
			rm_session()->addError ($e->getMessage());
		}

		return $this;
	}



	/**
	 * @return Df_Admin_Model_Config_Backend
	 */
	protected function getBackend () {

		/** @var Df_Admin_Model_Config_Backend $result  */
		$result = $this->cfg (self::PARAM__BACKEND);

		df_assert ($result instanceof Df_Admin_Model_Config_Backend);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__BACKEND, Df_Admin_Model_Config_Backend::getClass()
			)
		;
	}



	const PARAM__BACKEND = 'backend';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_BackendChecker';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


