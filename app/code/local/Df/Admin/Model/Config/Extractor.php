<?php

abstract class Df_Admin_Model_Config_Extractor extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityName ();



	/**
	 * @param  string $fieldNameUniqueSuffix
	 * @return bool
	 */
	protected function getYesNo ($fieldNameUniqueSuffix) {

		/** @var bool $result  */
		$result =
			df()->settings()->parseYesNo (
				$this->getValue (
					$fieldNameUniqueSuffix
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @param string $fieldNameUniqueSuffix
	 * @param string $defaultValue [optional]
	 * @return string
	 */
	protected function getValue ($fieldNameUniqueSuffix, $defaultValue = Df_Core_Const::T_EMPTY) {

		df_param_string ($fieldNameUniqueSuffix, 0);


		/** @var string $result  */
		$result =
			Mage::getStoreConfig (
				df()->config()->implodeKey (
					array (
						$this->getConfigGroupPath()
						,
						$this->implode (
							df_clean (
								array (
									$this->getConfigKeyPrefix()
									,
									$this->getEntityName ()
									,
									$fieldNameUniqueSuffix
								)
							)
						)
					)
				)
				,
				$this->getStore ()
			)
		;


		if (is_null ($result)) {
			$result = $defaultValue;
		}

		df_result_string ($result);

		return $result;
	}




	/**
	 * @param string $fieldNameUniqueSuffix
	 * @return string
	 */
	private function composePath ($fieldNameUniqueSuffix) {

		df_param_string ($fieldNameUniqueSuffix, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					$this->getConfigGroupPath()
					,
					$this->implode (
						df_clean (
							array (
								$this->getConfigKeyPrefix()
								,
								$this->getEntityName ()
								,
								$fieldNameUniqueSuffix
							)
						)
					)
				)
			)
		;

		df_result_string ($result);

		return $result;
	}






	/**
	 * @return string
	 */
	private function getConfigGroupPath () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CONFIG_GROUP_PATH);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getConfigKeyPrefix () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CONFIG_KEY_PREFIX);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return Mage_Core_Model_Store
	 */
	private function getStore () {

		/** @var Mage_Core_Model_Store $result  */
		$result =
			Mage::app()->getStore (
				$this->cfg (self::PARAM__STORE)
			)
		;

		df_assert ($result instanceof Mage_Core_Model_Store);

		return $result;
	}



	/**
	 * @param array $configKeyParts
	 * @return string
	 */
	private function implode (array $configKeyParts) {

		/** @var string $result  */
		$result =
			implode (
				self::T__CONFIG_PARTS_SEPARATOR
				,
				$configKeyParts
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CONFIG_KEY_PREFIX,	new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__CONFIG_GROUP_PATH,	new Df_Zf_Validate_String()
			)
			->validateClass (
				self::PARAM__STORE,	Df_Core_Const::STORE_CLASS,	false
			)
		;
	}



	const PARAM__CONFIG_GROUP_PATH = 'config_group_path';
	const PARAM__CONFIG_KEY_PREFIX = 'config_key_prefix';
	const PARAM__STORE = 'store';


	const T__CONFIG_PARTS_SEPARATOR = '__';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Extractor';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


