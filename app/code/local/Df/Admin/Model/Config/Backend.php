<?php

class Df_Admin_Model_Config_Backend extends Mage_Core_Model_Config_Data {
	
	
	/**
	 * @return Mage_Core_Model_Config_Element
	 */
	public function getFieldConfig () {
	
		/** @var Mage_Core_Model_Config_Element $result  */
		$result = $this->getData ('field_config');
	
		df_assert ($result instanceof Mage_Core_Model_Config_Element);
	
		return $result;
	}




	/**
	 * @param string $name
	 * @param bool $mustBeNonEmpty [optional]
	 * @return string
	 */
	public function getFieldConfigParam ($name, $mustBeNonEmpty = false) {

		/** @var string $result  */
		$result =
			df_a (
				$this->getFieldConfigAsCanonicalArray()
				,
				$name
				,
				Df_Core_Const::T_EMPTY
			)
		;

		if ($mustBeNonEmpty && df_empty ($result)) {
			df_error (
				'Требуется непустое значение для параметра %s'
				,
				df()->config()->implodeKey (
					array (
						$this->getData ('path')
						,
						$name
					)
				)
			);

		}


		df_result_string ($result);

		return $result;
	}

	


	/**
	 * Возвращает перечень магазинов, к которым относится текущее значение настройки
	 *
	 * @return Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection
	 */
	public function getStores () {
	
		if (!isset ($this->_stores)) {
	
			/** @var Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection $result  */
			$result = Mage::app()->getStore()->getResourceCollection();
			df()->assert()->storeCollection ($result);

			switch ($this->getScope()) {

				case self::SCOPE__DEFAULT:
					$result->setWithoutDefaultFilter ();
					break;

				case self::SCOPE__STORES:
					$result->addIdFilter ($this->getScopeId());
					break;

				case self::SCOPE__WEBSITES:
					$result->addWebsiteFilter ($this->getScopeId());
					break;

				default:
					df_error ();

			}
	
			$this->_stores = $result;
		}
	

		df()->assert()->storeCollection ($this->_stores);
	
		return $this->_stores;
	}
	
	
	/**
	* @var Mage_Core_Model_Resource_Store_Collection|Mage_Core_Model_Mysql4_Store_Collection
	*/
	private $_stores;	
	
	
	
	
	/**
	 * @return array
	 */
	public function getWebsiteIds () {
	
		if (!isset ($this->_websiteIds)) {
	
			/** @var array $result  */
			$result = array ();


			switch ($this->getScope()) {

				case self::SCOPE__DEFAULT:
					$result =
						array_keys (
							Mage::app()->getWebsites ($withDefault = false, $codeKey = false)
						)
					;
					break;

				case self::SCOPE__STORES:
					$result = array (Mage::app()->getStore($this->getScopeId())->getWebsiteId());
					break;

				case self::SCOPE__WEBSITES:
					$result = array ($this->getScopeId());
					break;

				default:
					df_error ();
			}

	
			df_assert_array ($result);
	
			$this->_websiteIds = $result;
		}
	
	
		df_result_array ($this->_websiteIds);
	
		return $this->_websiteIds;
	}
	
	
	/**
	* @var array
	*/
	private $_websiteIds;


	
	
	/**
	 * @return array
	 */
	private function getFieldConfigAsCanonicalArray () {
	
		if (!isset ($this->_fieldConfigAsCanonicalArray)) {
	
			/** @var array $result  */
			$result = $this->getFieldConfig()->asCanonicalArray();
	
			df_assert_array ($result);
	
			$this->_fieldConfigAsCanonicalArray = $result;
		}
	
	
		df_result_array ($this->_fieldConfigAsCanonicalArray);
	
		return $this->_fieldConfigAsCanonicalArray;
	}
	
	
	/**
	* @var array
	*/
	private $_fieldConfigAsCanonicalArray;	
	
	
	
	
	/**
	 * @return Mage_Core_Model_Resource_Website_Collection|Mage_Core_Model_Mysql4_Website_Collection
	 */
	private function getWebsites () {
	
		if (!isset ($this->_websites)) {
	
			/** @var Mage_Core_Model_Resource_Website_Collection|Mage_Core_Model_Mysql4_Website_Collection $result  */
			$result = Mage::app()->getWebsite()->getResourceCollection();
			df()->assert()->websiteCollection ($result);


			switch ($this->getScope()) {

				case self::SCOPE__DEFAULT:
					$result = Mage::app()->getWebsites ($withDefault = false);
					break;

				case self::SCOPE__STORES:
					$result->addIdFilter ($this->getScopeId());
					break;

				case self::SCOPE__WEBSITES:
					//$result->addWebsiteFilter ($this->getScopeId());
					$result->addIdFilter ($this->getScopeId());
					break;

				default:
					df_error ();
			}
	
			$this->_websites = $result;
		}

		df()->assert()->websiteCollection ($this->_websites);
	
		return $this->_websites;
	}
	
	
	/**
	* @var Mage_Core_Model_Resource_Website_Collection|Mage_Core_Model_Mysql4_Website_Collection
	*/
	private $_websites;	
	
	



	const SCOPE__DEFAULT = 'default';
	const SCOPE__STORES = 'stores';
	const SCOPE__WEBSITES = 'websites';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Backend';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


