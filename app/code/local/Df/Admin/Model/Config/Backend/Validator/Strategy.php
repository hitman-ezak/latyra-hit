<?php

abstract class Df_Admin_Model_Config_Backend_Validator_Strategy extends Df_Core_Model_Abstract {


	/**
	 * @abstract
	 * @return bool
	 */
	abstract public function validate ();




	/**
	 * @return Df_Admin_Model_Config_Backend_Validator
	 */
	protected function getBackend () {
		return $this->cfg (self::PARAM__BACKEND);
	}




	/**
	 * @return Mage_Core_Model_Store
	 */
	protected function getStore () {
		return $this->cfg (self::PARAM__STORE);
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__BACKEND, Df_Admin_Model_Config_Backend::getClass()
			)
			->validateClass (
				self::PARAM__STORE, Df_Core_Const::STORE_CLASS
			)
		;
	}


	const PARAM__BACKEND = 'backend';
	const PARAM__STORE = 'store';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Backend';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


