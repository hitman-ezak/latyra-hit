<?php


/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Admin_Model_Config_Source_SelectNumberFromDropdown extends Df_Admin_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
	protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result = $this->getAsOptionArray();

		df_result_array ($result);

		return $result;

    }




	
	/**
	 * @return array
	 */
	private function getAsOptionArray () {
	
		if (!isset ($this->_asOptionArray)) {
	
			
			/** @var array $result  */
			$result = array ();

			for ($i = $this->getMin(); $i <= $this->getMax(); $i += $this->getStep ()) {
				$result []=
					array (
						self::OPTION_KEY__LABEL => $i
						,
						self::OPTION_KEY__VALUE => $i
					)
				;
			}


			df_assert_array ($result);
	
			$this->_asOptionArray = $result;
		}
	
	
		df_result_array ($this->_asOptionArray);
	
		return $this->_asOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_asOptionArray;





	/**
	 * @return int
	 */
	private function getMax () {

		/** @var int $result  */
		$result = intval ($this->getFieldParam (self::CONFIG_PARAM__DF_MAX, 10));

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @return int
	 */
	private function getMin () {

		/** @var int $result  */
		$result = intval ($this->getFieldParam (self::CONFIG_PARAM__DF_MIN, 1));

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @return int
	 */
	private function getStep () {

		/** @var int $result  */
		$result = intval ($this->getFieldParam (self::CONFIG_PARAM__DF_STEP, 1));

		df_result_integer ($result);

		return $result;
	}



	const CONFIG_PARAM__DF_MIN = 'df_min';
	const CONFIG_PARAM__DF_MAX = 'df_max';
	const CONFIG_PARAM__DF_STEP = 'df_step';
	const CONFIG_PARAM__DF_FORMAT = 'df_format';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source_SelectNumberFromDropdown';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}