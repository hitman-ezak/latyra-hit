<?php


/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Admin_Model_Config_Source_YesNoDev extends Df_Admin_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
    protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result = $this->getAsOptionArray();

		df_result_array ($result);

		return $result;

    }




	/**
	 * @return array
	 */
	private function getAsOptionArray () {
	
		if (!isset ($this->_asOptionArray)) {
	
			
			/** @var array $result  */
			$result =
				array (

					array (
						self::OPTION_KEY__LABEL => 'да'
						,
						self::OPTION_KEY__VALUE => self::VALUE__YES
					)

					,
					array (
						self::OPTION_KEY__LABEL => 'нет'
						,
						self::OPTION_KEY__VALUE => self::VALUE__NO
					)

					,
					array (
						self::OPTION_KEY__LABEL =>
							sprintf (
								'только при %s режиме разработчика'
								,
									$this->needEnableInDeveloperMode()
								?
									'включенном'
								:
									'отключенном'
							)

						,
						self::OPTION_KEY__VALUE => self::VALUE__DEVELOPER_MODE
					)

				)
			;

			df_assert_array ($result);
	
			$this->_asOptionArray = $result;
		}
	
	
		df_result_array ($this->_asOptionArray);
	
		return $this->_asOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_asOptionArray;




	/**
	 * @return bool
	 */
	private function needEnableInDeveloperMode () {

		/** @var int $result  */
		$result =
				0
			!==
				intval (
					$this->getFieldParam (
						self::CONFIG_PARAM__DF_ENABLE_IN_DEVELOPER_MODE, 1
					)
				)
		;

		df_result_boolean ($result);

		return $result;
	}



	const CONFIG_PARAM__DF_ENABLE_IN_DEVELOPER_MODE = 'df_enable_in_developer_mode';


	const VALUE__DEVELOPER_MODE = 'developer-mode';
	const VALUE__NO = 'no';
	const VALUE__YES = 'yes';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source_YesNoDev';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}