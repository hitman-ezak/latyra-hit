<?php


/**
 * @singleton
 * Система создаёт объект-одиночку для потомков этого класса.
 * Не забывайте об этом при реализации кеширования результатов вычислений внутри этого класса!
 */
class Df_Admin_Model_Config_Source_RemoveIfEmpty extends Df_Admin_Model_Config_Source {



	/**
	 * @override
	 * @param bool $isMultiSelect
	 * @return array
	 */
    protected function toOptionArrayInternal ($isMultiSelect = false) {

		/** @var array $result  */
		$result = $this->getAsOptionArray();

		df_result_array ($result);

		return $result;

    }




	/**
	 * @return array
	 */
	private function getAsOptionArray () {
	
		if (!isset ($this->_asOptionArray)) {
	
			
			/** @var array $result  */
			$result =
				array (

					array (
						self::OPTION_KEY__LABEL => 'не удалять'
						,
						self::OPTION_KEY__VALUE => self::VALUE__NO_REMOVE
					)

					,
					array (
						self::OPTION_KEY__LABEL => 'удалить'
						,
						self::OPTION_KEY__VALUE => self::VALUE__REMOVE
					)

					,
					array (
						self::OPTION_KEY__LABEL => 'удалить, если пуст'
						,
						self::OPTION_KEY__VALUE => self::VALUE__REMOVE_IF_EMPTY
					)

				)
			;

			df_assert_array ($result);
	
			$this->_asOptionArray = $result;
		}
	
	
		df_result_array ($this->_asOptionArray);
	
		return $this->_asOptionArray;
	}
	
	
	/**
	* @var array
	*/
	private $_asOptionArray;




	const VALUE__NO_REMOVE = '0';
	const VALUE__REMOVE = '1';
	const VALUE__REMOVE_IF_EMPTY = 'remove-if-empty';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Model_Config_Source_RemoveIfEmpty';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}