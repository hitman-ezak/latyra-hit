<?php


/**
 * Обратите внимание, что Magento не создаёт отдельные экземпляры данного класса
 * для вывода каждого поля!
 * Magento использует ЕДИНСТВЕННЫЙ экземпляр данного класса для вывода всех полей!
 * Поэтому в объектах данного класса нельзя кешировать информацию,
 * которая индивидуальна для поля конкретного поля!
 */
class Df_Admin_Block_System_Config_Form_Field
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface {


	/**
	 * @return Df_Admin_Model_Form_Element
	 */
	public function getElement () {
		return $this->_element;
	}



	/**
	 * @return string
	 */
	public function getFeatureState () {
		return $this->getElement()->getFeatureInfo()->getState();
	}



	/**
	 * @return string
	 */
	public function getFeatureStateText () {
		return $this->getElement()->getFeatureInfo()->getStateText();
	}



	/**
	 * @return string
	 */
	public function getFeatureTitle () {
		return $this->getElement()->getFeatureInfo()->getTitle();
	}



	/**
	 * @return bool
	 */
	public function isFeatureDisabledForAllStoresInCurrentScope () {
		return $this->getElement()->getFeatureInfo()->isDisabledForAllStoresInCurrentScope();
	}



	/**
	 * @return bool
	 */
	public function isFeatureSpecified () {

		/**
		 * Обратите внимание, что Magento не создаёт отдельные экземпляры данного класса
		 * для вывода каждого поля!
		 * Magento использует ЕДИНСТВЕННЫЙ экземпляр данного класса для вывода всех полей!
		 * Поэтому в объектах данного класса нельзя кешировать информацию,
		 * которая индивидуальна для поля конкретного поля!
		 */

		/** @var bool $result  */
		$result =
			!is_null (
				$this->getElement()->getFeatureCode()
			)
		;


		df_assert_boolean ($result);

		return $result;
	}




    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render (Varien_Data_Form_Element_Abstract $element) {

	    $result =
			$this
				->setElement (
					$this->wrap ($element)
				)
				->setTemplate (self::DF_TEMPLATE)
				->renderView()
		;
		df_result_string ($result);
		return $result;
    }



	/**
	 * @param Df_Admin_Model_Form_Element $element
	 * @return Df_Admin_Block_System_Config_Form_Field
	 */
	public function setElement (Df_Admin_Model_Form_Element $element) {
		$this->_element = $element;
		return $this;
	}


	/**
	 * @var Df_Admin_Model_Form_Element
	 */
	private $_element;





	/**
	 * @param Varien_Data_Form_Element_Abstract $element
	 * @return Df_Admin_Model_Form_Element
	 */
	private function wrap (Varien_Data_Form_Element_Abstract $element) {
		$result =
			df_model (
				Df_Admin_Model_Form_Element::getNameInMagentoFormat()
				,
				array (
					Df_Admin_Model_Form_Element::PARAM_WRAPPED_ELEMENT => $element
				)
			)
		;

		df_assert ($result instanceof Df_Admin_Model_Form_Element);

		return $result;
	}



	const DF_TEMPLATE = 'df/admin/system/config/form/field.phtml';

}