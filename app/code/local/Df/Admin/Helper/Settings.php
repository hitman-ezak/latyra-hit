<?php

class Df_Admin_Helper_Settings extends Df_Core_Helper_Settings {


	/**
	 * @return Df_1C_Helper_Settings
	 */
	public function _1c () {

		/** @var Df_1C_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_1C_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_1C_Helper_Settings);
		}

		return $result;
	}



	/**
	 * @return Df_Admin_Helper_Settings_Admin
	 */
	public function admin () {

		/** @var Df_Admin_Helper_Settings_Admin $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Admin_Helper_Settings_Admin::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Admin_Helper_Settings_Admin);
		}

		return $result;
	}





	/**
	 * @return Df_Catalog_Helper_Settings
	 */
	public function catalog () {

		/** @var Df_Catalog_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Catalog_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Catalog_Helper_Settings);
		}

		return $result;
	}




	
	/**
	 * @return Df_Checkout_Helper_Settings
	 */
	public function checkout () {

		/** @var Df_Checkout_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Checkout_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Checkout_Helper_Settings);
		}

		return $result;
	}





	/**
	 * @return Df_Chronopay_Helper_Settings
	 */
	public function chronopay () {

		/** @var Df_Chronopay_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Chronopay_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Chronopay_Helper_Settings);
		}

		return $result;
	}
	
	
	


	/**
	 * @return Df_Cms_Helper_Settings
	 */
	public function cms () {
	
		/** @var Df_Cms_Helper_Settings $result */
		static $result;
	
		if (!isset ($result)) {
	
			/** @var Df_Cms_Helper_Settings $result  */
			$result = Mage::helper (Df_Cms_Helper_Settings::getNameInMagentoFormat());
	
			df_assert ($result instanceof Df_Cms_Helper_Settings);
		}
	
		return $result;
	}	
	
	




	/**
	 * @return Df_Admin_Helper_Settings_Common
	 */
	public function common () {

		/** @var Df_Admin_Helper_Settings_Common $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Admin_Helper_Settings_Common::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Admin_Helper_Settings_Common);
		}

		return $result;
	}




	/**
	 * @return Df_Customer_Helper_Settings
	 */
	public function customer () {

		/** @var Df_Customer_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Customer_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Customer_Helper_Settings);
		}

		return $result;
	}




	
	/**
	 * @return Df_Dataflow_Helper_Settings
	 */
	public function dataflow () {

		/** @var Df_Dataflow_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Dataflow_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Dataflow_Helper_Settings);
		}

		return $result;
	}




	
	/**
	 * @return Df_Directory_Helper_Settings
	 */
	public function directory () {

		/** @var Df_Directory_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Directory_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Directory_Helper_Settings);
		}

		return $result;
	}



	/**
	 * @return Df_Index_Helper_Settings
	 */
	public function index () {

		/** @var Df_Index_Helper_Settings $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Index_Helper_Settings $result  */
			$result = Mage::helper (Df_Index_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Index_Helper_Settings);
		}

		return $result;
	}



	/**
	 * @return Df_Localization_Helper_Settings
	 */
	public function localization () {

		/** @var Df_Localization_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Localization_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Localization_Helper_Settings);
		}

		return $result;
	}





	/**
	 * @return Df_Logging_Helper_Settings
	 */
	public function logging () {

		/** @var Df_Logging_Helper_Settings $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Logging_Helper_Settings $result  */
			$result = Mage::helper (Df_Logging_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Logging_Helper_Settings);
		}

		return $result;
	}




	/**
	 * @return Df_Newsletter_Helper_Settings
	 */
	public function newsletter () {

		/** @var Df_Newsletter_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {
			$result = Mage::helper (Df_Newsletter_Helper_Settings::getNameInMagentoFormat());
		}

		return $result;
	}






	/**
	 * @return Df_Promotion_Helper_Settings
	 */
	public function promotion () {

		/** @var Df_Promotion_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Promotion_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Promotion_Helper_Settings);
		}

		return $result;
	}






	/**
	 * @return Df_Reward_Helper_Settings
	 */
	public function reward () {

		/** @var Df_Reward_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Reward_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Reward_Helper_Settings);
		}
		return $result;
	}






	/**
	 * @return Df_Reports_Helper_Settings
	 */
	public function reports () {

		/** @var Df_Reports_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Reports_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Reports_Helper_Settings);
		}

		return $result;
	}





	/**
	 * @return Df_Sales_Helper_Settings
	 */
	public function sales () {

		/** @var Df_Sales_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Sales_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Sales_Helper_Settings);
		}

		return $result;
	}





	/**
	 * @return Df_Seo_Helper_Settings
	 */
	public function seo () {

		/** @var Df_Seo_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Seo_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Seo_Helper_Settings);
		}

		return $result;
	}

	
	
	
	
	/**
	 * @return Df_Shipping_Helper_Settings
	 */
	public function shipping () {
	
		/** @var Df_Shipping_Helper_Settings $result */
		static $result;
	
		if (!isset ($result)) {
	
			/** @var Df_Shipping_Helper_Settings $result  */
			$result = Mage::helper (Df_Shipping_Helper_Settings::getNameInMagentoFormat());
	
			df_assert ($result instanceof Df_Shipping_Helper_Settings);
		}
	
		return $result;
	}	
	





	/**
	 * @return Df_Speed_Helper_Settings
	 */
	public function speed () {

		/** @var Df_Speed_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {
			$result = Mage::helper (Df_Speed_Helper_Settings::getNameInMagentoFormat());
		}

		return $result;
	}


	
	
	
	/**
	 * @return Df_Torg12_Helper_Settings
	 */
	public function torg12 () {
	
		/** @var Df_Torg12_Helper_Settings $result */
		static $result;
	
		if (!isset ($result)) {
	
			/** @var Df_Torg12_Helper_Settings $result  */
			$result = Mage::helper (Df_Torg12_Helper_Settings::getNameInMagentoFormat());
	
			df_assert ($result instanceof Df_Torg12_Helper_Settings);
		}
	
		return $result;
	}		
	
	



	/**
	 * @return Df_Tweaks_Helper_Settings
	 */
	public function tweaks () {

		/** @var Df_Tweaks_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {
			$result = Mage::helper (Df_Tweaks_Helper_Settings::getNameInMagentoFormat());
		}

		return $result;
	}




	/**
	 * @return Df_Vk_Helper_Settings
	 */
	public function vk () {

		/** @var Df_Vk_Helper_Settings $result  */
		static $result;

		if (!isset ($result)) {

			$result = Mage::helper (Df_Vk_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Vk_Helper_Settings);
		}

		return $result;
	}




	/**
	 * @return Df_YandexMarket_Helper_Settings
	 */
	public function yandexMarket () {

		/** @var Df_YandexMarket_Helper_Settings $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_YandexMarket_Helper_Settings $result  */
			$result = Mage::helper (Df_YandexMarket_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_YandexMarket_Helper_Settings);
		}

		return $result;
	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}