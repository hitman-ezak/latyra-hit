<?php

class Df_Admin_Helper_Settings_Admin_Interface extends Df_Core_Helper_Settings {
	
	
	/**
	 * @return Df_Admin_Model_Config_Extractor_Font
	 */
	public function getButtonLabelFont () {

		if (!isset ($this->_buttonLabelFont)) {

			/** @var Df_Admin_Model_Config_Extractor_Font $result  */
			$result =
				df_model (
					Df_Admin_Model_Config_Extractor_Font::getNameInMagentoFormat()
					,
					array (
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_GROUP_PATH =>
							self::CONFIG_GROUP_PATH

						,
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_KEY_PREFIX =>
							self::CONFIG_KEY_PREFIX__BUTTON_LABEL
					)
				)
			;


			df_assert ($result instanceof Df_Admin_Model_Config_Extractor_Font);

			$this->_buttonLabelFont = $result;
		}


		df_assert ($this->_buttonLabelFont instanceof Df_Admin_Model_Config_Extractor_Font);

		return $this->_buttonLabelFont;

	}


	/**
	* @var Df_Admin_Model_Config_Extractor_Font
	*/
	private $_buttonLabelFont;
	
	
	
	
	
	/**
	 * @return Df_Admin_Model_Config_Extractor_Font
	 */
	public function getGridLabelFont () {

		if (!isset ($this->_gridLabelFont)) {

			/** @var Df_Admin_Model_Config_Extractor_Font $result  */
			$result =
				df_model (
					Df_Admin_Model_Config_Extractor_Font::getNameInMagentoFormat()
					,
					array (
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_GROUP_PATH =>
							self::CONFIG_GROUP_PATH

						,
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_KEY_PREFIX =>
							self::CONFIG_KEY_PREFIX__GRID_LABEL
					)
				)
			;


			df_assert ($result instanceof Df_Admin_Model_Config_Extractor_Font);

			$this->_gridLabelFont = $result;
		}


		df_assert ($this->_gridLabelFont instanceof Df_Admin_Model_Config_Extractor_Font);

		return $this->_gridLabelFont;

	}


	/**
	* @var Df_Admin_Model_Config_Extractor_Font
	*/
	private $_gridLabelFont;	
	
	
	
	
	/**
	 * @return Df_Admin_Model_Config_Extractor_Font
	 */
	public function getFormLabelFont () {

		if (!isset ($this->_formLabelFont)) {

			/** @var Df_Admin_Model_Config_Extractor_Font $result  */
			$result =
				df_model (
					Df_Admin_Model_Config_Extractor_Font::getNameInMagentoFormat()
					,
					array (
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_GROUP_PATH =>
							self::CONFIG_GROUP_PATH

						,
						Df_Admin_Model_Config_Extractor_Font::PARAM__CONFIG_KEY_PREFIX =>
							self::CONFIG_KEY_PREFIX__FORM_LABEL
					)
				)
			;


			df_assert ($result instanceof Df_Admin_Model_Config_Extractor_Font);

			$this->_formLabelFont = $result;
		}


		df_assert ($this->_formLabelFont instanceof Df_Admin_Model_Config_Extractor_Font);

		return $this->_formLabelFont;

	}


	/**
	* @var Df_Admin_Model_Config_Extractor_Font
	*/
	private $_formLabelFont;	
	
	
	
	
	const CONFIG_GROUP_PATH = 'df_tweaks_admin/interface';
	const CONFIG_KEY_PREFIX__FORM_LABEL = 'form_label';
	const CONFIG_KEY_PREFIX__GRID_LABEL = 'grid_label';
	const CONFIG_KEY_PREFIX__BUTTON_LABEL = 'button_label';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings_Admin_Interface';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}