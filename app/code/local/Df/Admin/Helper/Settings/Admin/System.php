<?php

class Df_Admin_Helper_Settings_Admin_System extends Df_Core_Helper_Settings {

	/**
	 * @return Df_Admin_Helper_Settings_Admin_System_Configuration
	 */
	public function configuration () {

		/** @var Df_Admin_Helper_Settings_Admin_System_Configuration $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_System_Configuration::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_System_Configuration);

		return $result;
	}

	


	/**
	 * @return Df_AdminNotification_Helper_Settings
	 */
	public function notifications () {

		/** @var Df_AdminNotification_Helper_Settings $result  */
		$result = Mage::helper (Df_AdminNotification_Helper_Settings::getNameInMagentoFormat());

		df_assert ($result instanceof Df_AdminNotification_Helper_Settings);

		return $result;
	}




	/**
	 * @return Df_Admin_Helper_Settings_Admin_System_Tools
	 */
	public function tools () {

		/** @var Df_Admin_Helper_Settings_Admin_System_Tools $result  */
		$result = Mage::helper (Df_Admin_Helper_Settings_Admin_System_Tools::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Admin_Helper_Settings_Admin_System_Tools);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings_Admin_System';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}