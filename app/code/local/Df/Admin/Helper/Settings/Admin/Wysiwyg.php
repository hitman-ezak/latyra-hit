<?php

class Df_Admin_Helper_Settings_Admin_Wysiwyg extends Df_Core_Helper_Settings {



	/**
	 * @return boolean
	 */
	public function fixImages () {

		/** @var bool $result  */
		$result =
			$this->getYesNo (
				'df_tweaks_admin/wysiwyg/fix_images'
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @return boolean
	 */
	public function fixHeadersAlreadySent () {

		/** @var bool $result  */
		$result =
			$this->getYesNo (
				'df_tweaks_admin/wysiwyg/fix_headers_already_sent'
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Admin_Helper_Settings_Admin_Wysiwyg';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}