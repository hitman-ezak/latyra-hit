<?php

class Df_GoogleAnalytics_Block_Ga extends Mage_GoogleAnalytics_Block_Ga {


	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result = parent::getCacheKeyInfo();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->googleAnalytics()
		) {
			$result =
				array_merge (
					$result
					,
					array (
						get_class ($this)
						,
						Mage::app()->getStore()->getId()
						,
						$this->getPageName()
					)
					,
					df_a ($this->getData(), 'order_ids', array())
				)
			;
		}

		df_result_array ($result);

		return $result;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->googleAnalytics()
		) {
			$this->setData ('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}

}

