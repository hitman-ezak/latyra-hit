<?php

class Df_Varien_Data_Form_Element_Abstract extends Varien_Data_Form_Element_Abstract {

	const PARAM__CAN_USE_WEBSITE_VALUE = 'can_use_website_value';
	const PARAM__CAN_USE_DEFAULT_VALUE = 'can_use_default_value';
	const PARAM__DEFAULT_VALUE = 'default_value';
	const PARAM__DISABLED = 'disabled';
	const PARAM__EXT_TYPE = 'ext_type';
	const PARAM__INHERIT = 'inherit';
	const PARAM__LABEL = 'label';
	const PARAM__NAME = 'name';
	const PARAM__REQUIRED = 'required';
	const PARAM__TITLE = 'title';
	const PARAM__VALUES = 'values';

	const TYPE__DATE = 'date';
	const TYPE__DATETIME = 'datetime';
	const TYPE__HIDDEN = 'hidden';
	const TYPE__SELECT = 'select';
	const TYPE__TEXT = 'text';
	const TYPE__TEXTAREA = 'textarea';

}


