<?php

class Df_Varien_Data_Form_Element_Map extends Df_Varien_Data_Form_Element_Abstract {

	/**
	 * @param array $attributes
	 * @return Df_Varien_Data_Form_Element_Map
	 */
	public function __construct ($attributes=array()) {
		parent::__construct ($attributes);
		//$this->setType (self::TYPE);
	}


	/**
	 * @override
	 * @return string
	 */
	public function getAfterElementHtml () {
		/** @var string $result */
		$result =
			self::getClass()
		;
		df_result_string ($result);
		return $result;
	}


	/**                                   
	 * Перед отображением элемента устанавливаем ему тип hidden,
	 * а затем возвращаем тип элемента назад.
	 * 
	 * Обратите внимание, что мы не можем вместо этого установить элементу тип hidden в конструкторе,
	 * потому что иначе шаблон widget/form/renderer/fieldset/element.phtml
	 * не отобразит элемент вовсе.
	 * 
	 * @override
	 * @return string
	 */
	public function getElementHtml () {

		/** @var string|null $type */
		$type = $this->getType();
		$this->setType (self::TYPE__HIDDEN);

		/** @var string $result */
		$result = parent::getElementHtml();

		$this->setType ($type);

		df_result_string ($result);
		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Varien_Data_Form_Element_Map';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


