<?php

class Df_Checkout_Block_Cart_Sidebar extends Mage_Checkout_Block_Cart_Sidebar {

	/**
	 * @override
	 * @return string
	 */
	public function __ () {
		/** @var array $args  */
		$args = func_get_args();

		/** @var string $result  */
		$result = df_helper()->localization()->translation()->translateByParent ($args, $this);

		return $result;
	}



	/**
	 * @override
	 * @return array
	 */
	public function getCacheKeyInfo() {

		/** @var array $result  */
		$result = parent::getCacheKeyInfo();

		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->checkoutCartSidebar()
		) {
			$result =
				array_merge (
					$result
					,
					array (
						get_class ($this)
					)
					,
					$this->getAdditionalKeys()
				)
			;
		}

		df_result_array ($result);

		return $result;
	}




	/**
	 * @return array
	 */
	private function getAdditionalKeys () {

		/** @var array $result  */
		$result =
			array (
				$this->getSummaryCount()
				,
				$this->getSubtotal()
			)
		;

		foreach ($this->getRecentItems() as $quoteItem) {
			/** @var Mage_Sales_Model_Quote_Item $quoteItem */
			$result []= $quoteItem->getProductId();
			$result []= $quoteItem->getQty();
		}

		df_result_array ($result);

		return $result;
	}





	/**
	 * @override
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		if (
				df_module_enabled (Df_Core_Module::SPEED)
			&&
				df_cfg()->speed()->blockCaching()->checkoutCartSidebar()
		) {
			$this->setData ('cache_lifetime', Df_Core_Block_Template::CACHE_LIFETIME_STANDARD);
		}
	}
}
