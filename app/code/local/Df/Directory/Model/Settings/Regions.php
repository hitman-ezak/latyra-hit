<?php

class Df_Directory_Model_Settings_Regions extends Df_Core_Model_Abstract {


	/**
	 * @return boolean
	 */
	public function getEnabled () {

		/** @var bool $result  */
		$result =
			$this->helper()->getYesNo (
				$this->getConfigKeyFull (
					'enabled'
				)
			)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @param int $position
	 * @return int
	 */
	public function getPriorityRegionIdAtPosition ($position) {

		df_param_integer ($position, 0);
		df_param_between ($position, 0, 1, self::NUM_PRIORITY_REGIONS);

		/** @var int $result  */
		$result =
			intval (
				Mage::getStoreConfig (
					$this->getConfigKeyFull (
						sprintf (
							'position_%d'
							,
							$position
						)
					)
				)
			)
		;

		df_result_integer ($result);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Settings
	 */
	private function helper () {

		/** @var Df_Core_Helper_Settings $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Settings $result  */
			$result = Mage::helper (Df_Core_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Settings);
		}

		return $result;
	}



	/**
	 * @param string $configKeyShort
	 * @return string
	 */
	private function getConfigKeyFull ($configKeyShort) {

		df_param_string ($configKeyShort, 0);

		/** @var string $result  */
		$result =
			df()->config()->implodeKey (
				array (
					'df_directory'
					,
					$this->getConfigKeyCountryPart()
					,
					$configKeyShort
				)
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string
	 */
	private function getConfigKeyCountryPart () {
		return $this->cfg (self::PARAM__CONFIG_KEY_COUNTRY_PART);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__CONFIG_KEY_COUNTRY_PART, new Df_Zf_Validate_String()
			)
		;
	}


	const NUM_PRIORITY_REGIONS = 5;
	const PARAM__CONFIG_KEY_COUNTRY_PART = 'config_key_country_part';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Settings_Regions';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


