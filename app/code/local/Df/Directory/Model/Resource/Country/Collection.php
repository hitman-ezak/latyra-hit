<?php

class Df_Directory_Model_Resource_Country_Collection
	extends Mage_Directory_Model_Resource_Country_Collection {


	/**
	 * @param string $iso2Code
	 * @return string
	 */
	public function getLocalizedNameByIso2Code ($iso2Code) {
		/** @var string $result */
		$result =
			df_a (
				$this->getMapFromIso2CodeToLocalizedName()
				,
				$iso2Code				
			)
		;
		df_result_string ($result);
		return $result;
	}
	
	
		
	/**
	 * @return array
	 */
	private function getMapFromIso2CodeToLocalizedName () {
		if (!isset ($this->_mapFromIso2CodeToLocalizedName)) {
	
			/** @var array $result  */
			$result = array ();

			foreach ($this as $country) {

				/** @var string $localizedName */
				$localizedName =
					Mage::app()->getLocale()->getCountryTranslation (
						$country->getName()
					)
				;

				if (df_empty ($localizedName)) {
					$localizedName = $country->getName();
				}

				/** @var Mage_DirectorY_Model_Country $country */
				$result[$country->getIso2Code()] =
					$localizedName
				;
			}
	
			df_result_array ($result);
			$this->_mapFromIso2CodeToLocalizedName = $result;
		}
		return $this->_mapFromIso2CodeToLocalizedName;
	}
	/** @var array */
	private $_mapFromIso2CodeToLocalizedName;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Resource_Country_Collection';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		return 'df_directory/country_collection';
	}

}


