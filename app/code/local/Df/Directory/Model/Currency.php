<?php

class Df_Directory_Model_Currency extends Mage_Directory_Model_Currency {


	/**
	 * Родительский метод работает некорректно, если $toCurrency — строка
	 *
	 * @param float $price
	 * @param string|Mage_Directory_Model_Currency $toCurrency
	 * @override
	 * @return float|null
	 * @throws Exception
	 */
	public function convert ($price, $toCurrency=null) {

		if (!is_string ($toCurrency)) {
			df_assert ($toCurrency instanceof Mage_Directory_Model_Currency);
		}

		/** @var float $result */
		$result = null;

		if (!is_null ($toCurrency)) {
			/** @var float|null $rate */
			$rate =
				$this->getRate (
					$toCurrency
				)
			;

			if (!df_empty ($rate)) {
				$result =
					$price * $rate
				;
			}
			else {
				throw
					new Exception (
						Mage::helper ('directory')->__(
							'Undefined rate from "%s-%s".'
							,
							$this->getCode()

							/**
							 * Ради этой правки перекрыт родительский метод
							 */
							, is_string ($toCurrency)
							?
								$toCurrency
							:
								$toCurrency->getCode()
						)
					)
				;
			}
		}
		return $result;
	}


	/**
	 * @param string|int|float $price
	 * @param array $options
	 * @return string
	 */
    public function formatTxt ($price, $options=array()) {
		$result =
				(
						df_enabled(Df_Core_Feature::LOCALIZATION)
					&&
						df_area (
							df_cfg ()->localization ()->translation()->frontend()->needHideDecimals()
							,
							df_cfg ()->localization ()->translation()->admin()->needHideDecimals()
						)
				)
			?
				$this->formatTxtDf ($price, $options)
			:
				parent::formatTxt ($price, $options)
		;

	    return $result;
    }



	/**
	 * @param string|int|float $price
	 * @param array $options
	 * @return string
	 */
    private function formatTxtDf ($price, $options=array())
    {
        return
			parent::formatTxt (
				$price
				,
				array_merge (
					$options
					,
					array (
					      'precision' => df_helper()->directory()->currency()->getPrecision ()
					)
				)

			)
		;
    }



	/**
	 * @param string|int|float $price
	 * @param array $options
	 * @param bool $includeContainer
	 * @param bool $addBrackets
	 * @return string
	 */
	public function format ($price, $options=array(), $includeContainer = true, $addBrackets = false)
    {
        return
				(
						df_enabled(Df_Core_Feature::LOCALIZATION)
					&&
						df_area (
							df_cfg ()->localization ()->translation()->frontend()->needHideDecimals()
							,
							df_cfg ()->localization ()->translation()->admin()->needHideDecimals()
						)
				)
			?
				$this->formatDf ($price, $options, $includeContainer, $addBrackets)
			:
				parent::format ($price, $options, $includeContainer, $addBrackets)
		;
    }


	/**
	 * @param string|int|float $price
	 * @param array $options
	 * @param bool $includeContainer
	 * @param bool $addBrackets
	 * @return string
	 */
    private function formatDf ($price, $options=array(), $includeContainer = true, $addBrackets = false)
    {
	    return
			$this->formatPrecision(
				$price
				,
				df_helper()->directory()->currency()->getPrecision ()
				,
				$options
				,
				$includeContainer
				,
				$addBrackets
			)
		;
    }
}
