<?php

class Df_Directory_Model_Currency_Import_Russia
	extends Df_Directory_Model_Currency_Import_XmlStandard {


	/**
	 * @protected
	 * @return string
	 */
	protected function getBaseCurrencyCode() {
		return Df_Directory_Const::CURRENCY__RUB;
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getName() {
		return 'Банк России';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_CurrencyCode() {
		return 'CharCode';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_CurrencyItem() {
		return 'Valute';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_Denominator() {
		return 'Nominal';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getTagName_Rate() {
		return 'Value';
	}


	/**
	 * @override
	 * @return string
	 */
	protected function getUrl() {
		return 'http://www.cbr.ru/scripts/XML_daily.asp';
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Directory_Model_Currency_Import_Russia';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


