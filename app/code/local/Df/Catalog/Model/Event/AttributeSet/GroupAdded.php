<?php


/**
 * Cообщение:		«df_catalog__attribute_set__group_added»
 *
 * Источник:		Df_Catalog_Helper_Product::addGroupToAttributeSetIfNeeded()
 * [code]
 * [/code]
 */
class Df_Catalog_Model_Event_AttributeSet_GroupAdded extends Df_Core_Model_Event {


	/**
	 * @return int
	 */
	public function getAttributeSetId () {

		/** @var int $result */
		$result = intval ($this->getEventParam(self::EVENT_PARAM__ATTRIBUTE_SET_ID));

		df_result_between ($result, 1);

		return $result;
	}
	
	
	
	/**
	 * @return Mage_Eav_Model_Entity_Attribute_Set
	 */
	public function getAttributeSet () {
	
		if (!isset ($this->_attributeSet)) {

			/** @var Mage_Eav_Model_Entity_Attribute_Set $result  */
			$result =
				df_model (
					Df_Eav_Const::CLASS__ENTITY_ATTRIBUTE_SET
				)
			;

			df_assert ($result instanceof Mage_Eav_Model_Entity_Attribute_Set);

			$result->load ($this->getAttributeSetId());
	
			df_assert (intval ($result->getId()) === $this->getAttributeSetId());
	
			$this->_attributeSet = $result;
		}
	
		df_assert ($this->_attributeSet instanceof Mage_Eav_Model_Entity_Attribute_Set);
	
		return $this->_attributeSet;
	}
	
	
	/**
	* @var Mage_Eav_Model_Entity_Attribute_Set
	*/
	private $_attributeSet;	


	
	
	/**
	 * @return int
	 */
	public function getGroupName () {
		return $this->getEventParam(self::EVENT_PARAM__GROUP_NAME);
	}	




	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EVENT;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Catalog_Model_Event_AttributeSet_GroupAdded';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}


	const EVENT = 'df_catalog__attribute_set__group_added';
	const EVENT_PARAM__ATTRIBUTE_SET_ID = 'attribute_set_id';
	const EVENT_PARAM__GROUP_NAME = 'group_name';

}


