<?php

abstract class Df_Core_Block_Admin_Entity_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getBuilderClassMf();



	/**
	 * @override
	 * @return Mage_Adminhtml_Block_Widget_Form
	 */
	protected function _prepareForm() {
		$this->getBuilder()->run();
		$this->setForm ($this->getBuilder()->getForm());
		return parent::_prepareForm();
	}



	/**
	 * @return Df_Core_Model_Form_Builder
	 */
	private function getBuilder () {

		if (!isset ($this->_builder)) {

			/** @var Varien_Data_Form $form */
			$form =
				new Varien_Data_Form (
					array (
						'id' => 'edit_form'
						,
						'action' => $this->getData('action')
						,
						'method' => 'post'
					)
				)
			;

			$form->setData('use_container', true);

			/** @var Varien_Data_Form_Element_Fieldset $fieldset */
			$fieldset =
				$form
					->addFieldset(
						'base_fieldset'
						,
						array (
							'legend' => 'Основное'
						)
					)
			;
			df_assert ($fieldset instanceof Varien_Data_Form_Element_Fieldset);


			/** @var Df_Core_Model_Form_Builder $result  */
			$result =
				df_model (
					$this->getBuilderClassMf()
					,
					array (
						Df_Core_Model_Form_Builder::PARAM__FIELDSET	=> $fieldset
					)
				)
			;

			df_assert ($result instanceof Df_Core_Model_Form_Builder);

			$this->_builder = $result;
		}

		df_assert ($this->_builder instanceof Df_Core_Model_Form_Builder);

		return $this->_builder;
	}


	/**
	* @var Df_Core_Model_Form_Builder
	*/
	private $_builder;




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Admin_Entity_Edit_Form';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}

