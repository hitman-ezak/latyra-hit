<?php

abstract class Df_Core_Block_Admin_Entity_Index_Grid extends Mage_Adminhtml_Block_Widget_Grid {	

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getCollectionClassMf();
	
	
	/**
	 * @return Varien_Data_Collection
	 */
	public function getCollection () {
		if (!isset ($this->_collection)) {
			/** @var Varien_Data_Collection $result */
			$result =
				Mage::getResourceModel (
					$this->getCollectionClassMf()
				)
			;
			df_assert ($result instanceof Varien_Data_Collection);
			$this->_collection = $result;
		}
		return $this->_collection;
	}


    /**
	 * @override
     * @param Df_Core_Model_Abstract $row
     * @return string
     */
    public function getRowUrl ($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }


	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct();
		$this->setId (get_class ($this));
		$this->setData ('use_ajax', true);
		$this->setSaveParametersInSession (true);
	}


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Admin_Entity_Index_Grid';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

