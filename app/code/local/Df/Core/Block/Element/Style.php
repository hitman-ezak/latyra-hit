<?php

class Df_Core_Block_Element_Style extends Df_Core_Block_Element {
	

	
	/**
	 * @return Df_Core_Model_Style_Selector_Collection
	 */
	public function getSelectors () {
	
		if (!isset ($this->_selectors)) {
	
			/** @var Df_Core_Model_Style_Selector_Collection $result  */
			$result = $this->cfg (self::PARAM__SELECTORS);

			if (is_null ($result)) {
				$result = new Df_Core_Model_Style_Selector_Collection ();
			}
	
			df_assert ($result instanceof Df_Core_Model_Style_Selector_Collection);
	
			$this->_selectors = $result;
		}
	
		df_assert ($this->_selectors instanceof Df_Core_Model_Style_Selector_Collection);
	
		return $this->_selectors;
	}
	
	
	/**
	* @var Df_Core_Model_Style_Selector_Collection
	*/
	private $_selectors;	
	
	



	/**
	 * Если данный метод вернёт true, то система не будет рисовать данный блок.
	 *
	 * @override
	 * @return bool
	 */
	protected function isBlockEmpty () {

		/** @var bool $result  */
		$result = (0 === $this->getSelectors()->count());

		df_result_boolean ($result);

		return $result;
	}



	const PARAM__SELECTORS = 'selectors';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Element_Style';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


