<?php

class Df_Core_Block_Element_Image extends Df_Core_Block_Element {


	/**
	 * @return string
	 */
	public function getAlt () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM_ALT, Df_Core_Const::T_EMPTY);

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	public function getTitle () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM_TITLE, Df_Core_Const::T_EMPTY);

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	public function getSrc () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM_SRC, Df_Core_Const::T_EMPTY);

		df_result_string ($result);

		return $result;
	}





	/**
	 * Если данный метод вернёт true, то система не будет рисовать данный блок.
	 *
	 * @return bool
	 */
	protected function isBlockEmpty () {

		/** @var bool $result  */
		$result = df_empty ($this->getSrc ());

		df_result_boolean ($result);

		return $result;
	}







	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Element_Image';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const PARAM_SRC = 'src';
	const PARAM_ALT = 'alt';
	const PARAM_TITLE = 'title';



}


