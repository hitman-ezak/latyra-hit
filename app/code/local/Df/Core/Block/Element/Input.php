<?php


abstract class Df_Core_Block_Element_Input extends Df_Core_Block_Element {





	/**
	 * @return string
	 */
	public function getName () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM_NAME);


		df_assert (
			!df_empty ($result)
		)
		;

		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	public function getValue () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM_VALUE, Df_Core_Const::T_EMPTY);

		df_result_string ($result);

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Element_Input';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const PARAM_NAME = 'name';
	const PARAM_VALUE = 'value';

}


