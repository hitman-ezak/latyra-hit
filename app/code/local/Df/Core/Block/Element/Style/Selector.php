<?php

class Df_Core_Block_Element_Style_Selector extends Df_Core_Block_Element {



	/**
	 * @return Df_Core_Model_Output_Css_Rule_Set
	 */
	public function getRuleSet () {

		if (!isset ($this->_ruleSet)) {

			/** @var Df_Core_Model_Output_Css_Rule_Set $result  */
			$result = $this->cfg (self::PARAM__RULE_SET);

			if (is_null ($result)) {
				$result =
					df_model (
						Df_Core_Model_Output_Css_Rule_Set::getNameInMagentoFormat()
					)
				;
			}


			df_assert ($result instanceof Df_Core_Model_Output_Css_Rule_Set);

			$this->_ruleSet = $result;
		}


		df_assert ($this->_ruleSet instanceof Df_Core_Model_Output_Css_Rule_Set);

		return $this->_ruleSet;

	}


	/**
	* @var Df_Core_Model_Output_Css_Rule_Set
	*/
	private $_ruleSet;





	/**
	 * @return string
	 */
	public function getSelector () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__SELECTOR);

		if (df_empty ($result)) {
			df_log ('Требуется селектор');

			/**
			 * Иначе  df_result_string приведёт к сбою браузера:
			 *
			 * Content Encoding Error
			 * The page you are trying to view cannot be shown
			 * because it uses an invalid or unsupported form of compression.
			 * Please contact the website owners to inform them of this problem.
			 */
			$result = '';
		}


		df_result_string ($result);

		return $result;
	}




	/**
	 * Если данный метод вернёт true, то система не будет рисовать данный блок.
	 *
	 * @override
	 * @return bool
	 */
	protected function isBlockEmpty () {

		/** @var bool $result  */
		$result = (0 === $this->getRuleSet()->count());

		df_result_boolean ($result);

		return $result;
	}



	const PARAM__RULE_SET = 'rule_set';
	const PARAM__SELECTOR = 'selector';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Block_Element_Style_Selector';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


