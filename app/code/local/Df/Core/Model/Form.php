<?php


abstract class Df_Core_Model_Form extends Df_Core_Model_Abstract {

	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getZendFormClass();


	/**
	 * @param $key
	 * @param string $default [optional]
	 * @return string
	 */
	protected function getField ($key, $default = null) {

		/** @var Zend_Form_Element|null $element  */
		$element = $this->getZendForm()->getElement($key);

		df_assert (
			$element instanceof Zend_Form_Element
			,
			sprintf (
				"Не найдено поле: «%s»"
				,
				$key
			)
		)
		;

		$result = $element->getValue ();
		$validator = new Zend_Validate_NotEmpty ();

		if (!$validator->isValid ($result)) {
			$result = $default;
		}

	    return $result;
	}



	/**
	 * @param  string $key
	 * @return array
	 */
	protected function getTextareaParam ($key) {
		df_param_string ($key, 0);

		$result =
			df_text()
				->parseTextarea (
					$this->getField ($key)
				)
			;
		;
		/** @var array $result */
		df_result_array ($result);

	    return $result;
	}



	
	/**
	 * @return Df_Zf_Form
	 */
	private function getZendForm () {
	
		if (!isset ($this->_zendForm)) {

			$class = $this->getZendFormClass();
	
			/** @var Df_Zf_Form $result  */
			$result = new $class;
	
			df_assert ($result instanceof Df_Zf_Form);


			$result->setValues ($this->getZendFormValues());

	
			$this->_zendForm = $result;
		}
	
		df_assert ($this->_zendForm instanceof Df_Zf_Form);
	
		return $this->_zendForm;
	}
	
	
	/**
	* @var Df_Zf_Form
	*/
	private $_zendForm;	




	/**
	 * @return array
	 */
	private function getZendFormValues () {
		return $this->cfg (self::PARAM__ZEND_FORM_VALUES);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
	    $this
			->addValidator (
				self::PARAM__ZEND_FORM_VALUES, new Df_Zf_Validate_Array()
			)
		;
	}


	const PARAM__ZEND_FORM_VALUES = 'zend_form_values';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Form';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
