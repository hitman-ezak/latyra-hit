<?php

class Df_Core_Model_Lib extends Df_Core_Model_Lib_Abstract {



	/**
	 * @return void
	 */
	public function init () {

		/** @var bool $initialized */
		static $initialized = false;

		if (!$initialized) {

			ini_set ('mbstring.internal_encoding', 'UTF-8');


			/**
			 * Обратите внимание, что двойной инициализации не происходит,
			 * потому что Mage::helper ('df_core/lib') - объект-одиночка.
			 */
			Mage::helper ('df_core/lib');

			$initialized = true;
		}



		/** @var bool $timeZoneInitialized */
		static $timeZoneInitialized = false;

		if (!$timeZoneInitialized) {
			try {

				/**
				 * Здесь может случиться исключительная ситуация,
				 * если мы попали в этот метод по событию resource_get_tablename,
				 * а магазин ещё не инициализирован.
				 *
				 * Просто игнорируем её.
				 */


				/** @var string|null $defaultTimezone  */
				$defaultTimezone =
					Mage::getStoreConfig (
						Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE
					)
				;


				/**
				 * По необъяснимой причине
				 * после предыдущего вызова $defaultTimezone может быть пустым значением
				 */
				if (!df_empty ($defaultTimezone)) {

					date_default_timezone_set ($defaultTimezone);

					$timeZoneInitialized = true;

				}


			}
			catch (Exception $e) {

			}
		}


		/** @var bool $shutdownInitialized */
		static $shutdownInitialized = false;

		if (!$shutdownInitialized) {

			register_shutdown_function (
				array (
					Df_Qa_Model_Shutdown::getClass()
					,
					'processStatic'
				)
			);

			$shutdownInitialized = true;
		}

	}




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Lib';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

	
}