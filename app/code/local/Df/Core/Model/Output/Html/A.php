<?php

class Df_Core_Model_Output_Html_A extends Df_Core_Model_Abstract {


	/**
	 * @return string
	 */
	public function __toString () {
		try {
			$result =
					!$this->hasHref()
				?
					$this->getAnchor()
				:
					$this->getRealTag ()
			;
		}
		catch (Exception $e) {
			df_handle_entry_point_exception ($e, false);
			$result = df_escape ("<invalid link>");
		}
		return $result;
	}




	/**
	 * @return string
	 * @throws Exception
	 */
	private function getRealTag () {
		ob_start ();
		try {
?><a
	href="<?php echo $this->getHref(); ?>"
<?php if (!df_empty ($this->getTarget ())) { ?>
	target="<?php echo $this->getTarget();?>"
<?php } ?>
<?php if (!df_empty ($this->getClasses ())) { ?>
	class="<?php echo $this->getClassesAsAttributeValue();?>"
<?php } ?>
<?php if (!df_empty ($this->getTitle ())) { ?>
	title="<?php echo $this->getTitle();?>"
<?php } ?>
<?php if (!df_empty ($this->getRel ())) { ?>
	rel="<?php echo $this->getRel ();?>"
<?php } ?>
><?php echo $this->getAnchor(); ?></a><?php
		}
		catch (Exception $e) {
			ob_get_clean ();
			throw $e;
		}
		return ob_get_clean ();
	}




	/**
	 * @return string
	 */
	private function getClassesAsAttributeValue () {
		return
			$this->filterForOutput (
				implode (
					","
					,
					array_map (
						"df_trim"
						,
						$this->getClasses()
					)
				)
			)
		;
	}



	/**
	 * @return string
	 */
	public function getAnchor () {
		return
			$this->filterForOutput (
				$this->cfg (self::PARAM_ANCHOR)
			)
		;
	}


	/**
	 * @return string
	 */
	public function getHref () {
		return
			$this->filterForOutput (
				$this->cfg (self::PARAM_HREF)
			)
		;
	}



	/**
	 * @return bool
	 */
	public function hasHref () {
		return !df_empty ($this->getHref());
	}



	/**
	 * @return array
	 */
	public function getClasses () {
		return $this->cfg (self::PARAM_CLASSES, array ());
	}



	/**
	 * @return string
	 */
	public function getTarget () {
		return $this->cfg (self::PARAM_TARGET);
	}



	/**
	 * @return string
	 */
	public function getTitle () {
		return
			$this->filterForOutput (
				$this->cfg (self::PARAM_TITLE)
			)
		;
	}


	/**
	 * @return string
	 */
	public function getRel () {
		return
			$this->filterForOutput (
				$this->cfg (self::PARAM_REL)
			)
		;
	}




	/**
	 * @param $text
	 * @return string
	 */
	private function filterForOutput ($text) {
		return
			df_escape (
				df_trim (
					$text
				)
			)
		;
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
	    $this
	        ->addValidator (
				self::PARAM_ANCHOR, new Df_Zf_Validate_String()
			)
	        ->addValidator (
				self::PARAM_CLASSES, new Df_Zf_Validate_Array(), false
			)
			->addValidator (
				self::PARAM_HREF, new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM_TARGET, new Df_Zf_Validate_String(), false
			)
	        ->addValidator (
				self::PARAM_TITLE, new Df_Zf_Validate_String(), false
			)
	        ->addValidator (
				self::PARAM_REL, new Df_Zf_Validate_String(), false
			)
		;
	}



	const PARAM_ANCHOR = 'anchor';
	const PARAM_CLASSES = 'classes';
	const PARAM_HREF = 'href';
	const PARAM_TARGET = 'target';
	const PARAM_TITLE = 'title';
	const PARAM_REL = 'rel';


	const TARGET__BLANK = '_blank';


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Output_Html_A';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}