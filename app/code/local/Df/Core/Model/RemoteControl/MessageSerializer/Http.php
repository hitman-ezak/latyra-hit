<?php

class Df_Core_Model_RemoteControl_MessageSerializer_Http extends Df_Core_Model_Abstract {

	/**
	 * @static
	 * @param Mage_Core_Controller_Request_Http $httpRequest
	 * @return Df_Core_Model_RemoteControl_Message_Request
	 */
	public static function restoreMessageRequest (Mage_Core_Controller_Request_Http $httpRequest) {

		/** @var string $classNameEncoded */
		$classNameEncoded =
			$httpRequest->getHeader (
				self::HEADER__CLASS_NAME
			)
		;

		df_assert_string ($classNameEncoded);



		/** @var string $className */
		$className =
			Df_Core_Model_RemoteControl_Coder::i()->decodeClassName (
				$classNameEncoded
			)
		;

		df_assert_string ($className);


		/** @var string $body */
		$body =
			$httpRequest->getRawBody()
		;

		df_assert_string ($body);



		/** @var array $messageData */
		$messageData =
			Df_Core_Model_RemoteControl_Coder::i()->decode (
				$body
			)
		;

		df_assert_array ($messageData);



		/** @var Df_Core_Model_RemoteControl_Message_Request $result */
		$result =
			df_model (
				$className
				,
				$messageData
			)
		;

		df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Request);

		return $result;
	}



	/**
	 * @static
	 * @param Zend_Http_Response $httpResponse
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	public static function restoreMessageResponse (Zend_Http_Response $httpResponse) {

		/** @var string $classNameEncoded */
		$classNameEncoded =
			$httpResponse->getHeader (
				self::HEADER__CLASS_NAME
			)
		;

		df_assert_string ($classNameEncoded);

		/** @var string $className */
		$className =
			Df_Core_Model_RemoteControl_Coder::i()->decodeClassName (
				$classNameEncoded
			)
		;

		df_assert_string ($className);



		/** @var string $body */
		$body = $httpResponse->getBody();

		df_assert_string ($body);



		/** @var array $messageData */
		$messageData =
			Df_Core_Model_RemoteControl_Coder::i()->decode (
				$body
			)
		;

		df_assert_array ($messageData);



		/** @var Df_Core_Model_RemoteControl_Message_Response $result */
		$result =
			df_model (
				$className
				,
				$messageData
			)
		;

		df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Response);

		return $result;
	}




	/**
	 * @param Zend_Http_Client $httpClient
	 * @param Df_Core_Model_RemoteControl_Message_Request $message
	 * @return void
	 */
	public static function serializeMessageRequest (
		Zend_Http_Client $httpClient
		,
		Df_Core_Model_RemoteControl_Message_Request $message
	) {

		$httpClient
			->setHeaders (
				self::HEADER__CLASS_NAME
				,
				Df_Core_Model_RemoteControl_Coder::i()->encodeClassName (
					$message->getCurrentClassNameInMagentoFormat()
				)
			)
			->setRawData (
				Df_Core_Model_RemoteControl_Coder::i()->encode (
					$message->getPersistentData()
					,
					'application/octet-stream'
				)
			)
		;
	}



	/**
	 * @param Mage_Core_Controller_Response_Http $httpResponse
	 * @param Df_Core_Model_RemoteControl_Message_Response $message
	 * @return void
	 */
	public static function serializeMessageResponse (
		Mage_Core_Controller_Response_Http $httpResponse
		,
		Df_Core_Model_RemoteControl_Message_Response $message
	) {

		$httpResponse
			->setHeader ('Content-Type', 'application/octet-stream')
			->setHeader (
				Df_Core_Model_RemoteControl_MessageSerializer_Http::HEADER__CLASS_NAME
				,
				Df_Core_Model_RemoteControl_Coder::i()->encodeClassName (
					$message->getCurrentClassNameInMagentoFormat()
				)
			)
			->setBody (
				Df_Core_Model_RemoteControl_Coder::i()->encode (
					$message->getPersistentData()
				)
			)
		;
	}



	const HEADER__CLASS_NAME = 'rm-message-class';
	const REQUEST_PARAM__BODY = 'body';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_MessageSerializer_Http';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}

