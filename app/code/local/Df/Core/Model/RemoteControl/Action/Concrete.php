<?php

abstract class Df_Core_Model_RemoteControl_Action_Concrete extends Df_Core_Model_RemoteControl_Action {


	/**
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	abstract protected function createMessageResponse();


	/**
	 * @return Df_Core_Model_RemoteControl_Message_Response
	 */
	public function getMessageResponse () {

		if (!isset ($this->_messageResponse)) {

			/** @var Df_Core_Model_RemoteControl_Message_Response $result */
			$result = null;

			try {
				$result = $this->createMessageResponse();
			}
			catch (Exception $e) {
				$result =
					df_model (
						Df_Core_Model_RemoteControl_Message_Response_GenericFailure::getNameInMagentoFormat()
						,
						array (
							Df_Core_Model_RemoteControl_Message_Response_GenericFailure
								::PARAM__IS_OK => false
							,
							Df_Core_Model_RemoteControl_Message_Response_GenericFailure
								::PARAM__TEXT => $e->getMessage()
						)
					)
				;
			}

			df_assert ($result instanceof Df_Core_Model_RemoteControl_Message_Response);

			$this->_messageResponse = $result;
		}

		df_assert ($this->_messageResponse instanceof Df_Core_Model_RemoteControl_Message_Response);

		return $this->_messageResponse;
	}


	/**
	* @var Df_Core_Model_RemoteControl_Message_Response
	*/
	private $_messageResponse;




	/**
	 * @return Df_Core_Model_RemoteControl_Message_Request
	 */
	protected function getMessageRequest () {
		return $this->cfg (self::PARAM__MESSAGE_REQUEST);
	}


	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__MESSAGE_REQUEST, Df_Core_Model_RemoteControl_Message_Request::getClass()
			)
		;
	}


	const PARAM__MESSAGE_REQUEST = 'message_request';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_RemoteControl_Action_Concrete';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


