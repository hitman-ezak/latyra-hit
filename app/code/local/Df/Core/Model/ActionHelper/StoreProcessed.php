<?php

class Df_Core_Model_ActionHelper_StoreProcessed extends Df_Core_Model_Abstract {


	/**
	 * @return Mage_Core_Model_Store
	 */
	public function getStoreProcessed () {

		if (!isset ($this->_storeProcessed)) {

			/** @var Mage_Core_Model_Store $result  */
			$result = null;

			if (Mage::app()->isSingleStoreMode()) {
				$result = Mage::app()->getStore(true);
			}

			else {


				/**
				 * Если в системе присутствует больше одного магазина,
				 * то администратор должен указать обрабатываемый магазин
				 * параметром в запрашиваемом адресе одним из двух способов:
				 *
				 * 1) http://localhost.com:686/df-1c/cml2/index/?store-view=store_686
				 * 2) http://localhost.com:686/df-1c/cml2/index/store-view/store_686/
				 */


				/** @var string $storeCode */
				$storeCode = df_request ('store-view');

				if (is_null ($storeCode)) {

					/** @var string $pattern */
					$pattern = '#\/store\-view\/([^\/]+)\/#u';

					/** @var array $matches */
					$matches = array ();


					/** @var int|bool $matchingResult  */
					$matchingResult =
						preg_match (
							$pattern
							,
							Mage::app()->getRequest()->getRequestUri()
							,
							$matches
						)
					;

					if (1 === $matchingResult) {

						/** @var string $storeCode */
						$storeCode = df_a ($matches, 1);

					}
				}


				if (df_empty ($storeCode)) {
					df_error (
						sprintf (
							'Ваша система содержит несколько витрин,
							поэтому Вы должны указать системное имя обрабатываемой витрины
							в запрашиваемом из «%s» адресе:
							%s'
							,
							$this->getModuleName()
							,
							$this->getUrlExample()
						)
					);
				}


				df_assert_string ($storeCode);


				try {
					$result = Mage::app()->getStore($storeCode);
				}
				catch (Mage_Core_Model_Store_Exception $e) {
					df_error (
						sprintf (
							'Витрина с системным именем «%s» отсутствует в Вашей системе.'
							,
							$storeCode
						)
					);
				}


			}


			df_assert ($result instanceof Mage_Core_Model_Store);

			$this->_storeProcessed = $result;
		}


		df_assert ($this->_storeProcessed instanceof Mage_Core_Model_Store);

		return $this->_storeProcessed;

	}


	/**
	* @var Mage_Core_Model_Store
	*/
	private $_storeProcessed;



	/**
	 * @return string
	 */
	private function getModuleName () {
		return $this->cfg (self::PARAM__MODULE_NAME);
	}



	/**
	 * @return string
	 */
	private function getUrlExample () {
		return $this->cfg (self::PARAM__URL_EXAMPLE);
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__MODULE_NAME, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__URL_EXAMPLE, new Df_Zf_Validate_String()
			)
		;
	}



	const PARAM__MODULE_NAME = 'module_name';
	const PARAM__URL_EXAMPLE = 'url_example';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_ActionHelper_StoreProcessed';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


