<?php

class Df_Core_Model_Money extends Df_Core_Model_Abstract {



	/**
	 * @override
	 * @return string
	 */
	public function __toString() {

		/** @var string $result  */
		$result = $this->getAsString ();

		return $result;
	}


	
	
	/**
	 * @return float
	 */
	public function getAsFixedFloat () {
	
		/** @var float $result  */
		$result = round ($this->getOriginal(), 2);	
	
		df_result_float ($result);
	
		return $result;
	}
	
	
	
	
	/**
	 * @return int
	 */
	public function getAsInteger () {
	
		if (!isset ($this->_asInteger)) {
	
			/** @var int $result  */
			$result = 
				intval (
					round (
						$this->getAsFixedFloat()
					)					
				)							
			;
	
	
			df_assert_integer ($result);
	
			$this->_asInteger = $result;
		}
	
	
		df_result_integer ($this->_asInteger);
	
		return $this->_asInteger;
	}
	
	
	/**
	* @var int
	*/
	private $_asInteger;		
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getAsString () {
	
		if (!isset ($this->_asString)) {
	
			/** @var string $result  */
			$result = 
				number_format (
					$this->getAsFixedFloat ()
					,
					2
					,
					Df_Core_Const::T_PERIOD
					,
					Df_Core_Const::T_EMPTY
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_asString = $result;
		}
	
	
		df_result_string ($this->_asString);
	
		return $this->_asString;
	}
	
	
	/**
	* @var string
	*/
	private $_asString;	
	
	
	
	
	
	/**
	 * @return int
	 */
	public function getIntegerPart () {
	
		if (!isset ($this->_integerPart)) {
	
			/** @var int $result  */
			$result = 
				intval (
					floor (
						$this->getAsFixedFloat()
					)					
				)							
			;
	
	
			df_assert_integer ($result);
	
			$this->_integerPart = $result;
		}
	
	
		df_result_integer ($this->_integerPart);
	
		return $this->_integerPart;
	}
	
	
	/**
	* @var int
	*/
	private $_integerPart;	
	
	
	
	
	
	/**
	 * @return int
	 */
	public function getFractionalPart () {
	
		if (!isset ($this->_fractionalPart)) {
	
			/** @var int $result  */
			$result =
				intval (
						100
					*
						(
								$this->getAsFixedFloat()
							-
								$this->getIntegerPart()
						)
				)
			;
	
	
			df_assert_integer ($result);
	
			$this->_fractionalPart = $result;
		}
	
	
		df_result_integer ($this->_fractionalPart);
	
		return $this->_fractionalPart;
	}
	
	
	/**
	* @var int
	*/
	private $_fractionalPart;		
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getFractionalPartAsString () {
	
		if (!isset ($this->_fractionalPartAsString)) {
	
			/** @var string $result  */
			$result = 
				sprintf (
					'%02d'
					,
					$this->getFractionalPart ()
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_fractionalPartAsString = $result;
		}
	
	
		df_result_string ($this->_fractionalPartAsString);
	
		return $this->_fractionalPartAsString;
	}
	
	
	/**
	* @var string
	*/
	private $_fractionalPartAsString;	
	
	
	
	
	
		
	/**
	 * @return float
	 */
	public function getOriginal () {
	
		/** @var float $result  */
		$result = $this->cfg (self::PARAM__AMOUNT);	
	
		df_result_float ($result);
	
		return $result;
	}
	
	



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__AMOUNT, new Zend_Validate_Float()
			)
		;
	}


	const PARAM__AMOUNT = 'amount';





	/**
	 * @return Df_Core_Model_Money
	 */
	public static function getZero () {

		/** @var Df_Core_Model_Money $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Model_Money $result  */
			$result =
				df_model (
					self::getNameInMagentoFormat()
					,
					array (self::PARAM__AMOUNT => 0)
				)
			;

			df_assert ($result instanceof Df_Core_Model_Money);
		}

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Money';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


