<?php

class Df_Core_Model_Units_Weight extends Df_Core_Model_Abstract {
	
	
	/**
	 * @return array
	 */
	public function getAll () {
	
		if (!isset ($this->_all)) {
	
			/** @var array $result  */
			$result =
				Mage::app()->getConfig()->getNode ('df/units/weight')
					->asCanonicalArray()
			;
	
	
			df_assert_array ($result);
	
			$this->_all = $result;
		}
	
	
		df_result_array ($this->_all);
	
		return $this->_all;
	}
	
	
	/**
	* @var array
	*/
	private $_all;





	/**
	 * @param float $productWeightInDefaultUnits
	 * @return float
	 */
	public function convertToGrammes ($productWeightInDefaultUnits) {

		df_param_float ($productWeightInDefaultUnits, 0);


		/** @var float $result  */
		$result =
			$this->getProductWeightUnitsRatio() * $productWeightInDefaultUnits
		;


		df_result_float ($result);

		return $result;
	}




	/**
	 * @param float $productWeightInDefaultUnits
	 * @return float
	 */
	public function convertToKilogrammes ($productWeightInDefaultUnits) {

		df_param_float ($productWeightInDefaultUnits, 0);


		/** @var float $result  */
		$result =
				$this->convertToGrammes ($productWeightInDefaultUnits)
			/
				1000
		;


		df_result_float ($result);

		return $result;
	}





	/**
	 * @return float
	 */
	private function getProductWeightUnitsRatio () {

		if (!isset ($this->_productWeightUnitsRatio)) {


			/** @var array $productDefaultUnits */
			$productDefaultUnits =
				df_a (
					df()->units()->weight()->getAll()
					,
					df_cfg()->shipping()->product()->getUnitsWeight()
				)
			;

			/** @var float $result  */
			$result =
				floatval (
					df_a ($productDefaultUnits, self::UNIT__RATIO)
				)
			;


			df_assert_float ($result);

			$this->_productWeightUnitsRatio = $result;
		}


		df_result_float ($this->_productWeightUnitsRatio);

		return $this->_productWeightUnitsRatio;

	}


	/**
	* @var float
	*/
	private $_productWeightUnitsRatio;



	



	const UNIT__LABEL = 'label';
	const UNIT__RATIO = 'ratio';

	

	
	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Units_Weight';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	
	
}


