<?php

class Df_Core_Model_Style_Selector_Collection extends Df_Varien_Data_Collection {



	/**
	 * @param string $selectorAsString
	 * @return Df_Core_Model_Style_Selector_Collection
	 */
	public function addHider ($selectorAsString) {

		df_param_string ($selectorAsString, 0);

		$this
			->addItem (
				df_block (
					Df_Core_Block_Element_Style_Selector::getNameInMagentoFormat()
					,
					null
					,
					array (
						Df_Core_Block_Element_Style_Selector::PARAM__SELECTOR => $selectorAsString
						,
						Df_Core_Block_Element_Style_Selector::PARAM__RULE_SET =>
							Df_Varien_Data_Collection::createFromCollection (
								array (
									df_model (
										Df_Core_Model_Output_Css_Rule::getNameInMagentoFormat()
										,
										array (
											Df_Core_Model_Output_Css_Rule::PARAM__NAME => 'display'
											,
											Df_Core_Model_Output_Css_Rule::PARAM__VALUE => 'none'
										)
									)
								)
								,
								Df_Core_Model_Output_Css_Rule_Set::getClass()
							)
					)
				)
			)
		;

		return $this;
	}




	/**
	 * @return string
	 */
	protected function getItemClass () {

		/** @var string $result  */
		$result = Df_Core_Block_Element_Style_Selector::getClass();

		df_result_string ($result);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Style_Selector_Collection';
	}




	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


