<?php



class Df_Core_Model_Cache_File extends Df_Core_Model_Abstract {



	/**
	 * @return string|null
	 */
	public function getCacheData () {

		if (!isset ($this->_cacheData)) {

			/** @var string|bool $result  */
			$result = null;

			if (file_exists ($this->getFullName())) {

				$result = @file_get_contents($this->getFullName());

			}



			if (false === $result) {
				$result = null;
			}

			if (!is_null ($result)) {
				df_assert_string ($result);
			}


			$this->_cacheData = $result;
		}


		if (!is_null ($this->_cacheData)) {
			df_result_string ($this->_cacheData);
		}

		return $this->_cacheData;

	}



	/**
	 * @return Df_Core_Model_Cache_File
	 */
	public function putCacheData ($data) {

		df_param_string ($data, 0);

		try {

			$this->getIo()->checkAndCreateFolder ($this->getBasePath ());

			file_put_contents ($this->getFullName(), $data);

			//$this->getIo()->write ($this->getFullName(), $data);

		}
		catch (Exception $e) {
			df_error (
				sprintf (
					'Не могу записать файл «%s»'
					,
					$this->getFullName()
				)
			);
		}



		$this->_cacheData = $data;

		return $this;
	}



	/**
	* @var string
	*/
	private $_cacheData;






	/**
	 * @return Varien_Io_File
	 */
	private function getIo () {

		if (!isset ($this->_io)) {

			/** @var Varien_Io_File $result  */
			$result =
				new Varien_Io_File()
			;

			//$result->checkAndCreateFolder ($this->getBasePath ());


			df_assert ($result instanceof Varien_Io_File);

			$this->_io = $result;
		}


		df_assert ($this->_io instanceof Varien_Io_File);

		return $this->_io;

	}


	/**
	* @var Varien_Io_File
	*/
	private $_io;




	
	
	
	
	/**
	 * @return string
	 */
	private function getFullName () {
	
		if (!isset ($this->_fullName)) {
	
			/** @var string $result  */
			$result = 
		
				implode (
					DS
					,
					array (
						$this->getBasePath()
						,
						$this->getName()
					)
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_fullName = $result;
		}
	
	
		df_result_string ($this->_fullName);
	
		return $this->_fullName;
	}
	
	
	/**
	* @var string
	*/
	private $_fullName;	
	
	
	
	
	
	/**
	 * @return string
	 */
	private function getBasePath () {
	
		if (!isset ($this->_basePath)) {
	
			/** @var string $result  */
			$result = 

				implode (
					DS
					,
					array_merge (
						array (
							BP, 'var', 'cache',	'df'
						)
						,
						$this->getRelativePathParts()
					)

				)

			;
	
	
			df_assert_string ($result);
	
			$this->_basePath = $result;
		}
	
	
		df_result_string ($this->_basePath);
	
		return $this->_basePath;
	}
	
	
	/**
	* @var string
	*/
	private $_basePath;
	
	
	
	
	
	
	/**
	 * @return string
	 */
	private function getName () {
	
		if (!isset ($this->_name)) {
	
			/** @var string $result  */
			$result = 
		
				implode (
					Df_Core_Const::T_FILE_EXTENSION_SEPARATOR
					,
					array (
						$this->getBaseName()
						,
						$this->getFileExtension ()
					)
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_name = $result;
		}
	
	
		df_result_string ($this->_name);
	
		return $this->_name;
	}
	
	
	/**
	* @var string
	*/
	private $_name;	




	/**
	 * @return array
	 */
	private function getRelativePathParts () {

		/** @var array $result  */
		$result =
			$this->cfg (self::PARAM_RELATIVE_PATH_PARTS, array ())
		;


		df_result_array ($result);

		return $result;
	}


	
	
	
	
	/**
	 * @return string
	 */
	private function getFileExtension () {
	
		/** @var string $result  */
		$result =
			$this->cfg (self::PARAM_FILE_EXTENSION, self::DEFAULT_FILE_EXTENSION)
		;
	
	
		df_result_string ($result);
	
		return $result;
	}
	
	
	
	
	
	
	/**
	 * @return string
	 */
	private function getBaseName () {
	
		/** @var string $result  */
		$result =	
			$this->cfg (self::PARAM_BASE_NAME)
		;
	
	
		df_result_string ($result);
	
		return $result;
	}
	

	
	
	
	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Cache_File';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}	
	
	
	
	
	
	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM_BASE_NAME,	new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM_FILE_EXTENSION,	new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM_RELATIVE_PATH_PARTS, new Df_Zf_Validate_Array(), false
			)
		;
	}



	const PARAM_BASE_NAME = 'baseName';
	const PARAM_FILE_EXTENSION = 'fileExtension';
	const PARAM_RELATIVE_PATH_PARTS = 'relativePathParts';


	const DEFAULT_FILE_EXTENSION = 'txt';
	
	
}


