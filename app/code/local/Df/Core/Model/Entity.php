<?php

abstract class Df_Core_Model_Entity extends Df_Core_Model_Abstract {


	/**
	 * @return string
	 */
	abstract public function getName();

	
	/**
	 * @return Df_Core_Model_Entity_Dependency_Collection
	 */
	public function getDependenciesInfo () {
		if (!isset ($this->_dependenciesInfo)) {
	
			/** @var Df_Core_Model_Entity_Dependency_Collection $result */
			$result =
				df_model (
					Df_Core_Model_Entity_Dependency_Collection::getNameInMagentoFormat()
				)
			;
	
			df_assert ($result instanceof Df_Core_Model_Entity_Dependency_Collection);
			$this->_dependenciesInfo = $result;

			$this->initDependenciesInfo();
		}
		return $this->_dependenciesInfo;
	}
	/** @var Df_Core_Model_Entity_Dependency_Collection */
	private $_dependenciesInfo;	
	
	
	
	/**
	 * @param string $name
	 * @return Df_Core_Model_Entity
	 */
	public function getDependencyByName ($name) {
		df_param_string ($name, 0);
		if (!isset ($this->_dependencyByName [$name])) {

			/** @var Df_Core_Model_Entity_Dependency $dependencyInfo */
			$dependencyInfo =
				$this->getDependenciesInfo()->getItemById (
					$name
				)
			;
			df_assert ($dependencyInfo instanceof Df_Core_Model_Entity_Dependency);


			/** @var Df_Core_Model_Entity $result */
			$result =
				df_model (
					$dependencyInfo->getEntityClassNameMf()
				)
			;
			df_assert ($result instanceof Df_Core_Model_Entity);


			/** @var int $dependencyId */
			$dependencyId =
				intval (
					$this->cfg (
						$dependencyInfo->getEntityIdFieldName()
					)
				)
			;

			if (0 < $dependencyId) {
				$result->load ($dependencyId);
			}

			$this->_dependencyByName [$name] = $result;
		}
		return $this->_dependencyByName [$name];
	}
	/** @var Df_Core_Model_Entity[] */
	private $_dependencyByName = array ();




	/**
	 * @override
	 * @return int|null
	 */
	public function getId () {
		return
				is_null (parent::getId())
			?
				/**
				 * Для несохранявшихся ранне в бузу данных объектов
				 * надо возвращать именно null, а не 0,
				 * потому что если вернуть 0, то не сработает проверка
				 * (!is_null($object->getId()) && (!$this->_useIsObjectNew || !$object->isObjectNew()))
				 * в методе Mage_Core_Model_Resource_Db_Abstract::save()
				 */
				parent::getId()
			:
				intval (parent::getId())
		;
	}


	/**
	 * @override
	 * @return string
	 */
	public function getIdFieldName () {
		/** @var string $result  */
		$result = parent::getIdFieldName();

		/**
		 * Вместо «id» используйте уникальное в рамках предметной области
		 * имя поля для идентификатора, например: «location_id», «warehouse_id».
		 *
		 * На уникальности имен иденнтификатора в рамках предметной области
		 * основывается, например, алгоритм метода Df_Core_Model_Form_Builder::getEntity()
		 */
		df_assert ('id' !== $result);

		return $result;
	}


	/**
	 * @return string
	 */
	public function getSessionKey () {
		return get_class ($this);
	}


	/**
	 * @return Df_Core_Model_Entity
	 */
	protected function initDependenciesInfo () {
		return $this;
	}




	const DEPENDENCY_INFO__CLASS_NAME_MF = 'class_name_mf';
	const DEPENDENCY_INFO__DELETE_CASCADE = 'delete_cascade';
	const DEPENDENCY_INFO__ID_FIELD_NAME = 'id_field_name';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Entity';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}


