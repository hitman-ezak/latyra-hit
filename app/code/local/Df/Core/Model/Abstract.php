<?php

abstract class Df_Core_Model_Abstract extends Mage_Core_Model_Abstract {


	/**
	 * @return void
	 */
	public function __destruct () {
		$this->cacheSave();
	}



	/**
	 * @param string $key
	 * @param mixed $default [optional]
	 * @return mixed
	 */
	public function cfg ($key, $default = null) {

		df_param_string ($key, 0);

		$result = $this->getData ($key);

		/**
		 * Раньше вместо is_null стояло df_empty.
		 * is_null выглядит логичней.
		 */
	    if (is_null ($result)) {
			$result = $default;
		}

	    return $result;
	}




	/**
	 * @return string
	 */
	public function getCurrentClassNameInMagentoFormat () {

		if (!isset ($this->_currentClassNameInMagentoFormat)) {

			/** @var string $result  */
			$result =
				df_helper ()->core ()->reflection()->getModelNameInMagentoFormat (
					get_class ($this)
				)
			;

			df_result_string ($result);

			$this->_currentClassNameInMagentoFormat = $result;
		}

		df_assert_string ($this->_currentClassNameInMagentoFormat);

		return $this->_currentClassNameInMagentoFormat;
	}


	/**
	 * @var string
	 */
	private $_currentClassNameInMagentoFormat;





	/**
	 * @throws Mage_Core_Exception
	 * @param string $key
	 * @param null|string|int $index
	 * @return mixed
	 */
	public function getData ($key = Df_Core_Const::T_EMPTY, $index = null) {

        if (Df_Core_Const::T_EMPTY === $key) {
            $result = parent::getData ();
        }
		else {
			$result = parent::_getData ($key);

			if (!isset ($this->_validated [$key])) {

				/**
				 * Для каждого свойства проводим валидацию только один раз
				 */
				$this->_validated [$key] = true;

				if (isset ($this->_validators [$key])) {

					/** @var Zend_Validate $validator  */
					$validator = $this->_validators [$key];

					if (!$validator->isValid($result)) {

						/** @var array $messages */
						$compositeMessage =
							sprintf (
									"«%s»: значение %s недопустимо для свойства «%s»."
								.
									"\r\nСообщение проверяющего:\r\n%s"
								,
								get_class ($this)
								,
								df_helper()->qa()->convertValueToDebugString ($result)
								,
								$key
								,
								implode (
									Df_Core_Const::T_NEW_LINE
									,
									$validator->getMessages()
								)
							)
						;

						df_assert_string ($compositeMessage);

						$exception = new Mage_Core_Exception ($compositeMessage);

						/** @var Mage_Core_Model_Message $coreMessage */
						$coreMessage = df_model ('core/message');

						$exception
							->addMessage(
								$coreMessage
									->error (
										$compositeMessage
										,
										get_class ($this)
										,
										'getData'
									)
							)
						;

						throw $exception;
					}
				}
			}
		}

		return $result;
	}


	/**
	 * @var array
	 */
	private $_validated = array ();





	/**
	 * @return mixed
	 */
	public function getId () {

		/** @var mixed $result  */
		$result =
				(
						empty ($this->_idFieldName)
					&&
						empty($this->_resourceName)
					&&
						is_null ($this->_getData('id'))
				)
			?
				/**
				 *
				 *
				 *
				 * Объект родительского класса такую ситуации переводит в исключительную.
				 * Мы же, для использования модели в коллекциях, создаём идентификатор.
				 *
				 * Конечно, таким образом мы лишаемся возможности проверки объекта на новизну,
				 * однако, раз ресурсная модель не установлена,
				 * то такая проверка нам вроде бы и не нужна.
				 */

				$this->getAutoGeneratedId ()
			:
				parent::getId ()
		;

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct();
		$this->cacheLoad();
	}




	/**
	 * @param  $key
	 * @param Zend_Validate_Interface $validator
	 * @param bool|null $isRequired [optional]
	 * @return Df_Core_Model_Abstract
	 */
	protected function addValidator ($key, Zend_Validate_Interface $validator, $isRequired = null) {

		if (
				!is_null ($isRequired)
			&&
				($validator instanceof Df_Zf_Validate)
		) {

			/** @var Df_Zf_Validate $validator */

			$validator->setRequired ($isRequired);
		}


		/**
		 * Замена выражения $this->getValidator ($key)->addValidator ($validator);
		 * на указанный ниже код ускорила загрузку главной страницы
		 * с 1.089 сек до 1.078 сек.
		 */

		/** @var bool $isZendValidate  */
		$isZendValidate = $validator instanceof Zend_Validate;

		/** @var Zend_Validate $wrapper  */
		$wrapper = null;

		if (!isset ($this->_validators [$key])) {

			if ($isZendValidate) {
				$wrapper = $validator;
			}
			else {
				$wrapper = new Zend_Validate ();
				$wrapper->addValidator ($validator);
			}

			$this->_validators [$key] = $wrapper;
		}
		else {
			$wrapper = $this->_validators [$key];
			$wrapper->addValidator ($validator);
		}

	    return $this;
	}



	/**
	 * @var array
	 */
	private $_validators = array ();




	/**
	 * @return string
	 */
	protected function getCacheKey () {
		return get_class ($this);
	}




	/**
	 * @return string[]
	 */
	protected function getPropertiesToCache () {
		return array ();
	}




	/**
	 * @param string $key
	 * @param string $className
	 * @param bool $isRequired [optional]
	 * @return Df_Core_Model_Abstract
	 */
	protected function validateClass ($key, $className, $isRequired = true) {
	    return
			$this->addValidator (
				$key
				,
				new Df_Zf_Validate_Class (
					$className
					,
					array (
						Df_Zf_Validate_Class::PARAM_CAN_BE_NULL => !$isRequired
					)
				)
			)
		;
	}





	/**
	 * @return string
	 */
	private function getAutoGeneratedId () {
		if (!isset ($this->_autoGeneratedId)) {
			$this->_autoGeneratedId = uniqid ();
		}
		return $this->_autoGeneratedId;
	}


	/**
	* @var string
	*/
	private $_autoGeneratedId;




	/**
	 * @return Df_Core_Model_Abstract
	 */
	private function cacheLoad () {

		if ($this->isCacheEnabled()) {

			/** @var string|bool $cacheSerialized  */
			$cacheSerialized =
				Mage::app()->getCacheInstance()->load (
					$this->getCacheKey()
				)
			;


			/** @var $cache $result  */
			$cache = null;


			if (false !== $cacheSerialized) {
				$cache = @unserialize ($cacheSerialized);
			}

			if (is_array ($cache)) {
				foreach ($this->getPropertiesToCache() as $propertyName) {
					/** @var string $propertyName */
					$this->$propertyName =
						df_a ($cache, $propertyName, array())
					;
				}
			}
		}

		return $this;
	}




	/**
	 * @return Df_Core_Model_Abstract
	 */
	private function cacheSave () {

		if ($this->isCacheEnabled()) {

			/** @var array $cache  */
			$cache = array ();

			foreach ($this->getPropertiesToCache() as $propertyName) {
				/** @var string $propertyName */
				$cache [$propertyName] = $this->$propertyName;
			}

			df_assert_array ($cache);

			/** @var array $cacheSerialized */
			$cacheSerialized = serialize ($cache);

			df_assert_string ($cacheSerialized);


			Mage::app()->getCacheInstance()
				->save (
					$cacheSerialized
					,
					$this->getCacheKey()
					,
					array()
					,
					86400
				)
			;
		}


		return $this;
	}





	/**
	 * @return bool
	 */
	private function isCacheEnabled () {
		return 0 < count ($this->getPropertiesToCache());
	}







    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'df_core_abstract';



    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'object';



	const ID_SUFFIX = '_id';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Abstract';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}