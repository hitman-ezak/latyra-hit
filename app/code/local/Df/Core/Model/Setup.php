<?php

abstract class Df_Core_Model_Setup extends Df_Core_Model_Abstract {

	/**
	 * @abstract
	 * @return Df_Core_Model_Setup
	 */
	abstract public function process ();



	/**
	 * @return Df_Core_Model_Setup
	 */
	private function getSetup () {
		return $this->cfg (self::PARAM__SETUP);
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__SETUP, 'Mage_Core_Model_Resource_Setup'
			)
		;
	}



	const PARAM__SETUP = 'setup';





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Setup';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
