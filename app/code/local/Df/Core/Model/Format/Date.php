<?php

class Df_Core_Model_Format_Date extends Df_Core_Model_Abstract {
	
	
	
	/**
	 * @return string
	 */
	public function getInRussianFormat () {
	
		if (!isset ($this->_inRussianFormat)) {
	
			/** @var string $result  */
			$result = 
				$this->getDate()->toString(self::FORMAT__RUSSIAN)
			;
	
	
			df_assert_string ($result);
	
			$this->_inRussianFormat = $result;
		}
	
	
		df_result_string ($this->_inRussianFormat);
	
		return $this->_inRussianFormat;
	}
	
	
	/**
	* @var string
	*/
	private $_inRussianFormat;	
	
	


	
	
	/**
	 * @return string
	 */
	public function getYear () {

		if (!isset ($this->_year)) {

			/** @var string $result  */
			$result =
				$this->getDate()->toString (Zend_Date::YEAR)
			;


			df_assert_string ($result);

			$this->_year = $result;
		}


		df_result_string ($this->_year);

		return $this->_year;

	}


	/**
	* @var string
	*/
	private $_year;	
	
	
	
	
	
	
	
	/**
	 * @return string
	 */
	public function getDayWith2Digits () {

		if (!isset ($this->_dayWith2Digits)) {

			/** @var string $result  */
			$result =
				$this->getDate()->toString (Zend_Date::DAY)
			;


			df_assert_string ($result);

			$this->_dayWith2Digits = $result;
		}


		df_result_string ($this->_dayWith2Digits);

		return $this->_dayWith2Digits;

	}


	/**
	* @var string
	*/
	private $_dayWith2Digits;	
	
	



	/**
	 * @return string
	 */
	public function getMonthInGenetiveCase () {

		if (!isset ($this->_monthInGenetiveCase)) {

			/** @var string $result  */
			$result =
				$this->getDate()->toString (
					Zend_Date::MONTH_NAME
					,
					null
					,
					Df_Core_Const::LOCALE__RUSSIAN
				)
			;


			df_assert_string ($result);

			$this->_monthInGenetiveCase = $result;
		}


		df_result_string ($this->_monthInGenetiveCase);

		return $this->_monthInGenetiveCase;

	}


	/**
	* @var string
	*/
	private $_monthInGenetiveCase;




	/**
	 * @return Zend_Date
	 */
	public function getDate () {

		/** @var Zend_Date $result  */
		$result =
			$this->cfg (self::PARAM__DATE)
		;


		df_assert ($result instanceof Zend_Date);

		return $result;
	}




	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM__DATE, 'Zend_Date'
			)
		;
	}



	const PARAM__DATE = 'date';

	const FORMAT__RUSSIAN = 'dd.MM.yyyy';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Format_Date';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


