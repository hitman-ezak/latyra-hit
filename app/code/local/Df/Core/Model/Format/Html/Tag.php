<?php

class Df_Core_Model_Format_Html_Tag extends Df_Core_Model_Abstract {


	/**
	 * @param string $content
	 * @param string $tag
	 * @param array $attributes [optional]
	 * @return string
	 */
	public static function output ($content, $tag, array $attributes = array ()) {

		df_param_string ($content, 0);
		df_param_string ($tag, 1);
		df_param_array ($attributes, 2);

		/** @var Df_Core_Model_Format_Html_Tag $formatter  */
		$formatter =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM__ATTRIBUTES => $attributes
					,
					self::PARAM__CONTENT => $content
					,
					self::PARAM__TAG => $tag
				)
			)
		;

		df_assert ($formatter instanceof Df_Core_Model_Format_Html_Tag);

		/** @var string $result  */
		$result = $formatter->getOutput();

		df_result_string ($result);

		return $result;
	}

	
	
	/**
	 * @return string
	 */
	public function getOutput () {
	
		if (!isset ($this->_output)) {
	
			/** @var string $result  */
			$result = 
				str_replace (
					array (
						'%tag%'
						,
						'%tag-and-attributes%'
						,
						'%after-attributes%'
						,
						'%content%'
					)
					,
					array (
						$this->getTag()
						,
						$this->getOpenTagWithAttributesAsText ()
						,
								$this->shouldAttributesBeMultiline()
							?
								"\r\n"
							:
								Df_Core_Const::T_EMPTY
						,
						$this->getContent()
					)
					,
							df_empty ($this->getContent())
						&&
							$this->isShortTagAllowed()
					?
						'<%tag-and-attributes%/>'
					:
						'<%tag-and-attributes%%after-attributes%>%content%</%tag%>'
				)
			;

	
			df_assert_string ($result);
	
			$this->_output = $result;
		}
	
	
		df_result_string ($this->_output);
	
		return $this->_output;
	}
	
	
	/**
	* @var string
	*/
	private $_output;	




	
	/**
	 * @return array
	 */
	public function getAttributes () {
	
		/** @var array $result  */
		$result = $this->cfg (self::PARAM__ATTRIBUTES, array ());	
	
		df_result_array ($result);
	
		return $result;
	}



	/**
	 * @param string $name
	 * @param string $value
	 * @return string
	 */
	public function getAttributeAsText ($name, $value) {

		df_param_string ($name, 0);
		df_param_string ($value, 1);

		/** @var string $result  */
		$result =
			implode (
				'='
				,
				array (
					$name
					,
					sprintf (
						'\'%s\''
						,
						str_replace (
							'\''
							,
							'\\\''
							,
							$value
						)
					)
				)
			)
		;

		df_result_string ($result);

		return $result;
	}

	
	
	
	
	/**
	 * @return string
	 */
	private function getAttributesAsText () {
	
		if (!isset ($this->_attributesAsText)) {
	
			/** @var string $result  */
			$result = 
				implode (
					$this->shouldAttributesBeMultiline() ? "\n" :  Df_Core_Const::T_SPACE
					,
					array_map (
						array ($this, 'getAttributeAsText')
						,
						array_keys ($this->getAttributes())
						,
						array_values ($this->getAttributes())
					)
				)
			;
	
	
			df_assert_string ($result);
	
			$this->_attributesAsText = $result;
		}
	
	
		df_result_string ($this->_attributesAsText);
	
		return $this->_attributesAsText;
	}
	
	
	/**
	* @var string
	*/
	private $_attributesAsText;	
	
	
	


	/**
	 * @return string
	 */
	public function getContent () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__CONTENT, Df_Core_Const::T_EMPTY);


		$result = df_trim ($result, "\n");


		/** @var bool $isMultiline  */
		$isMultiline =
			false !== strpos ($result, "\n")
		;

		if ($isMultiline) {
			$result =
				sprintf (
					"\n%s\n"
					,
					df_tab ($result)
				)
			;
		}


		df_result_string ($result);

		return $result;
	}





	/**
	 * @return string
	 */
	public function getOpenTagWithAttributesAsText () {

		if (!isset ($this->_openTagWithAttributesAsText)) {

			/** @var string $result  */
			$result =
				implode (
					Df_Core_Const::T_SPACE
					,
					df_clean (
						array (
							$this->getTag()
							,
							$this->shouldAttributesBeMultiline() ? "\n" : null
							,
							call_user_func (
								$this->shouldAttributesBeMultiline() ? "df_tab" : 'df_nop'
								,
								$this->getAttributesAsText()
							)
						)
					)
				)
			;


			df_assert_string ($result);

			$this->_openTagWithAttributesAsText = $result;
		}


		df_result_string ($this->_openTagWithAttributesAsText);

		return $this->_openTagWithAttributesAsText;

	}


	/**
	* @var string
	*/
	private $_openTagWithAttributesAsText;








	/**
	 * @return string
	 */
	public function getTag () {

		/** @var string $result  */
		$result = $this->cfg (self::PARAM__TAG);

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return bool
	 */
	public function isShortTagAllowed () {

		if (!isset ($this->_shortTagAllowed)) {

			/** @var bool $result  */
			$result =
				!in_array (
					strtolower ($this->getTag())
					,
					array (
						'div'
						,
						'script'
					)
				)
			;


			df_assert_boolean ($result);

			$this->_shortTagAllowed = $result;
		}


		df_result_boolean ($this->_shortTagAllowed);

		return $this->_shortTagAllowed;

	}


	/**
	* @var bool
	*/
	private $_shortTagAllowed;




	/**
	 * @return bool
	 */
	public function shouldAttributesBeMultiline () {

		if (!isset ($this->_attributesBeMultiline)) {

			/** @var bool $result  */
			$result =
				1 < count ($this->getAttributes())
			;

			df_assert_boolean ($result);

			$this->_attributesBeMultiline = $result;
		}


		df_result_boolean ($this->_attributesBeMultiline);

		return $this->_attributesBeMultiline;

	}


	/**
	* @var bool
	*/
	private $_attributesBeMultiline;



	


	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->addValidator (
				self::PARAM__TAG, new Df_Zf_Validate_String()
			)
			->addValidator (
				self::PARAM__CONTENT, new Df_Zf_Validate_String(), false
			)
			->addValidator (
				self::PARAM__ATTRIBUTES, new Df_Zf_Validate_Array(), false
			)
		;
	}



	const PARAM__TAG = 'tag';
	const PARAM__ATTRIBUTES = 'attributes';
	const PARAM__CONTENT = 'content';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Format_Html_Tag';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


