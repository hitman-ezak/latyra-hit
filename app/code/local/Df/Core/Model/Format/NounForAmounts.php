<?php

class Df_Core_Model_Format_NounForAmounts extends Df_Core_Model_Abstract {



	/**
	 * @param int $amount
	 * @param array $forms
	 * @return string
	 */
	public function getForm ($amount, array $forms) {

		df_param_integer ($amount, 0);

		/** @var string $result  */
		$result =
			df_a (
				$forms
				,
				$this->getIndex ($amount)
			)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * Форма склонения слова.
	 * Существительное с числительным склоняется одним из трех способов:
	 * 1 миллион, 2 миллиона, 5 миллионов.
	 *
	 * @param int $amount
	 * @return int
	 */
	public function getIndex ($amount) {

		/** @var int $result  */
		$result = null;

		/** @var int $n100 */
		$n100 = $amount % 100;

		df_assert_integer ($n100);


		/** @var int $n100 */
		$n10 = $amount % 10;

		df_assert_integer ($n10);


		if( ($n100 > 10) && ($n100 < 20) ) {
			$result = self::NOUN_FORM_5;
		}
		elseif ($n10 === 1) {
			$result = self::NOUN_FORM_1;
		}
		elseif (($n10 >= 2) && ($n10 <= 4) ) {
			$result = self::NOUN_FORM_2;
		}
		else {
			$result = self::NOUN_FORM_5;
		}

		df_result_integer ($result);

		return $result;
	}



	const NOUN_FORM_1 = 0;
	const NOUN_FORM_2 = 1;
	const NOUN_FORM_5 = 2;


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Format_NounForAmounts';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}


