<?php

class Df_Core_Model_Url_Rewrite extends Mage_Core_Model_Url_Rewrite {



	/**
	 * @return string|null
	 */
	public function getRequestPath () {

		/** @var string|null $result  */
		$result =
			$this->getData (self::PARAM_REQUEST_PATH)
		;

		if (!is_null ($result)) {
			df_result_string ($result);
		}

		return $result;
	}




	/**
	 * @param string $requestPath
	 * @return Df_Core_Model_Url_Rewrite
	 */
	public function setRequestPath ($requestPath) {

		df_param_string ($requestPath, 0);

		$this->setData (self::PARAM_REQUEST_PATH, $requestPath);

		return $this;
	}




	/**
	 * @param string $idPath
	 * @return Df_Core_Model_Url_Rewrite
	 */
	public function setIdPath ($idPath) {

		df_param_string ($idPath, 0);

		$this->setData (self::PARAM_ID_PATH, $idPath);

		return $this;
	}




	/**
	 * @param string $targetPath
	 * @return Df_Core_Model_Url_Rewrite
	 */
	public function setTargetPath ($targetPath) {

		df_param_string ($targetPath, 0);

		$this->setData (self::PARAM_TARGET_PATH, $targetPath);

		return $this;
	}






	/**
	 * @param  string $path
	 * @return Mage_Core_Model_Url_Rewrite
	 */
    public function loadByRequestPath ($path) {
		return
			parent::loadByRequestPath (
					!(df_enabled (Df_Core_Feature::SEO) && df_cfg()->seo()->urls()->getPreserveCyrillic())
				?
					$path
				:
					(
							!is_array ($path)
						?
							rawurldecode ($path)
						:
							array_map (
								"rawurldecode"
								,
								$path
							)
					)
			)
		;
    }



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Url_Rewrite';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}





	const PARAM_ID_PATH = 'id_path';
	const PARAM_IS_SYSTEM = 'is_system';
	const PARAM_OPTIONS = 'options';
	const PARAM_TARGET_PATH = 'target_path';
	const PARAM_REQUEST_PATH = 'request_path';
	const PARAM_STORE_ID = 'store_id';



}
