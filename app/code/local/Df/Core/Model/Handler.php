<?php

/**
 * Обработчик события.
 *
 * Обратите внимание, что «событие» и «обработчик события» — два разных объекта.
 * Это даёт возможность инкапсулировать программный код класса «событие»
 * и повторго использовать этот программный код для разных обработчиков событий
 */
abstract class Df_Core_Model_Handler extends Df_Core_Model_Abstract {


	/**
	 * @static
	 * @param string $class
	 * @param Df_Core_Model_Event $event
	 * @param array $additionalParams [optional]
	 * @return Df_Core_Model_Handler
	 */
	public static function create ($class, Df_Core_Model_Event $event, $additionalParams = array ()) {

		df_param_string ($class, 0);
		df_param_array ($additionalParams, 2);

		$result =
			df_model (
				$class
				,
				array_merge (
					array (
						self::PARAM_EVENT => $event
					)
					,
					$additionalParams
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Handler);

		return $result;
	}



	/**
	 * Метод-обработчик события
	 *
	 * @abstract
	 * @return void
	 */
	abstract public function handle ();


	/**
	 * Класс события (для валидации события)
	 *
	 * @abstract
	 * @return string
	 */
	abstract protected function getEventClass ();



	/**
	 * @return Df_Core_Model_Event
	 */
	protected function getEvent () {
		return $this->cfg (self::PARAM_EVENT);
	}



	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM_EVENT, $this->getEventClass()
			)
		;
	}



	/**
	 * Параметр конструктора: объект-событие
	 */
	const PARAM_EVENT = 'event';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Handler';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}
