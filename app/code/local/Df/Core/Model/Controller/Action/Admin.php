<?php


abstract class Df_Core_Model_Controller_Action_Admin extends Df_Core_Model_Controller_Action {



	/**
	 * @override
	 * @param string $path
	 * @param array $arguments [optional]
	 * @return Df_Core_Model_Controller_Action
	 */
	public function redirect ($path, $arguments = array()) {
		$this->getResponse()
			->setRedirect (
				$this->getController()->getUrl (
					$path
					,
					$arguments
				)
			)
		;
		return $this;
	}



	/**
	 * @override
	 * @return Df_Core_Controller_Admin
	 */
	protected function getController () {

		/** @var Df_Core_Controller_Admin $result  */
		$result = parent::getController();

		df_assert ($result instanceof Df_Core_Controller_Admin);

		return $result;
	}
}

