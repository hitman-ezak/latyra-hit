<?php

class Df_Core_Model_Location extends Df_Core_Model_Entity {


	/**
	 * @return string|null
	 */
	public function getCity () {
		return $this->cfg (self::PARAM__CITY);
	}



	/**
	 * @return Mage_Directory_Model_Country
	 */
	public function getCountry () {

		if (!isset ($this->_country)) {

			/** @var Mage_Directory_Model_Country $result  */
			$result =
				df_model (
					Df_Directory_Const::COUNTRY_CLASS_MF
				)
			;

			df_assert ($result instanceof Mage_Directory_Model_Country);
			
			$result->loadByCode ($this->getCountryIso2Code());

			$this->_country = $result;
		}

		df_assert ($this->_country instanceof Mage_Directory_Model_Country);

		return $this->_country;
	}


	/**
	* @var Mage_Directory_Model_Country
	*/
	private $_country;
	



	/**
	 * @return string
	 */
	public function getCountryIso2Code () {
		return $this->cfg (self::PARAM__COUNTRY_ISO2);
	}



	/**
	 * @return float
	 */
	public function getLatitude () {
		return floatval ($this->cfg (self::PARAM__LATITUDE));
	}



	/**
	 * @return float
	 */
	public function getLongitude () {
		return floatval ($this->cfg (self::PARAM__LONGITUDE));
	}



	/**
	 * @override
	 * @return string
	 */
	public function getName() {

		/** @var string $result */
		$result =
				is_null ($this->getId())
			?
				'Место'
			:
				sprintf (
					'Место №%d'
					,
					$this->getId()
				)
		;

		return $result;
	}



	/**
	 * @return string|null
	 */
	public function getPostalCode () {
		return $this->cfg (self::PARAM__POSTAL_CODE);
	}

	
	
	/**
	 * @return Df_Directory_Model_Region|null
	 */
	public function getRegion () {
	
		if (!isset ($this->_region) && !$this->_regionIsNull) {
	
			/** @var Df_Directory_Model_Region|null $result  */
			$result = null;

			if (0 !== $this->getRegionId()) {
				$result =
					df_model (
						Df_Directory_Model_Region::getNameInMagentoFormat()
					)
				;

				$result->load ($this->getRegionId());

				if (0 === intval ($result->getId())) {
					$result = null;
				}
			}

	
			if (!is_null ($result)) {
				df_assert ($result instanceof Df_Directory_Model_Region);
			}
			else {
				$this->_regionIsNull = true;
			}
	
			$this->_region = $result;
		}
	
	
		if (!is_null ($this->_region)) {
			df_assert ($this->_region instanceof Df_Directory_Model_Region);
		}		
		
		return $this->_region;
	}
	
	
	/**
	* @var Df_Directory_Model_Region|null
	*/
	private $_region;	
	
	/**
	 * @var bool
	 */
	private $_regionIsNull = false;	




	/**
	 * @return int
	 */
	public function getRegionId () {
		return intval ($this->cfg (self::PARAM__REGION_ID));
	}



	/**
	 * @return string
	 */
	public function getRegionName () {

		/** @var string $result  */
		$result =
				!is_null ($this->getRegion())
			?
				$this->getRegion()->getName()
			:
				$this->cfg (self::PARAM__REGION_NAME)
		;

		df_result_string ($result);

		return $result;
	}



	/**
	 * @return string|null
	 */
	private function getStreetAddress () {
		return $this->cfg (self::PARAM__STREET_ADDRESS);
	}




	/**
	 * @override
	 * @return void
	 */
    protected function _construct() {
		parent::_construct();
        $this->_init (Df_Core_Model_Resource_Location::getNameInMagentoFormat());
    }


	const PARAM__CITY = 'city';
	const PARAM__COUNTRY_ISO2 = 'country_iso2';
	const PARAM__ID = 'location_id';
	const PARAM__LATITUDE = 'latitude';
	const PARAM__LONGITUDE = 'longitude';
	const PARAM__PHONE = 'phone';
	const PARAM__POSTAL_CODE = 'postal_code';
	const PARAM__REGION_ID = 'region_id';
	const PARAM__REGION_NAME = 'region_name';
	const PARAM__STREET_ADDRESS = 'street_address';




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Location';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}

