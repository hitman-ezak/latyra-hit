<?php

 
class Df_Core_Model_Translate extends Mage_Core_Model_Translate {



	/**
	 * Adding translation data
	 *
	 * @override
	 * @param array $data
	 * @param string $scope
	 * @param bool $forceReload [optional]
	 * @return Mage_Core_Model_Translate
	 */
	protected function _addData($data, $scope, $forceReload=false) {

		/** @var bool $allowInterferenceAsBoolean  */
		static $allowInterferenceAsBoolean;

		if (!isset ($allowInterferenceAsBoolean)) {

			/** @var string $allowInterferenceAsString  */
			$allowInterferenceAsString =
				df_area (
					df_cfg()->localization ()->translation()->frontend()->allowInterference()
					,
					df_cfg()->localization ()->translation()->admin()->allowInterference()
				)
			;

			df_assert_string ($allowInterferenceAsString);


			$allowInterferenceAsBoolean =
					!Mage::getIsDeveloperMode()
				?
					(
							Df_Admin_Model_Config_Source_YesNoDev::VALUE__NO
						!==
							$allowInterferenceAsString
					)
				:
					(
							Df_Admin_Model_Config_Source_YesNoDev::VALUE__YES
						===
							$allowInterferenceAsString
					)
			;
		}



		foreach ($data as $key => $value) {
			if ($key === $value) {
				continue;
			}
			$key    = $this->_prepareDataString($key);
			$value  = $this->_prepareDataString($value);

			/** @var string|null $dataScope_value */
			$dataScope_value =
					is_null ($this->_dataScope)
				?
					null
				:
					df_a ($this->_dataScope, $key)
			;

			if ($scope && !is_null($dataScope_value) && !$forceReload ) {
				/**
				* Checking previous value
				*/
				$scopeKey = $dataScope_value . self::SCOPE_SEPARATOR . $key;
				if (!isset($this->_data[$scopeKey])) {
					if (isset($this->_data[$key])) {
						$this->_data[$scopeKey] = $this->_data[$key];
						/**
						* Not allow use translation not related to module
						*/


						/************************
						 * BEGIN PATCH
						 */

						if (!$allowInterferenceAsBoolean) {
							unset($this->_data[$key]);
						}

						/***********************
						 * END PATCH
						 */

					}
				}
				$scopeKey = $scope . self::SCOPE_SEPARATOR . $key;
				$this->_data[$scopeKey] = $value;
			}
			else {
				$this->_data[$key]     = $value;
				$this->_dataScope[$key] = $scope;
			}
		}
		return $this;
	}




    /**
     * Retrive translated template file
     *
	 * @override
     * @param string $file
     * @param string $type
     * @param string $localeCode
     * @return string
     */
    public function getTemplateFile($file, $type, $localeCode=null)
    {
        if (is_null($localeCode) || preg_match('/[^a-zA-Z_]/', $localeCode)) {
            $localeCode = $this->getLocale();
        }


		/**
		 * BEGIN PATCH
		 */
		if (
				(Df_Core_Const::LOCALE__RUSSIAN === $localeCode)
			&&
				(
						(
								df_enabled (Df_Core_Feature::LOCALIZATION)
							&&
								df_cfg()->localization ()->translation()->email()->isEnabled()
						)
					||
						(false !== strpos ($file, 'df' . DS))
				)

		) {
			$localeCode = 'ru_DF';
		}
		/**
		 * END PATCH
		 */



        $filePath = Mage::getBaseDir('locale')  . DS
                  . $localeCode . DS . 'template' . DS . $type . DS . $file;

        if (!file_exists($filePath)) { // If no template specified for this locale, use store default
            $filePath = Mage::getBaseDir('locale') . DS
                      . Mage::app()->getLocale()->getDefaultLocale()
                      . DS . 'template' . DS . $type . DS . $file;
        }

        if (!file_exists($filePath)) {  // If no template specified as  store default locale, use en_US
            $filePath = Mage::getBaseDir('locale') . DS
                      . Mage_Core_Model_Locale::DEFAULT_LOCALE
                      . DS . 'template' . DS . $type . DS . $file;
        }

        $ioAdapter = new Varien_Io_File();
        $ioAdapter->open(array('path' => Mage::getBaseDir('locale')));

        return (string) $ioAdapter->read($filePath);
    }




	/**
	 * @override
	 *
	 * @param $moduleName
	 * @param array $files
	 * @param bool $forceReload
	 * @return Df_Core_Model_Translate
	 */
    protected function _loadModuleTranslation ($moduleName, $files, $forceReload=false) {

		/** @var bool $localeIsRussian */
		static $localeIsRussian;

		if (!isset ($localeIsRussian)) {
			$localeIsRussian = df_helper()->localization()->locale()->isRussian();
		}

		/** @var bool $needEnableRmTranslation  */
		$needEnableRmTranslation = $this->needEnableRmTranslation();

		/** @var bool $needSetRmTranslationAsPrimary  */
		$needSetRmTranslationAsPrimary = $this->needSetRmTranslationAsPrimary();


		/** @var string $ruDfBasePath  */
		static $ruDfBasePath;

		if (!isset ($ruDfBasePath)) {
			$ruDfBasePath =
				implode (
					DS
					,
					array (
						Mage::getBaseDir('locale')
						,
						'ru_DF'
						,
						Df_Core_Const::T_EMPTY
					)
				)
			;
		}


        foreach ($files as $file) {
			/** @var string $file */

			/** @var array $paths  */
			$paths = array ();


			/** @var bool $isItRmModule  */
			$isItRmModule = (false !== strpos ($file, 'Df_'));


			if (
					$localeIsRussian
				&&
					(
							$needEnableRmTranslation
						||
							$isItRmModule
					)
			) {

				$paths []=
					/**
					 * Работает в 2 раза быстрее, чем implode
					 * @link http://stackoverflow.com/questions/4502654/php-many-concats-or-one-implode
					 */
						$ruDfBasePath
					.
						$file
				;
			}

			if (!$isItRmModule) {
				$paths [] = $this->_getModuleFilePath($moduleName, $file);
			}


			if ($needSetRmTranslationAsPrimary) {
				$paths = array_reverse ($paths);
			}


			foreach ($paths as $path) {
				$this->_addData($this->_getFileData($path), $moduleName, $forceReload);
			}

        }
        return $this;
    }


	
	
	
	/**
	 * @return bool
	 */
	private function isRuRuExist () {
	
		if (!isset ($this->_ruRuExist)) {
	
			/** @var bool $result  */
			$result = 
				is_dir (
					implode (
						DS
						,
						array (
							Mage::getBaseDir('locale')
							,
							'ru_RU'
						)
					)
				)
			;
	
			df_assert_boolean ($result);
	
			$this->_ruRuExist = $result;
		}
	
		df_result_boolean ($this->_ruRuExist);
	
		return $this->_ruRuExist;
	}
	
	
	/**
	* @var bool
	*/
	private $_ruRuExist;		

	



	/**
	 * @return bool
	 */
	private function needSetRmTranslationAsPrimary () {

		/** @var bool $result */
		static $result;

		if (!isset ($result)) {

			/** @var bool $result  */
			$result =
					/**
					 * Используем для экранов установки перевод Российской сборки Magento
					 */
					!Mage::isInstalled()
				||
					/**
					 * В системе выбрана русская локаль и присутствует только перевод
					 * Российской сборки Magento, поэтому автоматически включаем его
					 * и назначаем основным
					 */
					!$this->isRuRuExist()
				||
					df_area (
						df_cfg()->localization ()->translation()->frontend()
							->needSetAsPrimary()
						,
						df_cfg()->localization ()->translation()->admin()
							->needSetAsPrimary()
					)
			;


			df_result_boolean ($result);
		}

		return $result;
	}


	
	
	/**
	 * @return bool
	 */
	private function needEnableRmTranslation () {

		/** @var bool $result */
		static $result;

		if (!isset ($result)) {

			/** @var bool $result  */
			$result =
					df_helper()->localization()->locale()->isRussian()
				&&
					(
							/**
							 * Используем для экранов установки перевод Российской сборки Magento
							 */
							!Mage::isInstalled()
						||
							/**
							 * В системе выбрана русская локаль и присутствует только перевод
							 * Российской сборки Magento, поэтому автоматически включаем его
							 * и назначаем основным
							 */
							!$this->isRuRuExist()
						||
							df_area (
								df_cfg()->localization ()->translation()->frontend()->isEnabled()
								,
								df_cfg()->localization ()->translation()->admin()->isEnabled()
							)
					)
			;


			df_result_boolean ($result);
		}

		return $result;
	}
	



}