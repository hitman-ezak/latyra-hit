<?php


/**
 * Объект-обёртка вокруг другого объекта.
 */
abstract class Df_Core_Model_Adapter extends Df_Core_Model_Abstract {


	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Adapter';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}





	/**
	 * @static
	 * @param  Mage_Core_Model_Abstract $original
	 * @return Mage_Core_Model_Abstract
	 */
	public static function create (Mage_Core_Model_Abstract $original) {

		$result =
			df_model (
				self::getNameInMagentoFormat()
				,
				array (
					self::PARAM_ORIGINAL => $original
				)
			)
		;

		df_assert ($result instanceof Df_Core_Model_Adapter);

		return $result;
	}



	/**
	 * @abstract
	 * @return void
	 */
	abstract protected function getOriginalClass ();


	/**
	 * @return mixed
	 */
	protected function getOriginal () {
		return $this->cfg (self::PARAM_ORIGINAL);
	}


	/**
	 * @override
	 * @return void
	 */
	protected function _construct () {
		parent::_construct ();
		$this
			->validateClass (
				self::PARAM_ORIGINAL, $this->getOriginalClass ()
			)
		;
	}


	const PARAM_ORIGINAL = 'original';
}


