<?php



/**
 * Cообщение:		«controller_action_layout_generate_blocks_after»
 * Источник:		Mage_Core_Controller_Varien_Action::generateLayoutBlocks()
 * [code]
        if(!$this->getFlag('', self::FLAG_NO_DISPATCH_BLOCK_EVENT)) {
            Mage::dispatchEvent(
                'controller_action_layout_generate_blocks_after',
                array('action'=>$this, 'layout'=>$this->getLayout())
            );
        }
 * [/code]
 */
class Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter
	extends Df_Core_Model_Event_Controller_Action_Layout {
	



	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Controller_Action_Layout_GenerateBlocksAfter';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'controller_action_layout_generate_blocks_after';


}


