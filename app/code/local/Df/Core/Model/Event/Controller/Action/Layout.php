<?php


abstract class Df_Core_Model_Event_Controller_Action_Layout	extends Df_Core_Model_Event {
	

	
	
	/**
	 * @return Mage_Core_Controller_Varien_Action
	 */
	public function getAction () {

		/** @var Mage_Core_Controller_Varien_Action $result  */
		$result = $this->getEventParam (self::EVENT_PARAM__ACTION);

		df_assert ($result instanceof Mage_Core_Controller_Varien_Action);

		return $result;
	}
	
	


	/**
	 * @return Mage_Core_Model_Layout
	 */
	public function getLayout () {

		/** @var Mage_Core_Model_Layout $result  */
		$result = $this->getEventParam (self::EVENT_PARAM__LAYOUT);

		df_assert ($result instanceof Mage_Core_Model_Layout);

		return $result;
	}



	/**
	 * @override
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Controller_Action_Layout';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}


	const EXPECTED_EVENT_PREFIX = 'controller_action_layout';

	const EVENT_PARAM__LAYOUT = 'layout';
	const EVENT_PARAM__ACTION = 'action';


}


