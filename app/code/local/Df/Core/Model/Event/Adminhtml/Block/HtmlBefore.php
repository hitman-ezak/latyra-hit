<?php



/**
 * Cообщение:		«adminhtml_block_html_before»
 * Источник:		Mage_Adminhtml_Block_Template::toHtml()
 * [code]
        Mage::dispatchEvent('adminhtml_block_html_before', array('block' => $this));
 * [/code]
 */
class Df_Core_Model_Event_Adminhtml_Block_HtmlBefore extends Df_Core_Model_Event {



	/**
	 * @return Mage_Core_Block_Abstract
	 */
	public function getBlock () {

		/** @var Mage_Core_Block_Abstract $result  */
		$result =
			$this->getEventParam (self::EVENT_PARAM__BLOCK)
		;


		df_assert ($result instanceof Mage_Core_Block_Abstract);

		return $result;
	}





	/**
	 * @return string
	 */
	protected function getExpectedEventPrefix () {
		return self::EXPECTED_EVENT_PREFIX;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Event_Adminhtml_Block_HtmlBefore';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



	const EXPECTED_EVENT_PREFIX = 'adminhtml_block_html_before';

	const EVENT_PARAM__BLOCK = 'block';

}


