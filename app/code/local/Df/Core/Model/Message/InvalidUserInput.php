<?php

class Df_Core_Model_Message_InvalidUserInput extends Mage_Core_Model_Message_Error {



	/**
	 * @param Zend_Form_Element $element
	 * @return Df_Core_Model_Message_InvalidUserInput
	 */
	public function setElement (Zend_Form_Element $element) {
	
		df_assert ($element instanceof Zend_Form_Element);
	
		$this->_element = $element;
	
		return $this;
	}

	
	
	
	/**
	 * @return Zend_Form_Element
	 */
	public function getElement () {
	
		/** @var Zend_Form_Element $result  */
		$result = $this->_element;
	
		df_assert ($result instanceof Zend_Form_Element);
	
		return $this->_element;
	}
	

	
	/**
	* @var Zend_Form_Element
	*/
	private $_element;





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Model_Message_InvalidUserInput';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}

