<?php

class Df_Core_Helper_Config extends Mage_Core_Helper_Abstract {



	/**
	 * @param string $keyAsString
	 * @return array
	 */
	public function explodeKey ($keyAsString) {

		/** @var array $result  */
		$result =
			explode (
				self::PATH_SEPARATOR
				,
				$keyAsString
			)
		;

		df_result_array ($result);

		return $result;
	}




	/**
	 * Обратите внимание, что Magento кэширует объект-результат!
	 *
	 * @param string $key
	 * @return Mage_Core_Model_Config_Element|null
	 */
	public function getNodeByKey ($key) {

		df_param_string ($key, 0);

		/** @var Mage_Core_Model_Config_Element|null $result  */
		$result =
			Mage::getConfig ()->getNode ($key)
		;

		if (false === $result) {
			$result = null;
		}

		if (!is_null ($result)) {
			df_assert ($result instanceof Mage_Core_Model_Config_Element);
		}

		return $result;
	}



	/**
	 * @param Mage_Core_Model_Config_Element|null $node
	 * @return string
	 */
	public function getNodeValueAsString ($node) {

		if (!is_null ($node)) {
			df_assert ($node instanceof Mage_Core_Model_Config_Element);
		}

		/** @var string $result  */
		$result =
				is_null ($node)
			?
				Df_Core_Const::T_EMPTY
			:
				df_trim (
					/**
					 * Обратите внимание, что у классов
					 * Mage_Core_Model_Config_Element и SimpleXMLElement
					 * отсутствует метод __toString
					 * Так же, к некорректным результатам ведёт asXML()
					 * Однако (string) почему-то работает :-)
					 *
					 * Однако в функции df_module_enabled (string) не работает,
					 * а работает asXML
					 *
					 */
					(string)$node
				)
		;

		df_result_string ($result);

		return $result;
	}






	/**
	 * @param array $keyParts
	 * @return string
	 */
	public function implodeKey (array $keyParts) {

		/** @var string $result  */
		$result =
			implode (
				self::PATH_SEPARATOR
				,
				$keyParts
			)
		;

		df_result_string ($result);

		return $result;
	}



	const PATH_SEPARATOR = '/';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Config';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}