<?php

class Df_Core_Helper_Url extends Mage_Core_Helper_Abstract {



	/**
	 * @param string $url
	 * @return string
	 */
	public function addVersionStamp ($url) {

		df_param_string ($url, 0);

		/** @var string $result  */
		$result =
			sprintf (
				'%s?v=%s'
				,
				$url
				,
				rm_version()
			)
		;

		df_result_string ($result);

		return $result;
	}




	/**
	 * @return string
	 */
	public function getBase () {

		$result = Mage::getBaseUrl (Mage_Core_Model_Store::URL_TYPE_WEB);

		df_result_string ($result);

		return $result;
	}





	/**
	 * @param string $path
	 * @param bool $relative
	 * @return string
	 */
	public function fromPath ($path, $relative = true) {

		df_param_string ($path, 0);
		df_param_boolean ($relative, 1);


		/** @var string $result  */
		$result =
			implode (
				Df_Core_Const::T_EMPTY
				,
				array (
					$relative ? Df_Core_Const::T_EMPTY : $this->getBase ()
					,
					$this->encode (
						$this->adjustSlashes (
							df_helper ()->core ()->path ()->makeRelative (
								$path
							)
						)
					)
				)
			)
		;


		df_result_string ($result);

		return $result;


	}





	/**
	 * @param  string $url
	 * @return string
	 */
	public function toPath ($url) {

		df_param_string ($url, 0);

		df_assert (
			$this->isInternal ($url)
			,
			strtr (
						"Метод %method% требует, чтобы его параметр был внутренним для магазина адресом."
					.
						"\nОднако программист передал в качестве параметра адрес «%url%»"
					.
						", который не является внутренним для магазина адресом"
				,
				array (
					'%method%' => __METHOD__
					,
					'%url%' => $url
				)
			)
		)
		;


		$result =
			implode (
				DS
				,
				array (
					BP
					,
					df_helper ()->core ()->path ()->adjustSlashes (
						rawurldecode (
							$this->makeRelative ($url)
						)
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}







	/**
	 * @param  string $url
	 * @return string
	 */
	public function adjustSlashes ($url) {

		df_param_string ($url, 0);


		/** @var string $result */
		$result =
			str_replace (
				DS
				,
				Df_Core_Const::T_URL_PATH_SEPARATOR
				,
				$url
			)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @param  string $url
	 * @return string
	 */
	public function encode ($url) {

		df_param_string ($url, 0);

		/** @var string $result */
		$result =
			$this
				->processParts (
					self::RAW_URL_ENCODE
					,
					$url
				)
		;


		df_result_string ($result);

		return $result;
	}





	public function decode ($url) {

		df_param_string ($url, 0);

		/** @var string $result */
		$result =
			$this->processParts (
				self::RAW_URL_DECODE
				,
				$url
			)
		;

		df_result_string ($result);

		return $result;
	}






	/**
	 * @param  string $function
	 * @param  string $url
	 * @return string
	 */
	private function processParts ($function, $url) {

		df_param_string ($url, 0);

		/** @var string $result */
		$result =
			implode (
				Df_Core_Const::T_URL_PATH_SEPARATOR
				,
				array_map (
					$function
					,
					explode (
						Df_Core_Const::T_URL_PATH_SEPARATOR
						,
						$url
					)
				)
			)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @param  string $url
	 * @return string
	 */
	public function makeRelative ($url) {

		df_param_string ($url, 0);


		/** @var string $result  */
		$result =
				(
						-1
					>=
						stripos (
							$url
							,
							$this->getBase()
						)
				)
			?
				$url
			:
				df_text()->replaceCI (
					$this->getBase()
					,
					Df_Core_Const::T_EMPTY
					,
					$url
				)
		;


		df_result_string ($result);

		return $result;
	}




	/**
	 * @param  string $url
	 * @param  string $domain
	 * @return bool
	 */
	public function isUrlBelongsToTheDomain ($url, $domain) {

		df_param_string ($url, 0);
		df_param_string ($domain, 1);

		/**
		 * Надо запретить распространение лицензии домена на поддомены
		 */

		try {
			/** @var Zend_Uri_Http $zendUri  */
			$zendUri = Zend_Uri::factory ($url);

			$result =
				in_array (
					mb_strtoupper (
						$zendUri->getHost()
					)
					,
					array (
						mb_strtoupper (
							$domain
						)
						,
						mb_strtoupper (
							'www.' . $domain
						)
					)
				)
			;
		}
		catch (Exception $e) {
			$result = (FALSE !== mb_strpos ($url, $domain, 0, Df_Core_Const::UTF_8));
		}



		df_result_boolean ($result);

		return $result;
	}






	/**
	 * @param  string $url
	 * @return bool
	 */
	public function isInternal ($url) {

		df_param_string ($url, 0);


		/** @var Zend_Uri_Http $uri */
		$uri = Zend_Uri_Http::fromString ($url);


		/** @var bool $result */
		$result =
				!$uri->getHost ()
			||
				(-1 < mb_strpos ($url, $this->getBase()))
		;


		df_result_boolean ($result);

		return $result;
	}



	const RAW_URL_ENCODE = 'rawurlencode';
	const RAW_URL_DECODE = 'rawurldecode';



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Url';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}

