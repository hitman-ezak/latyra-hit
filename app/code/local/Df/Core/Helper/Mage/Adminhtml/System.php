<?php

class Df_Core_Helper_Mage_Adminhtml_System extends Mage_Core_Helper_Abstract {


	/**
	 * @return Df_Core_Helper_Mage_Adminhtml_System_Config
	 */
	public function config () {

		/** @var Df_Core_Helper_Mage_Adminhtml_System_Config $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Adminhtml_System_Config::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Adminhtml_System_Config);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Adminhtml_System';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}