<?php

class Df_Core_Helper_Mage_Adminhtml extends Mage_Core_Helper_Abstract {


	
	/**
	 * @return Mage_Adminhtml_Model_Config
	 */
	public function getConfig () {
	
		if (!isset ($this->_config)) {
	
			/** @var Mage_Adminhtml_Model_Config $result  */
			$result = Mage::getModel('adminhtml/config');
	
			df_assert ($result instanceof Mage_Adminhtml_Model_Config);
	
			$result->getSections();

			$this->_config = $result;
		}
	
		df_assert ($this->_config instanceof Mage_Adminhtml_Model_Config);
	
		return $this->_config;
	}
	
	
	/**
	* @var Mage_Adminhtml_Model_Config
	*/
	private $_config;	
	



	/**
	 * @return Mage_Adminhtml_Helper_Data
	 */
	public function helper () {

		/** @var Mage_Adminhtml_Helper_Data $result  */
		$result = Mage::helper ('adminhtml');

		df_assert ($result instanceof Mage_Adminhtml_Helper_Data);

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mage_Adminhtml_Html
	 */
	public function html () {

		/** @var Df_Core_Helper_Mage_Adminhtml_Html $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Adminhtml_Html::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Adminhtml_Html);

		return $result;
	}



	/**
	 * @return Mage_Adminhtml_Model_Session
	 */
	public function session () {

		/** @var Mage_Adminhtml_Model_Session $result  */
		$result = Mage::getSingleton ('adminhtml/session');

		df_assert ($result instanceof Mage_Adminhtml_Model_Session);

		return $result;
	}


	/**
	 * @return Df_Core_Helper_Mage_Adminhtml_System
	 */
	public function system () {

		/** @var Df_Core_Helper_Mage_Adminhtml_System $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Adminhtml_System::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Adminhtml_System);

		return $result;
	}


	/**
	 * @return Df_Core_Helper_Mage_Adminhtml_Widget
	 */
	public function widget () {

		/** @var Df_Core_Helper_Mage_Adminhtml_Widget $result  */
		$result = Mage::helper (Df_Core_Helper_Mage_Adminhtml_Widget::getNameInMagentoFormat());

		df_assert ($result instanceof Df_Core_Helper_Mage_Adminhtml_Widget);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Adminhtml';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}

}