<?php

class Df_Core_Helper_Mage_Customer extends Mage_Core_Helper_Abstract {



	/**
	 * @return Mage_Customer_Helper_Address
	 */
	public function addressHelper () {

		/** @var Mage_Customer_Helper_Address $result */
		static $result;

		if (!isset ($result)) {

			/** @var Mage_Customer_Helper_Address $result  */
			$result = Mage::helper ('customer/address');

			df_assert ($result instanceof Mage_Customer_Helper_Address);
		}

		return $result;
	}



	/**
	 * @return bool
	 */
    public function isLoggedIn() {

		/** @var bool $result */
        $result = $this->session()->isLoggedIn();

		df_result_boolean ($result);

		return $result;

    }



	/**
	 * @return Mage_Customer_Model_Session
	 */
	public function session () {

		$result = Mage::getSingleton('customer/session');

		df_assert ($result instanceof Mage_Customer_Model_Session);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Customer';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}