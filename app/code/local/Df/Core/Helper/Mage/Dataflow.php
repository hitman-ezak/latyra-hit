<?php

class Df_Core_Helper_Mage_Dataflow extends Mage_Core_Helper_Abstract {


	/**
	 * @return Mage_Dataflow_Model_Batch
	 */
	public function batch () {

		/** @var Mage_Dataflow_Model_Batch $result  */
		$result = Mage::getSingleton('dataflow/batch');

		df_assert ($result instanceof Mage_Dataflow_Model_Batch);

		return $result;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Dataflow';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}