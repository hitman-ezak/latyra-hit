<?php

class Df_Core_Helper_Output_Element extends Mage_Core_Helper_Abstract {



	/**
	 * @param array $params
	 * @return Df_Core_Block_Element_Input_Hidden
	 */
	public function createInputHidden (array $params = array ()) {

		df_param_array ($params, 0);


		/** @var Df_Core_Block_Element_Input_Hidden $result  */
		$result =
			df_mage()->core()->layout()
				->createBlock(
					Df_Core_Block_Element_Input_Hidden::getNameInMagentoFormat()
					,
					Df_Core_Const::T_EMPTY
					,
					$params
				)
		;


		df_assert ($result instanceof Df_Core_Block_Element_Input_Hidden);

		return $result;
	}





	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Mage_Index';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}


