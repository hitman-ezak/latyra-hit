<?php



class Df_Core_Helper_Lib_Abstract extends Mage_Core_Helper_Abstract {

	/**
	 *
	 */
	public function __construct () {
		$this
			->includeScripts ()
			->addIncludePath ()
		;
		return $this;
	}


	/**
	 * @return Df_Core_Helper_Lib_Abstract
	 */
	private function addIncludePath () {
		set_include_path (
			implode (
				PATH_SEPARATOR
				,
				array (
					get_include_path ()
					,
					df_path ()->removeTrailingSlash (
						$this->getLibPath ()
					)
				)
			)
		)
		;
		return $this;
	}


	/**
	 * @return Df_Core_Helper_Lib_Abstract
	 */
	private function includeScripts () {
		$this->setCompatibleErrorReporting ();
		array_map (
			array ($this, "includeScript")
			,
			$this->getScriptsToInclude ()
		)
		;
		$this->restoreErrorReporting ();
		return $this;
	}


	/**
	 * @return int
	 */
	protected function getIncompatibleErrorLevels () {
		return 0;
	}


	/**
	 * @return array
	 */
	protected function getScriptsToInclude () {
		return array ();
	}


	/**
	 * @var int
	 */
	private $_errorReporting;

	/**
	 * @return Df_Core_Helper_Lib_Abstract
	 */
	public function setCompatibleErrorReporting () {
		$this->_errorReporting = error_reporting ();
		error_reporting ($this->_errorReporting ^ $this->getIncompatibleErrorLevels ());
		return $this;
	}


	/**
	 * @return Df_Core_Helper_Lib_Abstract
	 */
	public function restoreErrorReporting () {
		if (isset ($this->_errorReporting)) {
			error_reporting ($this->_errorReporting);
		}
		return $this;
	}



	/**
	 * @param  string $libName
	 * @return Df_Core_Helper_Lib_Abstract
	 */
	public function includeScript ($libName) {
		require_once $this->getScriptPath ($libName);
		return $this;
	}


	/**
	 * @param  string $libName
	 * @return string
	 */
	private function getScriptPath ($libName) {
		return
			sprintf (
				"%s%s.php"
				,
				$this->getLibPath ()
				,
				$libName
			)
		;

//		if (defined ('COMPILER_INCLUDE_PATH') ) {
//			$compiledResult =
//				sprintf (
//					"%s%s.php"
//					,
//					$this->getCompiledLibPath ()
//					,
//					$libName
//				)
//			;
//			if (is_file ($compiledResult)) {
//				$result = $compiledResult;
//			}
//		}
//		return $result;
	}



	private $_libPath;

	private function getLibPath () {
		if (!isset ($this->_libPath)) {
			$this->_libPath =
					defined ('COMPILER_INCLUDE_PATH') && is_dir ($this->getCompiledLibPath ())
				?
					$this->getCompiledLibPath ()
				:
					$this->getStandardLibPath ()
			;
		}
		return $this->_libPath;
	}



	private $_standardLibPath;

	private function getStandardLibPath () {
		if (!isset ($this->_standardLibPath)) {
			$this->_standardLibPath =
					implode (
						DS
						,
						array (
							BP
							,
							'app', 'code', 'local'
							,
							str_replace (
								'_', DS, $this->_getModuleName ()
							)
							,
							'lib'
						)
					)
				.
					DS
			;
		}
		return $this->_standardLibPath;
	}



	private $_compiledLibPath;

	private function getCompiledLibPath () {
		if (!isset ($this->_compiledLibPath)) {
			$this->_compiledLibPath =
					!defined ('COMPILER_INCLUDE_PATH')
				?
					null
				:
						implode (
							DS
							,
							array (
								COMPILER_INCLUDE_PATH
								,
								str_replace ('_', DS, $this->_getModuleName ())
								,
								'lib'
							)
						)
					.
						DS
			;
		}
		return $this->_compiledLibPath;
	}




	/**
	 * @var string
	 */
	private $_moduleLocalPath;

	/**
	 * Path to module relative to app/code, e.g.: core/Mage/Catalog
	 *
	 * @return string
	 */
	private function getModuleLocalPath () {
		if (!isset ($this->_moduleLocalPath)) {
			$this->_moduleLocalPath =
				df_helper ()->core ()->path ()->adjustSlashes (
					implode (
						DS
						,
						array_slice (
							explode (
								DS
								,
								str_replace (
									Mage::getRoot ()
									,
									''
									,
									realpath (
										dirname (
											$this->getCurrentInstanceFileName ()
										)
									)
								)
							)
							,
							2
							,
							3
						)
					)
				)
			;
		}
		return $this->_moduleLocalPath;
	}



	/**
	 * @var string
	 */
	private $_currentInstanceFileName;

	/**
	 * Returns path to current instance's file
	 *
	 * @return string
	 */
	private function getCurrentInstanceFileName () {
		if (!isset ($this->_currentInstanceFileName)) {
			$this->_currentInstanceFileName =
				$this
					->getReflectionClass ()
					->getFileName ()
			;
		}
		return $this->_currentInstanceFileName;
	}


	/**
	 * @var ReflectionClass
	 */
	private $_reflectionClass;

	/**
	 * @return ReflectionClass
	 */
	private function getReflectionClass () {
		if (!isset ($this->_reflectionClass)) {
			$this->_reflectionClass =
				new ReflectionClass (
					get_class ($this)
				)
			;
		}
		return $this->_reflectionClass;
	}

}