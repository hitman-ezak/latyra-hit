<?php

class Df_Core_Helper_File extends Mage_Core_Helper_Abstract {


	/**
	 * @param  string $file
	 * @return string
	 */
	public function getExt ($file) {
		$dot = strrpos($file, '.') + 1;
		return substr($file, $dot);
	}
	

	/**
	 * @param  string $file
	 * @return mixed
	 */
	public function stripExt ($file) {
		return preg_replace('#\.[^.]*$#', '', $file);
	}


	/**
	 * @param  string $fileName
	 * @return string
	 */
	public function getUniqueFileName ($fileName) {
		$result = $fileName;

		if (file_exists ($fileName)) {
			$fileInfo = pathinfo($fileName);

			$dirname = df_a ($fileInfo, 'dirname');
			$extension = df_a ($fileInfo, 'extension');
			$key = df_a ($fileInfo, 'filename');

			$i = 1;
			while (1) {
				$result =
					sprintf (
						"%s/%s"
						,
						$dirname
						,
						implode (
							"."
							,
							df_clean (
								array (
									$this->generateOrderedKey ($key, $i++)
									,
									$extension
								)
							)
						)
					)
				;

				if (!file_exists ($result)) {
					break;
				}
			}
		}

		return $result;
	}


	/**
	 * @param  string $key
	 * @param  int $ordering
	 * @return string
	 */
	private function generateOrderedKey ($key, $ordering) {
		return
				(1 === $ordering)
			?
				$key
			:
				implode (
					"-"
					,
					array (
						$key
						,
						$ordering
					)
				)
		;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_File';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
