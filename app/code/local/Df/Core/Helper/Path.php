<?php

class Df_Core_Helper_Path extends Mage_Core_Helper_Abstract {



	/**
	 * @param  string $path
	 * @return mixed
	 */
	public function makeRelative ($path) {

		df_param_string ($path, 0);


		/** @var string $cleaned  */
		$cleaned = $this->clean ($path);


		/** @var string $base  */
		$base = BP . DS;


		/** @var string $result */
		$result =
				(0 === strpos ($cleaned, $base))
			?

		        str_replace ($base, '', $cleaned)
			:
				$cleaned
		;


		df_result_string ($result);

		return $result;
	}





	/**
	 * @param  string $path
	 * @return string
	 */
	public function adjustSlashes ($path) {

		df_param_string ($path, 0);


		/** @var string $result */
		$result =
			str_replace (
				Df_Core_Const::T_URL_PATH_SEPARATOR
				,
				DS
				,
				$path
			)
		;


		df_result_string ($result);

		return $result;
	}



	/**
	 * Function to strip additional / or \ in a path name
	 *
	 * @param	string	$path	The path to clean
	 * @param	string	$ds		Directory separator (optional)
	 * @return	string	The cleaned path
	 */
	public function clean ($path, $ds=DS) {

		df_param_string ($path, 0);
		df_param_string ($path, 1);

		/** @var string $result */
		$result = df_trim ($path);

		$result =
				df_empty ($result)
			?
				BP
			:
				$this->adjustSlashes (
					// Remove double slashes and backslahses and convert all slashes and backslashes to DS
					preg_replace (
						'#[/\\\\]+#u'
						,
						$ds
						,
						$result
					)
				)
		;

		df_assert (
			is_string ($result)
			,
			strtr (
				"[%method%]:\tНе могу обработать путь %path%"
				,
				array (
					'%method%' => __METHOD__
					,
					'%path%' => $path
				)
			)
		)
		;


		df_result_string ($result);

		return $path;
	}



	/**
	 * @param  string $path
	 * @return string
	 */
	public function removeTrailingSlash ($path) {
 		$length = strlen ($path);
		return
				(('/' != $path[$length - 1]) && (DS != $path[$length - 1]))
			?
				$path
			:
				substr (
					$path
					,
					0
					,
					$length - 1
				)
		;
	}



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Path';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}
}
