<?php

class Df_Core_Helper_Settings extends Mage_Core_Helper_Abstract {



	/**
	 * @param  int $value
	 * @param  int $stackLevel [optional]
	 * @return void
	 */
	public function assertYesNo ($value, $stackLevel = 0) {
			df_result_integer (
				$value,	0

				/**
				 * Т.к. этот метод — вспомогательный, то глубина стека — не 0, а 1
				 */
				, $stackLevel + 1
			)
		;
		df_result_between ($value, 0, 1, 0, 1);
	}



	/**
	 * @param  string $key
	 * @param mixed $store [optional]
	 * @return bool
	 */
	public function getYesNo ($key, $store = null) {

		/** @var bool $result  */
		$result =
			$this->parseYesNo (
				Mage::getStoreConfig ($key, $store)
			)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @param  string $value
	 * @return bool
	 */
	public function parseYesNo ($value) {

		if (df_empty ($value) || ('false' === $value)) {
			$result = false;
		}
		else {

			/** @var int $valueAsInteger  */
			$valueAsInteger = intval ($value);

			$this->assertYesNo ($valueAsInteger, 1);


			/** @var bool $result  */
			$result = (0 !== $valueAsInteger);
		}

		return $result;
	}





	/**
	 * @param  int $storeId
	 * @return Df_Core_Helper_Settings
	 */
	public function setStoreId ($storeId) {
		$this->_storeId = $storeId;
	    return $this;
	}


	/**
	 * @return int
	 */
	public function getStoreId () {
		return $this->_storeId;
	}


	/**
	 * @var int
	 */
	private $_storeId;



	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Settings';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}