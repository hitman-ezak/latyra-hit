<?php

class Df_Core_Helper_Db extends Mage_Core_Helper_Abstract {



	/**
	 * @return Varien_Db_Adapter_Pdo_Mysql
	 */
	public function conn () {

		/** @var Varien_Db_Adapter_Pdo_Mysql $result  */
		$result = $this->getCoreResource()->getConnection ('write');

		/**
		 * В Magento ранее версии 1.6 отсутствует интерфейс Varien_Db_Adapter_Interface,
		 * поэтому проводим грубую проверку на класс Varien_Db_Adapter_Pdo_Mysql
		 */
		df_assert ($result instanceof Varien_Db_Adapter_Pdo_Mysql);

		return $result;
	}




	/**
	 * @param Varien_Db_Adapter_Pdo_Mysql|Varien_Db_Adapter_Interface $adapter
	 * @param string $table
	 * @return Df_Core_Helper_Db
	 */
	public function truncate ($adapter, $table) {

		/** @var bool $truncated */
		$truncated = false;


		/** @var string $method */
		$method = Df_Core_Const::T_EMPTY;


		/**
		 * Метод Varien_Db_Adapter_Pdo_Mysql::truncateTable
		 * появился только в Magento CE 1.6.0.0,
		 * и при этом метод Varien_Db_Adapter_Pdo_Mysql::truncate стал устаревшим.
		 */

		/** @var string[] $methods */
		$methods = array ('truncateTable', 'truncate');

		foreach ($methods as $currentMethod) {
			/** @var string $currentMethod */
			if (method_exists($adapter, $currentMethod)) {
				$method = $currentMethod;
				break;
			}
		}

		if (!df_empty ($method)) {
			try {
				call_user_func (
					array (
						$adapter
						,
						$method
					)
					,
					$table
				);
				$truncated = true;
			}
			catch (Exception $e) {
				/**
				 * При выполнении профилей импорта-экспорта одним из клиентов
				 * произошёл сбой «DDL statements are not allowed in transactions»
				 */
			}
		}

		if (!$truncated) {
			$adapter->delete ($table);
		}

		return $this;
	}





	/**
	 * @return Mage_Core_Model_Resource
	 */
	private function getCoreResource () {

		if (!isset ($this->_coreResource)) {

			/** @var Mage_Core_Model_Resource $result  */
			$result = df_model ('core/resource');

			df_assert ($result instanceof Mage_Core_Model_Resource);

			$this->_coreResource = $result;
		}

		df_assert ($this->_coreResource instanceof Mage_Core_Model_Resource);

		return $this->_coreResource;
	}


	/**
	* @var Mage_Core_Model_Resource
	*/
	private $_coreResource;




	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Db';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}