<?php

class Df_Core_Helper_Data extends Mage_Core_Helper_Abstract {


	/**
	 * @return Df_Core_Helper_Data
	 */
	public function forbid () {
		$this->_isUsageForbidden = true;
		return $this;
	}


	/**
	 * @return bool
	 */
	public function isUsageForbidden () {
		return $this->_isUsageForbidden;
	}

	/**
	 * @var bool
	 */
	private $_isUsageForbidden = false;




	/**
	 * @return Df_Core_Helper_Assert
	 */
	public function assert () {

		/** @var Df_Core_Helper_Assert $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Assert $result  */
			$result = Mage::helper (Df_Core_Helper_Assert::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Assert);
		}

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Base
	 */
	public function base () {

		/** @var Df_Core_Helper_Base $result  */
		static $result;

		if (!isset ($result)) {
			$result = Mage::helper ('df_core/base');

			/**
			 * Вызывать здесь df_assert нельзя, иначе случится рекурсия
			 */
		}

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Check
	 */
	public function check () {

		/** @var Df_Core_Helper_Check $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Check $result  */
			$result = Mage::helper (Df_Core_Helper_Check::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Check);
		}

		return $result;
	}
	
	



	/**
	 * @return Df_Core_Helper_Config
	 */
	public function config () {

		/** @var Df_Core_Helper_Config $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Config $result  */
			$result =
				/**
				 * Не используем getNameInMagentoFormat,
				 * чтобы не попасть в рекурсию.
				 */
				Mage::helper ('df_core/config')
			;

			df_assert ($result instanceof Df_Core_Helper_Config);
		}

		return $result;
	}





	/**
	 * @return Df_Core_Helper_Db
	 */
	public function db () {

		/** @var Df_Core_Helper_Db $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Db $result  */
			$result = Mage::helper (Df_Core_Helper_Db::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Db);
		}

		return $result;
	}





	/**
	 * @return Df_Core_Helper_Debug
	 */
	public function debug () {

		/** @var Df_Core_Helper_Debug $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Debug $result  */
			$result = Mage::helper (Df_Core_Helper_Debug::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Debug);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_File
	 */
	public function file () {

		/** @var Df_Core_Helper_File $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_File $result  */
			$result = Mage::helper (Df_Core_Helper_File::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_File);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Fp
	 */
	public function fp () {

		/** @var Df_Core_Helper_Fp $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Fp $result  */
			$result =
				/**
				 * Не используем getNameInMagentoFormat,
				 * чтобы не попасть в рекурсию.
				 */
				Mage::helper ('df_core/fp')
			;

			df_assert ($result instanceof Df_Core_Helper_Fp);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Layout
	 */
	public function layout () {

		/** @var Df_Core_Helper_Layout $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Layout $result  */
			$result = Mage::helper (Df_Core_Helper_Layout::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Layout);
		}

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Mail
	 */
	public function mail () {

		/** @var Df_Core_Helper_Mail $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Mail $result  */
			$result = Mage::helper (Df_Core_Helper_Mail::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Mail);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Path
	 */
	public function path () {

		/** @var Df_Core_Helper_Path $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Path $result  */
			$result = Mage::helper (Df_Core_Helper_Path::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Path);
		}

		return $result;
	}

	



	/**
	 * @return Df_Core_Helper_Request
	 */
	public function request () {

		/** @var Df_Core_Helper_Request $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Request $result  */
			$result = Mage::helper (Df_Core_Helper_Request::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Request);
		}

		return $result;
	}
	



	/**
	 * @return Df_Core_Model_Reflection
	 */
	public function reflection () {

		/** @var Df_Core_Model_Reflection $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Model_Reflection $result  */
			$result =
				/**
				 * Не используем getNameInMagentoFormat,
				 * чтобы не попасть в рекурсию.
				 */
				df_model ('df_core/reflection')
			;

			df_assert ($result instanceof Df_Core_Model_Reflection);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_RemoteControl
	 */
	public function remoteControl () {

		/** @var Df_Core_Helper_RemoteControl $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_RemoteControl $result  */
			$result = Mage::helper (Df_Core_Helper_RemoteControl::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_RemoteControl);
		}

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Settings
	 */
	public function settings () {

		/** @var Df_Core_Helper_Settings $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Settings $result  */
			$result = Mage::helper (Df_Core_Helper_Settings::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Settings);
		}

		return $result;
	}



	/**
	 * @return Df_Core_Helper_Text
	 */
	public function text () {

		/** @var Df_Core_Helper_Text $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Text $result  */
			$result = Mage::helper (Df_Core_Helper_Text::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Text);
		}

		return $result;
	}





	/**
	 * @return Df_Core_Helper_Units
	 */
	public function units () {

		/** @var Df_Core_Helper_Units $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Units $result  */
			$result = Mage::helper (Df_Core_Helper_Units::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Units);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Url
	 */
	public function url () {

		/** @var Df_Core_Helper_Url $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Url $result  */
			$result = Mage::helper (Df_Core_Helper_Url::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Url);
		}

		return $result;
	}




	/**
	 * @return Df_Core_Helper_Version
	 */
	public function version () {

		/** @var Df_Core_Helper_Version $result */
		static $result;

		if (!isset ($result)) {

			/** @var Df_Core_Helper_Version $result  */
			$result = Mage::helper (Df_Core_Helper_Version::getNameInMagentoFormat());

			df_assert ($result instanceof Df_Core_Helper_Version);
		}

		return $result;
	}




    /**
	 * @param array $args
     * @return string
     */
    public function translateByParent (array $args) {

		/** @var string $result  */
        $result =
			df_helper()->localization()->translation()->translateByModule (
				$args, self::DF_PARENT_MODULE
			)
		;

		df_result_string ($result);

	    return $result;
    }



	const DF_PARENT_MODULE = 'Mage_Core';


}