<?php

class Df_Core_Helper_Check extends Mage_Core_Helper_Abstract {


	
	/**
	 * @var Varien_Data_Collection_Db $collection
	 * @return bool
	 */
	public function resourceDbCollectionAbstract (Varien_Data_Collection_Db $collection) {

		/** @var bool $result  */
		$result =
				@class_exists ('Mage_Core_Model_Resource_Db_Collection_Abstract')
			?
				($collection instanceof	Mage_Core_Model_Resource_Db_Collection_Abstract)
			:
				($collection instanceof	Mage_Core_Model_Mysql4_Collection_Abstract)
		;

		df_result_boolean ($result);

		return $result;
	}




	/**
	 * @var int|string|null|Mage_Core_Model_Store $store
	 * @return bool
	 */
	public function storeAsParameterForGettingConfigValue ($store) {

		/** @var bool $result  */
		$result =
				is_int ($store)
			||
				is_string ($store)
			||
				is_null ($store)
			||
				($store instanceof Mage_Core_Model_Store)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @var Varien_Data_Collection_Db $collection
	 * @return bool
	 */
	public function storeCollection (Varien_Data_Collection_Db $collection) {

		/** @var bool $result  */
		$result =
				@class_exists ('Mage_Core_Model_Resource_Store_Collection')
			?
				($collection instanceof	Mage_Core_Model_Resource_Store_Collection)
			:
				($collection instanceof	Mage_Core_Model_Mysql4_Store_Collection)
		;

		df_result_boolean ($result);

		return $result;
	}



	/**
	 * @var Varien_Data_Collection_Db $collection
	 * @return bool
	 */
	public function websiteCollection (Varien_Data_Collection_Db $collection) {

		/** @var bool $result  */
		$result =
				@class_exists ('Mage_Core_Model_Resource_Website_Collection')
			?
				($collection instanceof	Mage_Core_Model_Resource_Website_Collection)
			:
				($collection instanceof	Mage_Core_Model_Mysql4_Website_Collection)
		;

		df_result_boolean ($result);

		return $result;
	}






	/**
	 * @static
	 * @return string
	 */
	public static function getClass () {
		return 'Df_Core_Helper_Check';
	}


	/**
	 * Например, для класса Df_SalesRule_Model_Event_Validator_Process
	 * метод должен вернуть: «df_sales_rule/event_validator_process»
	 *
	 * @static
	 * @return string
	 */
	public static function getNameInMagentoFormat () {
		/** @var string $result */
		static $result;
		if (!isset ($result)) {
			$result = df()->reflection()->getModelNameInMagentoFormat (self::getClass());
		}
		return $result;
	}



}