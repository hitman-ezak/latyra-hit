<?php

class Df_Core_Const {


	const FAKE_USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0';


	const FILE_EXTENSION__TEMPLATE = 'phtml';
	const FILE_EXTENSION__XML = 'xml';



	const FILE_NAME_PATTERN__CSV = '#\.csv#i';
	const FILE_NAME_PATTERN__XML = '#\.xml$#i';



	const MAGENTO_CORE__CACHE_ID_SERPARATOR = '_';
	const MAGENTO_CORE__FORM_ELEMENT_ID_PARTS_SEPARATOR = '_';



	const T_ASSIGNMENT = '=';
	const T_COMMA = ',';
	const T_CONFIG_WORD_SEPARATOR = '_';
	const T_EMPTY = '';
	const T_FILE_EXTENSION_SEPARATOR = '.';
	const T_NEW_LINE = "\r\n";
	const T_PERIOD = '.';
	const T_REGEX_ALTERNATIVE_SEPARATOR = '|';
	const T_QUOTE_DOUBLE = '"';
	const T_QUOTE_SINGLE = '\'';
	const T_SPACE = ' ';
	const T_UNIQUE_SEPARATOR = '#russian-magento#';
	const T_URL_PARAM_SEPARATOR = '&';
	const T_URL_PATH_SEPARATOR = '/';
	const T_XPATH_SEPARATOR = '/';


	const CORE_RESOURCE_TRANSACTION_CLASS_MF = 'core/resource_transaction';


	const CONFIG_ELEMENT_CLASS = 'Mage_Core_Model_Config_Element';


	const LOCALE_CLASS = 'Mage_Core_Model_Locale';
	const LOCALE_CLASS_MF = 'core/locale';

	const SESSION_ABSTRACT_CLASS = 'Mage_Core_Model_Session_Abstract';

	const STORE_CLASS = 'Mage_Core_Model_Store';
	const STORE_CLASS_MF = 'core/store';

	const STORE_COLLECTION_CLASS_MF = 'core/store_collection';

	const WEBSITE_CLASS = 'Mage_Core_Model_Website';
	const WEBSITE_CLASS_MF = 'core/website';


	const LOCALE__RUSSIAN = 'ru_RU';

	const UTF_8 = 'UTF-8';

	const REDIRECT_TO_CONTROLLER_INDEX_ACTION = '*/*';

}

