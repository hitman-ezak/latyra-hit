<?php

/**
 * Это класс используется как пространство имён
 * для соответствующих потребительским функциям («feature») констант
 */
class Df_Core_Feature {

	const _1C = '1c';
	const ALL = 'all';
	const ACCESS_CONTROL = 'access-control';
	const ASSIST = 'assist';
	const AUTOLUX = 'autolux';
	const AUTOTRADING = 'autotrading';
	const BANNER = 'banner';
	const CHECKOUT = 'checkout';
	const CHRONOPAY = 'chronopay';
	const CMS_2 = 'cms-2';
	const CUSTOMER_BALANCE = 'customer-balance';
	const DATAFLOW = 'dataflow';
	const DATAFLOW_CATEGORIES = 'dataflow-categories';
	const DATAFLOW_CO = 'dataflow-co';
	const DATAFLOW_IMAGES = 'dataflow-images';
	const DELLIN = 'dellin';
	const DELIVERY_UA = 'delivery_ua';
	const DIRECTORY = 'directory';
	const EASYPAY = 'easypay';
	const EMS = 'ems';
	const EURO_EXPRESS = 'euro-express';
	const FULL_PAGE_CACHING = 'full-page-caching';
	const GALLERY = 'gallery';
	const GARANTPOST = 'garantpost';
	const GIFT = 'gift';
	const GUNSEL = 'gunsel';
	const IN_TIME = 'in-time';
	const INTERKASSA = 'interkassa';
	const INVITATION = 'invitation';
	const IPAY = 'ipay';
	const LIQPAY = 'liqpay';
	const LOCALIZATION = 'localization';
	const LOGGING = 'logging';
	const MEGAPOLIS = 'megapolis';
	const MONETA = 'moneta';
	const NEWSLETTER = 'newsletter';
	const NIGHT_EXPRESS = 'night-express';
	const NOVA_POSHTA = 'nova-poshta';
	const ONPAY = 'onpay';
	const PAYONLINE = 'payonline';
	const PD4 = 'pd4';
	const PEC = 'pec';
	const PLATRON = 'platron';
	const PONY_EXPRESS = 'pony-express';
	const PROMO_GIFT = 'promo-gift';
	const QIWI = 'qiwi';
	const RBK_MONEY = 'rbk-money';
	const REPORTS = 'reports';
	const REWARD = 'reward';
	const ROBOKASSA = 'robokassa';
	const RUSSIAN_POST = 'russian-post';
	const SALES = 'sales';
	const SAT = 'sat';
	const CDEK = 'cdek';
	const SEO = 'seo';
	const SPEED = 'speed';
	const SPSR = 'spsr';
	/**
	 * Фальшивая фича для ловли мошенников
	 */
	const SUPER = 'super';
	const TORG12 = 'torg12';
	const TWEAKS = 'tweaks';
	const TWEAKS_ADMIN = 'tweaks-admin';
	const UKR_POSHTA = 'ukr-poshta';
	const UNITELLER = 'uniteller';
	const VK = 'vk';
	const WALLET_ONE = 'wallet-one';
	const WEBMONEY = 'webmoney';
	const WEBPAY = 'webpay';
	const YANDEX_MARKET = 'yandex-market';

}


