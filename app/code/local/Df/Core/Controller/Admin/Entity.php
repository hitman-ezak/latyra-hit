<?php

abstract class Df_Core_Controller_Admin_Entity extends Df_Core_Controller_Admin {


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getActionSaveClassMf();



	/**
	 * @return string
	 */
	abstract protected function getActiveMenuPath();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityClassMf();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityTitle();


	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getEntityTitleNew();



	/**
	 * @abstract
	 * @return string
	 */
	abstract protected function getMessageDeleteSuccess();


	/**
	 * @return array
	 */
	abstract protected function getTitleParts();


	/**
	 * @return void
	 */
	public function deleteAction() {

		/** @var Df_Core_Model_Controller_Action_Admin_Entity_Delete $action */
		$action =
			df_model (
				Df_Core_Model_Controller_Action_Admin_Entity_Delete::getNameInMagentoFormat()
				,
				array (
					Df_Core_Model_Controller_Action_Admin_Entity_Delete
						::PARAM__CONTROLLER => $this
					,
					Df_Core_Model_Controller_Action_Admin_Entity_Delete
						::PARAM__ENTITY_CLASS_MF => $this->getEntityClassMf()
					,
					Df_Core_Model_Controller_Action_Admin_Entity_Delete
						::PARAM__MESSAGE_SUCCESS => $this->getMessageDeleteSuccess()
					,
					Df_Core_Model_Controller_Action_Admin_Entity_Delete
						::PARAM__REQUEST_PARAMS => $this->getRequest()->getParams()
				)
			)
		;

		df_assert ($action instanceof Df_Core_Model_Controller_Action_Admin_Entity_Delete);

		$action->process();
	}




	/**
	 * @return void
	 */
	public function editAction () {

		$this->setTitle();

		$this
			->title (
					$this->getEntity()->getId()
				?
					$this->getEntityTitle()
				:
					$this->getEntityTitleNew()
			)
		;

		$this->loadAndRenderLayout();
	}




	/**
	 * @return void
	 */
    public function indexAction () {
		$this->setTitle();
		$this->loadAndRenderLayout();
    }



	/**
	 * @return void
	 */
	public function newAction () {
		$this->_forward('edit');
	}




	/**
	 * @return void
	 */
    public function saveAction () {

		/** @var Df_Core_Model_Controller_Action_Admin_Entity_Save $action */
		$action =
			df_model (
				$this->getActionSaveClassMf()
				,
				array (
					Df_Core_Model_Controller_Action_Admin_Entity_Save
						::PARAM__CONTROLLER => $this
					,
					Df_Core_Model_Controller_Action_Admin_Entity_Save
						::PARAM__REQUEST_PARAMS => $this->getRequest()->getParams()
				)
			)
		;

		df_assert ($action instanceof Df_Core_Model_Controller_Action_Admin_Entity_Save);

		$action->process();
    }


	/**
	 * @return Df_Core_Model_Entity
	 */
	protected function getEntity () {

		if (!isset ($this->_entity)) {

			/** @var Df_Core_Model_Entity $result  */
			$result =
				df_model (
					$this->getEntityClassMf()
				)
			;

			df_assert ($result instanceof Df_Core_Model_Entity);


			/** @var int $entityId */
			$entityId =
				intval (
					$this->getRequest()->getParam (
						/**
						 * Обратите внимание, что при запросе на редактирование сущности
						 * идентификатор сущности передаётся в адресе запроса
						 * параметром «id» (для красоты адреса),
						 * однако при запросе на сохранение сущности идентификатор сущности
						 * передаётсся в массиве POST параметром, имя которого соответствует
						 * имени идентификатора сущности (getIdFieldName())
						 */
						self::REQUEST_PARAM__ENTITY_ID
					)
				)
			;
			df_assert_between ($entityId, 0);


			if (0 < $entityId) {
				$result->load ($entityId);
				df_assert_between ($result->getId(), 1);
			}


			/** @var array|null $data */
			$data =
				df_mage()->adminhtml()->session()
					->getData (
						$result->getSessionKey()
						,
						$clear = true
					)
			;

			if (!df_empty ($data)) {
				$result->addData($data);
			}

			Mage::register($result->getSessionKey(), $result);

			$this->_entity = $result;
		}

		df_assert ($this->_entity instanceof Df_Core_Model_Entity);

		return $this->_entity;
	}


	/**
	* @var Df_Core_Model_Entity
	*/
	private $_entity;




	/**
	 * @return Df_Core_Controller_Admin_Entity
	 */
	private function loadAndRenderLayout () {

		$this->loadLayout();
		$this->setActiveMenu ($this->getActiveMenuPath());
		$this
			->addBreadcrumb (
				df_array_last ($this->getTitleParts())
				,
				df_array_last ($this->getTitleParts())
			)
		;
		$this->renderLayout();

		return $this;
	}




	/**
	 * @return Df_Core_Controller_Admin_Entity
	 */
	private function setTitle () {

		foreach ($this->getTitleParts() as $titlePart) {
			/** @var string|int|bool|null $titlePart */
			$this
				->title (
					$this->__($titlePart)
				)
			;
		}

		return $this;
	}


	/**
	 * Обратите внимание, что при запросе на редактирование сущности
	 * идентификатор сущности передаётся в адресе запроса
	 * параметром «id» (для красоты адреса),
	 * однако при запросе на сохранение сущности идентификатор сущности
	 * передаётсся в массиве POST параметром, имя которого соответствует
	 * имени идентификатора сущности (getIdFieldName())
	 */
	const REQUEST_PARAM__ENTITY_ID = 'id';

}
