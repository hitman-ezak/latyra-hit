<?php
/**
*	Author		: 	Cybernetikz
*	Author Email:   info@cybernetikz.com
*	Blog		: 	http://blog.cybernetikz.com
*	Website		: 	http://www.cybernetikz.com
*/

class Cybernetikz_Cnslider_Block_Slider extends Mage_Core_Block_Template
{
    protected $_sliderCollection = null;

    protected function _getCollection()
    {
        $collection=Mage::getResourceModel('cybernetikz_cnslider/slider_collection');
		//$collection = Mage::getModel('cybernetikz_cnslider/slider')->getResourceCollection();
		$collection->addFieldToFilter('store_id',Mage::app()->getStore(true)->getId());
		$collection->addFieldToFilter('is_active','1');
		$collection->addOrder('sort_order','ASC');
		return $collection;
    }

    public function getCollection()
    {
        if (is_null($this->_sliderCollection)) {
            $this->_sliderCollection = $this->_getCollection();
        }
        return $this->_sliderCollection;
    }


    public function getImageUrl($item, $width)
    {
        return Mage::helper('cybernetikz_cnslider/image')->resize($item, $width);
    }
}
