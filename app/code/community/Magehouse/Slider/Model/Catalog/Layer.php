<?php

/**
 * Catalog view layer model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Mrugesh Mistry <support@magehouse.com>
 */
class Magehouse_Slider_Model_Catalog_Layer extends Mage_Catalog_Model_Layer
{


    public function prepareProductCollection($collection)
    {
        $collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($this->getCurrentCategory()->getId());

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        return $this;
    }

	/*
	* Add Filter in product Collection for new price
	*
	* @return object
	*/
    public function getProductCollection()
    {

        if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
            $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
        } else {
            $collection = $this->getCurrentCategory()->getProductCollection();
            $this->prepareProductCollection($collection);
            $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
        }

		$this->currentRate = $collection->getCurrencyRate();

		$max=$this->getMaxPriceFilter();
		$min=$this->getMinPriceFilter();
        $maxm2=$this->getMaxM2PriceFilter();
        $minm2=$this->getMinM2PriceFilter();

		if($min && $max){
			$collection->getSelect()->where(' final_price >= "'.$min.'" AND final_price <= "'.$max.'" ');
		}

        $counter = Mage::registry('price_m2');

        if($minm2 && $maxm2 && $counter<1){
            //TODO cant understand why not working and fire up an error(in pma this sql is working great)
            $counter++;
            if($counter>1) {
                Mage::unregister('price_m2');
            }
            Mage::register('price_m2', $counter);

            $collection->getSelect()->where('price_m2 >= '.$minm2.' AND price_m2 <= '.$maxm2.' ');
        }
        return $collection;
    }

    /*
	* convert Price m2 as per currency
	*
	* @return currency
	*/
    public function getMaxM2PriceFilter(){
        if (isset($_GET['maxm2'])):
            return round($_GET['maxm2']/$this->currentRate);
        else:
            return false;
        endif;
    }


    /*
    * Convert Min Price m2 to current currency
    *
    * @return currency
    */
    public function getMinM2PriceFilter(){
        if (isset($_GET['minm2'])):
            return round($_GET['minm2']/$this->currentRate);
        else:
            return false;
        endif;
    }
	
	/*
	* convert Price as per currency
	*
	* @return currency
	*/
	public function getMaxPriceFilter(){
        if (isset($_GET['max'])):
		    return round($_GET['max']/$this->currentRate);
        else:
            return false;
        endif;

	}
	
	
	/*
	* Convert Min Price to current currency
	*
	* @return currency
	*/
	public function getMinPriceFilter(){
        if (isset($_GET['min'])):
		    return round($_GET['min']/$this->currentRate);
        else:
            return false;
        endif;
	}
    
	
}
