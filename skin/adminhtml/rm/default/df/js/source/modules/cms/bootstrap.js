(function ($) { $(function () {


	$('.cms-hierarchy .fieldset')
		.each (
			function () {
				df.admin.configForm.Fieldset
					.construct (
						{
							element: $(this)
						}
					)
				;
			}
		)
	;


	$('.cms-hierarchy .fieldset .df-field')
		.change (
			function () {
				if ('undefined' !== typeof hierarchyNodes) {
					hierarchyNodes.nodeChanged()
				}
			}
		)
	;



}); })(jQuery);
