/**
 * Программный код,
 * который надо выполнить сразу после загрузки страницы
 */

(function ($) { $(function () {

	df.namespace('df.vk.comments');

	if (df.vk.comments.enabled) {
		df.vk.Widget
			.construct (
				{
					applicationId: df.vk.comments.applicationId
					,
					containerId: 'vk_comments'
					,
					objectName: 'VK.Widgets.Comments'
					,
					parentSelector: '.product-view'
					,
					widgetSettings: df.vk.comments.settings
				}
			)
		;
	}


	df.namespace('df.vk.like');

	if (df.vk.like.enabled) {
		df.vk.Widget
			.construct (
				{
					applicationId: df.vk.like.applicationId
					,
					containerId: 'vk_like'
					,
					objectName: 'VK.Widgets.Like'
					,
					parentSelector: '.product-shop'
					,
					widgetSettings: df.vk.like.settings
				}
			)
		;
	}



	df.namespace('df.vk.groups');

	if (df.vk.groups.enabled) {
		df.vk.widget.Groups
			.construct (
				{
					applicationId: df.vk.groups.applicationId
					,
					containerId: 'vk_groups'
					,
					objectName: 'VK.Widgets.Group'
					,
					widgetSettings: df.vk.groups.settings
				}
			)
		;
	}


}); })(jQuery);