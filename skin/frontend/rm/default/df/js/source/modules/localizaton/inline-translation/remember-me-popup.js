(function ($) {

	'use strict';

	$(function () {

		/** @type {String} */
		var locale = $('html').attr('lang');

		/**
		 * Применяем заплатки перевода только для русскоязычного административного интерфейса
		 */
		if ('ru' === locale) {

			/** @type {jQuery} HTMLDivElement */
			var $block = $('#remember-me-popup');

			if (0 < $block.size())  {

				$block
					.html (
						$block.html()
							.replace (
								'What\'s this?'
								,
								'Что это?'
							)
							.replace (
								'Checking "Remember Me" will let you access your shopping cart on this computer when you are logged out'
								,
								'Вам не придется вводить логин и пароль каждый раз'
							)
					)
				;
			}
		}
	});

})(jQuery);
