/**
 * Программный код,
 * который надо выполнить сразу после загрузки страницы
 */

df.namespace ('df.checkout');

(function ($) { $(function () {

	df.checkout.Ergonomic
		.construct (
			{
				elementSelector: '.df .df-checkout-ergonomic'
			}
		)
	;


	df.checkout.OrderComments
		.construct (
			{
			}
		)
	;


	$(window)
		.bind (
			df.checkout.Ergonomic.sectionUpdated
			,
			/**
			 * @param {jQuery.Event} event
			 */
			function (event) {

				if ('review' === event.section) {

					df.checkout.OrderComments
						.construct (
							{
							}
						)
					;

				}
			}
		)
	;


}); })(jQuery);