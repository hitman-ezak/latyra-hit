(function ($) {

	df.namespace ('df.checkout.ergonomic');


	df.checkout.ergonomic.Helper = {

		/**
		 * @function
		 * @returns {df.checkout.ergonomic.Helper}
		 */
		construct: function (_config) { var _this = {

			init: function () {

			}



			,
			/**
			 * @public
			 * @param {String} inputId
			 * @returns {df.checkout.ergonomic.Helper}
			 */
			addFakeInputIfNeeded: function (inputId) {

				if (!document.getElementById (inputId)) {
					$('<input/>')
						.attr ({
							id: inputId
							,
							type: 'text'
						})
						.hide ()
						.appendTo (this.getFakeForm())
					;

				}

				return this;
			}




			,
			/**
			 * @public
			 * @returns {jQuery} HTMLFormElement
			 */
			getFakeForm: function () {

				if ('undefined' === typeof this._fakeForm) {

					var fakeFormClass = 'df-fake-form';

					/**
					 * @type {jQuery} HTMLFormElement
					 */
					this._fakeForm = $('form.' + fakeFormClass);

					if (1 > this._fakeForm.size ()) {
						this._fakeForm =
							$('<form/>')
								.addClass (fakeFormClass)
								.appendTo ('body')
						;
					}
				}

				return this._fakeForm;
			}


			,
			/**
			 * @public
			 * @param {Object} response
			 * @returns {df.checkout.ergonomic.Helper}
			 */
			updateSections: function (response) {

				if (response.df_update_sections) {

					$.each (response.df_update_sections, function () {

						/** @type {String} */
						var containerId = '#checkout-'+this.name+'-load';

						/** @type {jQuery} HTMLElement[] */
						var $newContent = $(this.html);

						/** @type {jQuery} HTMLElement */
						var $oldContainer = $(containerId);

						/** @type {jQuery} HTMLElement */
						var $newContainer = $(containerId, $newContent);

						if (0 === $newContainer.size()) {
							$oldContainer.html (this.html);
						}
						else {
							$oldContainer.replaceWith (this.html);
						}

						$(window)
							.trigger (
								{
									/** @type {String} */
									type: df.checkout.Ergonomic.sectionUpdated

									,
									/** @type {String} */
									section: this.name
								}
							)
						;
					});

				}


				if (response.update_section) {
					$(window)
						.trigger (
							{
								/** @type {String} */
								type: df.checkout.Ergonomic.sectionUpdated

								,
								/** @type {String} */
								section: response.update_section.name
							}
						)
					;
				}

				return this;
			}



		}; _this.init (); return _this; }


	};


	/**
	 * @type {df.checkout.ergonomic.Helper}
	 */
	df.checkout.ergonomic.helperSingleton =
		df.checkout.ergonomic.Helper
			.construct (
				{}
			)
	;





})(jQuery);