(function ($) {

	df.namespace ('df.checkout');


	df.checkout.OrderComments = {

		construct: function (_config) { var _this = {

			init: function () {

				if (
					/**
					 * Отсутствие блока комментариев
					 * говорит об отключенности данной функциональности
					 */
					(0 < this.getElement().size())
				) {

					if (this.isItMultiShippingCheckout()) {

						if ('below' === df.checkout.orderComments.position) {

							this.getTarget().after (this.getElement ());

						}
						else {

							this.getTarget().before (this.getElement ());

						}
					}

					else {
						if (0 === this.getAgreements().size()) {

							$('#checkout-review-submit')
								.prepend (
									$('<form/>')
										.attr ({
											id: 'checkout-agreements'
											,
											action: ''
											,
											onsubmit: 'return false;'
										})
										.append (
											this.getElement ()
										)
								)
							;
						}
						else {

							this.getElement ().removeClass ('buttons-set');

							if ('below' === df.checkout.orderComments.position) {
								this.getTarget().append (this.getElement ());
							}
							else {
								this.getTarget().prepend (this.getElement ());
							}

						}

					}

				}

			}




			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement[]
			 */
			getAgreements: function () {

				if ('undefined' == typeof this._agreements) {

					/** @type {jQuery} HTMLElement */
					this._agreements =
						$('.agree')
					;
				}

				return this._agreements;
			}




			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getElement: function () {

				if ('undefined' == typeof this._element) {

					/** @type {jQuery} HTMLElement */
					this._element =
						$('#df_checkout_review_orderComments')
							.clone()
							.removeAttr ('id')
							.removeClass ('df-hidden')
					;


					/**
					 * Стандартный браузерный программный код оформления заказа
					 * перезаписывает блок review
					 * после практически любых шагов покупателя при оформлении заказа.
					 *
					 * При этом перезаписывается и блок комментариев, и комментарии теряются.
					 *
					 * Чтобы сохранить комментарии,
					 * надо на событие потери фокуса блоком комментариев
					 * сохранять комментарий в какой-нибудь браузерной переменной
					 * (но не динамической переменной внутри данного класса,
					 * потому что объект данного класса создается заново
					 * после перезаписи блока review).
					 */

					/** @type {jQuery} HTMLTextAreaElement */
					var $textarea = $('textarea', this._element);

					$textarea
						.blur (
							function () {
								df.checkout.ergonomic.helperSingleton.orderComment =
									$textarea.val()
								;
							}
						)
						.val(
							df.checkout.ergonomic.helperSingleton.orderComment
						)
					;


				}

				return this._element;
			}




			,
			/**
			 * @private
			 * @returns {jQuery} HTMLElement
			 */
			getTarget: function () {

				if ('undefined' == typeof this._target) {

					/** @type {jQuery} HTMLElement */
					this._target =
						$('#checkout-agreements')
					;

				}

				return this._target;
			}




			,
			/**
			 * @private
			 * @returns {Boolean}
			 */
			isItMultiShippingCheckout: function () {

				if ('undefined' == typeof this._itIsMultiShippingCheckout) {

					/** @type {jQuery} HTMLElement */
					this._itIsMultiShippingCheckout =
						0 < $('.multiple-checkout').size()
					;

				}

				return this._itIsMultiShippingCheckout;
			}



		}; _this.init (); return _this; }


	};





})(jQuery);